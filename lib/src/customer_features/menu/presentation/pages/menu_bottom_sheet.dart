import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/menu/presentation/widgets/menu_item_builder.dart';
import 'package:flutter/material.dart';

openMenuBottomSheet(BuildContext context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(20),
      ),
    ),
    builder: (context) {
      return SizedBox(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.8,
          padding: const EdgeInsets.only(
            top: 15,
            left: 15,
            right: 15,
          ),
          child: Column(
            children: [
              Container(
                height: 4,
                width: 60,
                decoration: BoxDecoration(
                  color: kGreyMedium,
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
              Expanded(
                child: StatefulBuilder(builder: (context, setState) {
                  return Padding(
                    padding: MediaQuery.of(context).viewInsets,
                    child: SingleChildScrollView(
                      child: Container(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            /// Menu Title text
                            Text(
                              'Menu',
                              textAlign: TextAlign.center,
                              style: h2.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),

                            /// list tile items of my account
                            MenuItemBuilder(
                              onTap: () {},
                              leadingAssetIcon: AssetPath.giftCardIcon,
                              title: 'Gift Card',
                            ),
                            MenuItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.orderTrackForm,
                              ),
                              leadingAssetIcon: AssetPath.orderTrackIcon,
                              title: 'Order Track',
                            ),
                            MenuItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.returnPolicy,
                              ),
                              leadingAssetIcon: AssetPath.returnPolicyIcon,
                              title: 'Return Policy',
                            ),
                            MenuItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.aboutUs,
                              ),
                              leadingAssetIcon: AssetPath.aboutUs,
                              title: 'About Us',
                            ),
                            MenuItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.contactUs,
                              ),
                              leadingAssetIcon: AssetPath.contactUsIcon,
                              title: 'Contact Us',
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      );
    },
  );
}
