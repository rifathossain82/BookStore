import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class MenuItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String leadingAssetIcon;
  final String title;

  const MenuItemBuilder({
    Key? key,
    required this.onTap,
    required this.leadingAssetIcon,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      contentPadding: EdgeInsets.zero,
      onTap: onTap,
      leading: Image.asset(
        leadingAssetIcon,
        height: 30,
        width: 30,
      ),
      title: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: h3,
      ),
      trailing: Icon(
        Icons.arrow_forward_ios,
        color: kGrey,
        size: 15,
      ),
    );
  }
}
