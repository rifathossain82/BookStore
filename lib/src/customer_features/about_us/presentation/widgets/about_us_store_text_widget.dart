import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AboutUsStoryText extends StatelessWidget {
  const AboutUsStoryText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.screenWidth,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Our Story',
            style: h1,
          ),
          const SizedBox(height: 8),
          Text(
            '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam vulputate dictum nibh id elementum. Sed gravida tortor quis lacinia finibus. Fusce justo leo, condimentum accumsan orci gravida, laoreet gravida augue. Suspendisse egestas dui sed porta ultricies. Integer quis cursus orci. Mauris bibendum augue nec tortor blandit, eget efficitur quam venenatis. Duis varius, metus quis luctus dignissim, tellus justo molestie leo, vel dictum mi nulla vel nisl. In dignissim lorem dui, ac mattis ligula faucibus scelerisque. Suspendisse volutpat justo et hendrerit interdum. Etiam et sem sit amet massa auctor consectetur et eu orci. Etiam vitae luctus neque.

Proin pellentesque risus arcu. In aliquam iaculis erat, non egestas urna blandit facilisis. Proin vulputate nisl vitae malesuada fermentum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc eget orci eu libero porta molestie. Pellentesque vehicula, lorem at faucibus feugiat, ante massa iaculis lectus, et convallis nunc sem a diam. Sed quam est, faucibus ac imperdiet eget, aliquet eu mi. Pellentesque nulla nunc, commodo et interdum ac, euismod quis tortor. Phasellus euismod orci a vulputate bibendum. Sed ornare mauris mi, sed gravida orci lobortis ac. Nam pellentesque lacinia velit, nec pretium nisi ultricies nec. Aliquam egestas eleifend hendrerit. Praesent eget.''',
            textAlign: TextAlign.justify,
            style: h4.copyWith(

            ),
          ),
        ],
      ),
    );
  }
}
