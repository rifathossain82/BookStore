import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class ContactUsDropDownBuilder extends StatelessWidget {
  final List<String> items;
  final String? selectedValue;
  final ValueChanged onChanged;
  final FormFieldValidator? validator;
  final String hintText;

  const ContactUsDropDownBuilder({
    Key? key,
    required this.items,
    required this.selectedValue,
    required this.onChanged,
    this.validator,
    required this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 15,
      ),
      child: DropdownButtonFormField(
        validator: validator,
        isDense: true,
        decoration: const InputDecoration(
          isDense: true,
          border: OutlineInputBorder(),
        ),
        onChanged: onChanged,
        value: selectedValue,
        hint: Text(
          hintText,
        ),
        items: items
            .map(
              (item) => DropdownMenuItem(
                value: item,
                child: Text(
                  item,
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
