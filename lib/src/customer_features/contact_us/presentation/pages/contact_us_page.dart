import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/contact_us/presentation/widgets/contact_us_drop_down_builder.dart';
import 'package:book_store/src/customer_features/contact_us/presentation/widgets/contact_us_text_form_field_builder.dart';
import 'package:flutter/material.dart';

class ContactUsPage extends StatefulWidget {
  ContactUsPage({Key? key}) : super(key: key);

  @override
  State<ContactUsPage> createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {

  final contactUsFormKey = GlobalKey<FormState>();
  final nameTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  final emailTextController = TextEditingController();
  final messageTextController = TextEditingController();

  String? selectedSubject;
  final subjects = [
    'সাধারণ তথ্য সম্পর্কিত প্রশ্ন',
    'বই সম্পর্কিত তথ্য',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Contact Us'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
        ),
        child: SingleChildScrollView(
          child: Form(
            key: contactUsFormKey,
            child: Column(
              children: [
                ContactUsTextFormFieldBuilder(
                  label: 'Name',
                  textInputType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  controller: nameTextController,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyName;
                    }
                    return null;
                  },
                ),
                ContactUsTextFormFieldBuilder(
                  label: 'Phone',
                  textInputType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  controller: phoneTextController,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyPhone;
                    } else if(!value.toString().isValidPhone){
                      return Message.invalidPhone;
                    }
                    return null;
                  },
                ),
                ContactUsTextFormFieldBuilder(
                  label: 'Email',
                  textInputType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  controller: emailTextController,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyEmail;
                    } else if (!value.toString().isValidEmail) {
                      return Message.invalidEmail;
                    }
                    return null;
                  },
                ),
                ContactUsDropDownBuilder(
                  items: subjects,
                  selectedValue: selectedSubject,
                  onChanged: (value){
                    setState(() {
                      selectedSubject = value;
                    });
                  },
                  validator: (value){
                    if(selectedSubject == null){
                      return Message.emptySubject;
                    }
                    return null;
                  },
                  hintText: 'Subject',
                ),
                ContactUsTextFormFieldBuilder(
                  label: 'Message',
                  maxLine: 5,
                  minLine: 5,
                  textInputType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  controller: messageTextController,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyMessage;
                    }
                    return null;
                  },
                ),
                KButton(
                  onPressed: submitMethod,
                  child: Text(
                    'Submit',
                    style: h3.copyWith(
                      color: kWhite,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void submitMethod(){
    if(contactUsFormKey.currentState!.validate()){
      ///TODO: Yours Method
    }
  }
}
