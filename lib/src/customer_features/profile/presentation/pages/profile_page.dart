import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/services/image_services.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:book_store/src/core/widgets/select_country_code.dart';
import 'package:book_store/src/core/widgets/phone_text_field_builder.dart';
import 'package:book_store/src/customer_features/profile/presentation/widgets/card_title_builder.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final changePasswordFormKey = GlobalKey<FormState>();
  final changeMobileNumberFormKey = GlobalKey<FormState>();

  final nameTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  final otpTextController = TextEditingController();
  final currentPasswordTextController = TextEditingController();
  final newPasswordTextController = TextEditingController();
  final confirmPasswordTextController = TextEditingController();

  /// for phone textField
  bool isPhoneTextFieldValid = false;
  FakeCountryInfo selectedCountry = fakeCountryList.first;

  String? selectedGender;
  DateTime? selectedDate;

  bool isWantToChangePhoneNumber = false;
  bool isWantToChangePassword = false;
  bool isOTPSend = false;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate ?? DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        title: const Text('My Account'),
        centerTitle: true,
        elevation: 4,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _buildPersonalInformation(),
            _buildContactInformation(),
            _buildPassword(),

            /// build save button
            const SizedBox(height: 10),
            KButton(
              onPressed: () {
                if (isWantToChangePassword) {
                  if (changePasswordFormKey.currentState!.validate()) {
                    /// TODO: Your methods
                    Navigator.pop(context);
                  }
                }
              },
              width: context.screenWidth - 30,
              child: Text(
                'Save',
                style: h3.copyWith(
                  color: kWhite,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPersonalInformation() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      elevation: 4,
      margin: const EdgeInsets.all(15),
      child: Column(
        children: [
          const CardTitleBuilder(
            icon: Icons.person_outline_outlined,
            title: 'Personal Information',
          ),
          Container(
            color: kWhite,
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// name and date of birth text field
                Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: nameTextController,
                        // validator: (value) {
                        //   if (value!.isEmpty) {
                        //     return Message.emptyName;
                        //   }
                        //   return null;
                        // },
                        keyboardType: TextInputType.name,
                        decoration: InputDecoration(
                          isDense: true,
                          border: OutlineInputBorder(),
                          hintText: 'Rifat Hossain', // enter user name here
                        ),
                      ),
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: TextFormField(
                        onTap: () => _selectDate(context),
                        keyboardType: TextInputType.name,
                        readOnly: true,
                        decoration: InputDecoration(
                          isDense: true,
                          border: const OutlineInputBorder(),
                          hintText: selectedDate != null
                              ? DateFormat('dd-MM-yyyy').format(selectedDate!)
                              : 'Date of Birth', // enter user name here
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),

                /// male and female radio buttons
                Row(
                  children: [
                    Text(
                      'Gender: ',
                      style: h4,
                    ),
                    Expanded(
                      child: RadioListTile(
                        onChanged: (value) {
                          setState(() {
                            selectedGender = value;
                          });
                        },
                        value: 'Male',
                        groupValue: selectedGender,
                        dense: true,
                        contentPadding: EdgeInsets.zero,
                        title: Text(
                          'Male',
                          style: h4.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: RadioListTile(
                        onChanged: (value) {
                          setState(() {
                            selectedGender = value;
                          });
                        },
                        value: 'Female',
                        groupValue: selectedGender,
                        dense: true,
                        contentPadding: EdgeInsets.zero,
                        title: Text(
                          'Female',
                          style: h4.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10),

                /// profile picture
                Text(
                  'Profile Picture: ',
                  style: h4,
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    KProfileImage(
                      height: 60,
                      width: 60,
                      imgURL: '',
                    ),
                    const SizedBox(width: 15),
                    KOutlinedButton(
                      onPressed: () async {
                        final img = await ImageServices.galleryImage();
                        var imageFile = await ImageServices.getImageFile(img);

                        /// TODO: apply your method with this imageFile
                      },
                      width: 80,
                      borderColor: mainColor,
                      child: Text(
                        'Browse Photo',
                        style: h4.copyWith(
                          color: mainColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContactInformation() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      elevation: 4,
      margin: const EdgeInsets.all(15),
      child: Column(
        children: [
          const CardTitleBuilder(
            icon: Icons.perm_phone_msg_outlined,
            title: 'Contact Information',
          ),
          Container(
            color: kWhite,
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// mail text field
                TextFormField(
                  readOnly: true,
                  decoration: InputDecoration(
                    isDense: true,
                    border: const OutlineInputBorder(),
                    hintText: 'dummyemail@gmail.com', // enter user email here
                  ),
                ),
                const SizedBox(height: 10),

                /// phone text field
                TextFormField(
                  readOnly: true,
                  decoration: InputDecoration(
                    isDense: true,
                    border: const OutlineInputBorder(),
                    hintText: '018811111111', // enter user phone number here
                  ),
                ),
                const SizedBox(height: 15),

                /// change mobile number text button
                isWantToChangePhoneNumber
                    ? Form(
                        key: changeMobileNumberFormKey,
                        child: Column(
                          children: [
                            PhoneTextFieldBuilder(
                              controller: phoneTextController,
                              onChanged: (value) {
                                setState(() {
                                  if (value.toString().length ==
                                      selectedCountry.maxPhoneNoLength) {
                                    isPhoneTextFieldValid = true;
                                  } else {
                                    isPhoneTextFieldValid = false;
                                  }
                                });
                              },
                              isValid: isPhoneTextFieldValid,
                              maxLength: selectedCountry.maxPhoneNoLength,
                              selectedCountry: selectedCountry,
                              textInputAction: TextInputAction.done,
                              onTapPrefixIcon: () async {
                                var result = await selectCountryCode(context);
                                if (result != null) {
                                  setState(() {
                                    selectedCountry = result;
                                  });
                                }
                              },
                            ),
                            const SizedBox(height: 20),
                            isOTPSend
                                ? IntrinsicHeight(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Expanded(
                                          child: TextFormField(
                                            controller: otpTextController,
                                            validator: (value) {
                                              if (value.toString().isEmpty) {
                                                return 'OTP is Empty!';
                                              } else if (value.toString().length < 6) {
                                                return 'Invalid OTP!';
                                              }
                                              return null;
                                            },
                                            decoration: const InputDecoration(
                                              isDense: true,
                                              contentPadding: EdgeInsets.symmetric(
                                                horizontal: 12,
                                                vertical: 12,
                                              ),
                                              border: OutlineInputBorder(),
                                              hintText: 'Enter OTP',
                                            ),
                                          ),
                                        ),
                                        const SizedBox(width: 15),
                                        Expanded(
                                          child: KOutlinedButton(
                                            onPressed: () {
                                              if (changeMobileNumberFormKey.currentState!.validate()) {
                                                /// TODO: Your Methods
                                              }
                                            },
                                            borderColor: mainColor,
                                            child: Text(
                                              'Confirm OTP',
                                              style: h4.copyWith(
                                                color: mainColor,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                )
                                : KOutlinedButton(
                                    onPressed: () {
                                      if (changeMobileNumberFormKey
                                          .currentState!
                                          .validate()) {
                                        setState(() {
                                          isOTPSend = true;
                                        });
                                      }
                                    },
                                    borderColor: mainColor,
                                    child: Text(
                                      'Send OTP',
                                      style: h4.copyWith(
                                        color: mainColor,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      )
                    : Container(),

                /// editable phone text field
                isWantToChangePhoneNumber
                    ? Container()
                    : GestureDetector(
                        onTap: () {
                          setState(() {
                            isWantToChangePhoneNumber = true;
                          });
                        },
                        child: Text(
                          'Change Mobile Number',
                          style: h4.copyWith(
                            color: mainColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPassword() {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      elevation: 4,
      margin: const EdgeInsets.all(15),
      child: Column(
        children: [
          const CardTitleBuilder(
            icon: Icons.lock,
            title: 'Password',
          ),
          Container(
            color: kWhite,
            padding: const EdgeInsets.all(15),
            child: Form(
              key: changePasswordFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// current password text field and change password text button
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          controller: currentPasswordTextController,
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.toString().isEmpty) {
                              return '8 characters needed.';
                            } else if (value.toString().length < 8) {
                              return '8 characters needed.';
                            }
                            return null;
                          },
                          readOnly: isWantToChangePassword ? false : true,
                          decoration: InputDecoration(
                            isDense: true,
                            border: const OutlineInputBorder(),
                            hintText: isWantToChangePassword
                                ? 'Current Password'
                                : '********',
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              isWantToChangePassword = true;
                            });
                          },
                          child: Text(
                            'Change Password',
                            style: h4.copyWith(
                              color: mainColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),

                  /// new and confirm password text field
                  isWantToChangePassword
                      ? IntrinsicHeight(
                        child: Row(
                            children: [
                              Expanded(
                                child: TextFormField(
                                  controller: newPasswordTextController,
                                  textInputAction: TextInputAction.next,
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return '8 characters needed.';
                                    } else if (value.toString().length < 8) {
                                      return '8 characters needed.';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                    isDense: true,
                                    border: OutlineInputBorder(),
                                    hintText: 'New Password',
                                  ),
                                ),
                              ),
                              const SizedBox(width: 10),
                              Expanded(
                                child: TextFormField(
                                  controller: confirmPasswordTextController,
                                  textInputAction: TextInputAction.done,
                                  validator: (value) {
                                    if (value.toString().isEmpty) {
                                      return '8 characters needed.';
                                    } else if (value.toString().length < 8) {
                                      return '8 characters needed.';
                                    } else if (value.toString() !=
                                        newPasswordTextController.text) {
                                      return 'Not matched.';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                    isDense: true,
                                    border: OutlineInputBorder(),
                                    hintText: 'Confirm Password',
                                  ),
                                ),
                              ),
                            ],
                          ),
                      )
                      : Container(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
