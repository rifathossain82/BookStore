import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/customer_features/author/presentation/widgets/authors_vertical_list_view_builder.dart';
import 'package:book_store/src/customer_features/author/presentation/widgets/top_authors_horizontal_list_view_builder.dart';
import 'package:flutter/material.dart';

class AuthorPage extends StatefulWidget {
  const AuthorPage({Key? key}) : super(key: key);

  @override
  State<AuthorPage> createState() => _AuthorPageState();
}

class _AuthorPageState extends State<AuthorPage> {
  bool _isSearch = false;
  TextEditingController searchTextController = TextEditingController();

  void _startSearch() {
    setState(() {
      _isSearch = true;
    });
  }

  void _stopSearch() {
    setState(() {
      _isSearch = false;
      searchTextController.clear();
    });
  }

  Widget _buildSearchField() {
    return TextField(
      controller: searchTextController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: 'Search your favorite author',
        border: InputBorder.none,
        hintStyle: h4.copyWith(
          color: kGrey,
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Text(
      'AUTHOR',
      style: h2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: _isSearch ? _buildSearchField() : _buildTitle(),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              _isSearch ? Icons.close : Icons.search,
            ),
            onPressed: () {
              if (_isSearch) {
                _stopSearch();
              } else {
                _startSearch();
              }
            },
          )
        ],
      ),
      body: Column(
        children: const [
          TopAuthorsHorizontalListViewBuilder(),
          Expanded(
            child: AuthorsVerticalListViewBuilder(),
          ),
        ],
      ),
    );
  }
}
