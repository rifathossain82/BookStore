import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/customer_features/author/presentation/widgets/author_card_builder.dart';
import 'package:book_store/src/customer_features/category/presentation/widgets/book_item_builder_with_cart_button.dart';
import 'package:book_store/src/customer_features/category/presentation/widgets/filter_checkbox_tile_builder.dart';
import 'package:book_store/src/customer_features/category/presentation/widgets/filter_search_field_builder.dart';
import 'package:flutter/material.dart';

class AuthorBookListPage extends StatefulWidget {
  const AuthorBookListPage({Key? key}) : super(key: key);

  @override
  State<AuthorBookListPage> createState() => _AuthorBookListPageState();
}

class _AuthorBookListPageState extends State<AuthorBookListPage> {

  /// received from parent page
  FakeAuthorData? authorData;

  String? selectedSort;
  int selectedFilterTypeIndex = 0;

  final authorTextController = TextEditingController();
  final publisherTextController = TextEditingController();

  RangeValues priceRangeValues = RangeValues(0, 1620);
  RangeValues discountRangeValues = RangeValues(0, 50);

  final List<String> selectedAuthorList = [];
  final List<String> selectedCategoryList = [];
  final List<String> selectedPublisherList = [];

  /// dummy data
  List<String> sortList = [
    'Best Seller',
    'New Released',
    'Price - Low to High',
    'Price - High to Low',
    'Discount - Low to High',
    'Discount - High to Low',
  ];

  final List<String> filterTypes = [
    'Authors',
    'Categories',
    'Publishers',
  ];

  final List<String> authorList = [
    'Jony',
    'Niloy',
    'Rahat',
    'Kawchar',
    'Akash',
    'Arafat',
    'Shakil',
    'Nayeem',
  ];

  final List<String> categoryList = [
    'ড্রয়িং',
    'শিল্প',
    'চিত্র',
    'মটিভেশনাল',
    'ফটোগ্রাফি',
  ];

  final List<String> publisherList = [
    'আমার প্রকাশনী',
    'ছয়াকবি',
    'মক্তদেশ',
    'যুক্ত প্রকাশনী',
    'মতওয়ারি',
  ];

  @override
  void didChangeDependencies() {
    authorData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            size: 30,
          ),
        ),
        centerTitle: true,
        title: Text(
          authorData!.name ?? "",
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(50),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              border: Border.symmetric(
                horizontal: BorderSide(
                  color: kGreyMedium,
                  width: 1,
                ),
              ),
            ),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () => openSortDialog(context),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.sort,
                            color: kBlackLight.withOpacity(0.6),
                          ),
                          const SizedBox(width: 5),
                          Text(
                            'Sort',
                            style: h3.copyWith(
                              color: kBlackLight.withOpacity(0.6),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  VerticalDivider(color: kGreyMedium, thickness: 1),
                  Expanded(
                    child: GestureDetector(
                      onTap: () => openFilterDialog(context),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.filter_list,
                            color: kBlackLight.withOpacity(0.6),
                          ),
                          const SizedBox(width: 5),
                          Text(
                            'Filter',
                            style: h3.copyWith(
                              color: kBlackLight.withOpacity(0.6),
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AuthorCardBuilder(authorData: authorData!),
            GridView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 15,
                crossAxisSpacing: 20,
                childAspectRatio: 1 / 1.8,
              ),
              padding: const EdgeInsets.only(
                top: 12,
                left: 8,
                right: 8,
                bottom: 20,
              ),
              itemCount: fakeBookList.length,
              itemBuilder: (context, index) => BookItemBuilderWithCartButton(
                bookData: fakeBookList[index],
              ),
            ),
          ],
        ),
      ),
    );
  }

  openSortDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              height: 430,
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Sort',
                    textAlign: TextAlign.center,
                    style: h2.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  KDivider(
                    color: kGrey,
                  ),
                  ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: sortList.length,
                    itemBuilder: (context, index) => RadioListTile(
                      onChanged: (value) {
                        setState(() {
                          selectedSort = sortList[index];
                        });

                        /// close the dialog
                        Navigator.pop(context);
                      },
                      value: sortList[index],
                      visualDensity: const VisualDensity(
                        vertical: VisualDensity.minimumDensity,
                        horizontal: VisualDensity.minimumDensity,
                      ),
                      groupValue: selectedSort,
                      dense: true,
                      title: Text(
                        sortList[index],
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: h4,
                      ),
                    ),
                    separatorBuilder: (context, index) => KDivider(
                      color: kGrey,
                    ),
                  ),
                  KDivider(
                    color: kGrey,
                  ),
                  const SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      /// reset button
                      KButton(
                        onPressed: () {
                          /// close the bottom sheet
                          Navigator.pop(context);
                        },
                        borderRadius: 20,
                        bgColor: kGreyMedium.withOpacity(0.5),
                        height: 38,
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                          'Reset',
                          style: h3.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kBlackLight,
                          ),
                        ),
                      ),

                      const SizedBox(width: 15),

                      /// cancel button
                      KButton(
                        onPressed: () {
                          /// close the bottom sheet
                          Navigator.pop(context);
                        },
                        borderRadius: 20,
                        height: 38,
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Text(
                          'Cancel',
                          style: h3.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kWhite,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }

  openFilterDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          /// filter widgets
          final List<Widget> filterWidgets = [
            _buildAuthorFilterWidget(setState),
            _buildCategoryFilterWidget(setState),
            _buildPublisherFilterWidget(setState),
          ];

          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              padding: const EdgeInsets.only(top: 16),
              height: context.screenHeight,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// heading and close button
                  Container(
                    height: 40,
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          child: IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: Icon(
                              Icons.close,
                              color: kBlackLight,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Filter',
                              textAlign: TextAlign.center,
                              style: h2.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  const Divider(
                    height: 0,
                  ),

                  /// content
                  Expanded(
                    child: Column(
                      children: [

                        /// filter content
                        Expanded(
                          flex: 10,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              /// side navigation
                              SizedBox(
                                width: 120,
                                height: double.infinity,
                                child: SingleChildScrollView(
                                  physics: const BouncingScrollPhysics(),
                                  child: Column(
                                    children: List.generate(
                                      filterTypes.length,
                                          (index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              selectedFilterTypeIndex = index;
                                            });
                                          },
                                          child: Container(
                                            height: 40,
                                            width: 100,
                                            margin: const EdgeInsets.only(
                                                bottom: 10),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(4),
                                              color: selectedFilterTypeIndex ==
                                                  index
                                                  ? mainColor
                                                  : Colors.transparent,
                                            ),
                                            child: Text(
                                              filterTypes[index],
                                              textAlign: TextAlign.start,
                                              style: h3.copyWith(
                                                color:
                                                selectedFilterTypeIndex ==
                                                    index
                                                    ? kWhite
                                                    : kBlackLight,
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    ).toList(),
                                  ),
                                ),
                              ),

                              /// filter widget based on selected side navigation value
                              Expanded(
                                child: Container(
                                  height: double.infinity,
                                  color: kGreyMedium.withOpacity(0.2),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 15,
                                  ),
                                  child: filterWidgets[selectedFilterTypeIndex],
                                ),
                              ),
                            ],
                          ),
                        ),

                        /// apply button
                        Expanded(
                          flex: 1,
                          child: KButton(
                            height: 50,
                            onPressed: () {},
                            bgColor: kOrange,
                            child: Text(
                              'Apply',
                              style: h3.copyWith(
                                fontWeight: FontWeight.bold,
                                color: kWhite,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }

  /// setState for bottom sheet state
  Widget _buildAuthorFilterWidget(Function(Function()) setState) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 15),
          FilterSearchFieldBuilder(
            controller: authorTextController,
          ),
          SizedBox(
            height: 250,
            child: Scrollbar(
              thickness: 6,
              thumbVisibility: true,
              trackVisibility: true,
              radius: const Radius.circular(10),
              child: ListView.builder(
                itemCount: authorList.length,
                itemBuilder: (context, index) => FilterCheckboxTileBuilder(
                  onChanged: (value) {
                    setState(() {
                      if (value!) {
                        selectedAuthorList.add(authorList[index]);
                      } else {
                        selectedAuthorList.remove(authorList[index]);
                      }
                    });
                  },
                  value: selectedAuthorList.contains(authorList[index]),
                  title: authorList[index],
                ),
              ),
            ),
          ),
          const SizedBox(height: 10),
          _buildPriceAndDiscountRangeSliders(setState)
        ],
      ),
    );
  }

  Widget _buildCategoryFilterWidget(Function(Function()) setState) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 15),
          SizedBox(
            height: 250,
            child: Scrollbar(
              thickness: 6,
              thumbVisibility: true,
              trackVisibility: true,
              radius: const Radius.circular(10),
              child: ListView.builder(
                itemCount: categoryList.length,
                itemBuilder: (context, index) => FilterCheckboxTileBuilder(
                  onChanged: (value) {
                    setState(() {
                      if (value!) {
                        selectedCategoryList.add(categoryList[index]);
                      } else {
                        selectedCategoryList.remove(categoryList[index]);
                      }
                    });
                  },
                  value: selectedCategoryList.contains(categoryList[index]),
                  title: categoryList[index],
                ),
              ),
            ),
          ),
          const SizedBox(height: 10),
          _buildPriceAndDiscountRangeSliders(setState)
        ],
      ),
    );
  }

  Widget _buildPublisherFilterWidget(Function(Function()) setState) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 15),
          FilterSearchFieldBuilder(
            controller: publisherTextController,
          ),
          SizedBox(
            height: 250,
            child: Scrollbar(
              thickness: 6,
              thumbVisibility: true,
              trackVisibility: true,
              radius: const Radius.circular(10),
              child: ListView.builder(
                itemCount: publisherList.length,
                itemBuilder: (context, index) => FilterCheckboxTileBuilder(
                  onChanged: (value) {
                    setState(() {
                      if (value!) {
                        selectedPublisherList.add(publisherList[index]);
                      } else {
                        selectedPublisherList.remove(publisherList[index]);
                      }
                    });
                  },
                  value: selectedPublisherList.contains(publisherList[index]),
                  title: publisherList[index],
                ),
              ),
            ),
          ),
          const SizedBox(height: 10),
          _buildPriceAndDiscountRangeSliders(setState),
        ],
      ),
    );
  }

  Widget _buildPriceAndDiscountRangeSliders(Function(Function()) setState) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        /// by price
        Text(
          'By Price',
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10),
        RangeSlider(
          values: priceRangeValues,
          min: 0,
          max: 1620,
          labels: RangeLabels(
            '${priceRangeValues.start.round()}',
            '${priceRangeValues.end.round()}',
          ),
          onChanged: (RangeValues values) {
            setState(() {
              priceRangeValues = values;
            });
          },
          divisions: 1620,
          activeColor: kBlue,
          inactiveColor: kGrey,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Tk. ${priceRangeValues.start}',
              style: h5,
            ),
            Text(
              'Tk. ${priceRangeValues.end}',
              style: h5,
            ),
          ],
        ),

        /// by discount
        Text(
          'By Discount',
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        RangeSlider(
          values: discountRangeValues,
          min: 0,
          max: 50,
          labels: RangeLabels(
            '${discountRangeValues.start.round()}',
            '${discountRangeValues.end.round()}',
          ),
          onChanged: (RangeValues values) {
            setState(() {
              discountRangeValues = values;
            });
          },
          divisions: 50,
          activeColor: kBlue,
          inactiveColor: kGrey,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Tk. ${discountRangeValues.start}',
              style: h5,
            ),
            Text(
              'Tk. ${discountRangeValues.end}',
              style: h5,
            ),
          ],
        ),
      ],
    );
  }
}
