import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/author/presentation/widgets/top_author_item_builder.dart';
import 'package:flutter/material.dart';

class TopAuthorsHorizontalListViewBuilder extends StatelessWidget {
  const TopAuthorsHorizontalListViewBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 30,
                width: 5,
                color: mainColor,
              ),
              const SizedBox(width: 8),
              Text(
                'Top Authors',
                style: h3.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
          const SizedBox(height: 15),
          Expanded(
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: fakeAuthorList.length,
              itemBuilder: (context, index) => TopAuthorItemBuilder(
                authorData: fakeAuthorList[index],
              ),
              separatorBuilder: (context, index) => const SizedBox(
                width: 15,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
