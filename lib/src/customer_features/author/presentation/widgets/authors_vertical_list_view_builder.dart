import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/customer_features/author/presentation/widgets/author_item_builder.dart';
import 'package:flutter/material.dart';

class AuthorsVerticalListViewBuilder extends StatelessWidget {
  const AuthorsVerticalListViewBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: ListView.separated(
        itemCount: fakeAuthorList.length,
        itemBuilder: (context, index) => AuthorItemBuilder(
          authorData: fakeAuthorList[index],
        ),
        separatorBuilder: (context, index) => const SizedBox(
          height: 15,
        ),
      ),
    );
  }
}
