import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:flutter/material.dart';

class AuthorItemBuilder extends StatelessWidget {
  final FakeAuthorData authorData;

  const AuthorItemBuilder({
    Key? key,
    required this.authorData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.authorBookListPage,
        arguments: authorData,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          KProfileImage(
            height: 75,
            width: 75,
            imgURL: authorData.image,
          ),
          const SizedBox(width: 15),
          Expanded(
            child: Text(
              authorData.name,
              textAlign: TextAlign.start,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: h4.copyWith(),
            ),
          )
        ],
      ),
    );
  }
}
