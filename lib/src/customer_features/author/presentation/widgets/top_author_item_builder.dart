import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:flutter/material.dart';

class TopAuthorItemBuilder extends StatelessWidget {
  final FakeAuthorData authorData;

  const TopAuthorItemBuilder({
    Key? key,
    required this.authorData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.authorBookListPage,
        arguments: authorData,
      ),
      child: SizedBox(
        width: 100,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            KProfileImage(
              height: 75,
              width: 75,
              imgURL: authorData.image,
            ),
            const SizedBox(height: 8),
            Text(
              authorData.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: h4.copyWith(
              ),
            )
          ],
        ),
      ),
    );
  }
}
