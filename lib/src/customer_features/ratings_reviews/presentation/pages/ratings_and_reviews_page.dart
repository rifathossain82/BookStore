import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/customer_features/ratings_reviews/presentation/widgets/reviewed_items_widget.dart';
import 'package:flutter/material.dart';

class RatingsAndReviewsPage extends StatefulWidget {
  const RatingsAndReviewsPage({Key? key}) : super(key: key);

  @override
  State<RatingsAndReviewsPage> createState() => _RatingsAndReviewsPageState();
}

class _RatingsAndReviewsPageState extends State<RatingsAndReviewsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('My Reviews & Ratings'),
        centerTitle: true,
        elevation: 2,
      ),
      body: ReviewedItemsWidget(),
    );
  }
}
