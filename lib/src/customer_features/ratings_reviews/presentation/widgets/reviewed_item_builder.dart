import 'package:book_store/main.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/star_rating_builder.dart';
import 'package:book_store/src/customer_features/ratings_reviews/presentation/widgets/edit_review_dialog.dart';
import 'package:flutter/material.dart';

class ReviewedItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const ReviewedItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 4,
          child: ProductImageBuilder(
            height: 140,
            width: 100,
            imgUrl: bookData.image!,
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          flex: 6,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                bookData.name!,
                style: h4,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                bookData.author!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: h5.copyWith(
                  color: kGrey,
                ),
              ),
              const SizedBox(height: 5),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  StarRatingBuilder(
                    color: kGrey,
                    starCount: 5,
                    rating: 5, // to show filled star
                    starSize: 20,
                  ),
                  const SizedBox(width: 10),
                  Text(
                    /// show your ratings
                    '0.0',
                    style: h3.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 2),
              Text(
                /// show your reviews
                'good',
                maxLines: 3,
                textAlign: TextAlign.start,
                style: h5.copyWith(
                  color: kGrey,
                ),
              ),
              const SizedBox(height: 10),
              KOutlinedButton(
                onPressed: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return EditReviewDialog(
                        previousRating: 3.0,
                        previousComment: 'Good',
                      );
                    },
                  );
                },
                borderColor: mainColor,
                height: 40,
                child: Text(
                  'Edit Review',
                  style: h5.copyWith(
                    color: mainColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
