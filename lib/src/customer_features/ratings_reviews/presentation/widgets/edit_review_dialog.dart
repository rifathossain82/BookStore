import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/star_rating_builder.dart';
import 'package:flutter/material.dart';

class EditReviewDialog extends StatefulWidget {
  final double previousRating;
  final String previousComment;

  const EditReviewDialog({
    Key? key,
    required this.previousRating,
    required this.previousComment,
  }) : super(key: key);

  @override
  _EditReviewDialogState createState() => _EditReviewDialogState();
}

class _EditReviewDialogState extends State<EditReviewDialog> {
  double rating = 0.0;
  final commentTextController = TextEditingController();

  @override
  void initState() {
    rating = widget.previousRating;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Write your Ratings & Comments'),
      actionsPadding: const EdgeInsets.all(15),
      scrollable: true,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const KDivider(height: 0),
          const SizedBox(height: 16.0),
          Align(
            alignment: Alignment.center,
            child: StarRatingBuilder(
              color: kOrange,
              rating: rating,
              blankStarColor: kGrey,
              starCount: 5,
              starSize: 40,
              onRatingChanged: (value) {
                setState(() {
                  rating = value;
                });
              },
            ),
          ),
          const SizedBox(height: 16.0),
          TextFormField(
            controller: commentTextController,
            decoration: InputDecoration(
              hintText: widget.previousComment ?? 'Enter your comment...',
              border: const OutlineInputBorder(),
            ),
            minLines: 4,
            maxLines: null,
          ),
        ],
      ),
      actions: [
        KButton(
          onPressed: (){
            /// Perform your methods
            Navigator.pop(context);
          },
          child: Text(
            'Submit',
            style: h4.copyWith(
              color: kWhite,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
