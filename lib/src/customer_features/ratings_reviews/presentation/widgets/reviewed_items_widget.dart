import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/customer_features/ratings_reviews/presentation/widgets/reviewed_item_builder.dart';
import 'package:flutter/material.dart';

class ReviewedItemsWidget extends StatelessWidget {
  ReviewedItemsWidget({Key? key}) : super(key: key);

  final reviewedItems = [
    fakeBookList[0],
    fakeBookList[1],
    fakeBookList[2],
    fakeBookList[3],
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 15,
      ),
      itemCount: reviewedItems.length,
      itemBuilder: (context, index) => ReviewedItemBuilder(
        bookData: reviewedItems[index],
      ),
      separatorBuilder: (context, index) => KDivider(
        height: 30,
        color: kGreyMedium,
      ),
    );
  }
}
