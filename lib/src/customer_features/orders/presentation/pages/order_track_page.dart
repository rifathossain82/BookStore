import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/order_track_stepper_item_builder.dart';
import 'package:flutter/material.dart';

class OrderTrackPage extends StatefulWidget {
  const OrderTrackPage({Key? key}) : super(key: key);

  @override
  State<OrderTrackPage> createState() => _OrderTrackPageState();
}

class _OrderTrackPageState extends State<OrderTrackPage> {
  /// receive from my parent page
  FakeOrderData? orderData;

  @override
  void didChangeDependencies() {
    orderData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        title: const Text('Order Track'),
        elevation: 2,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [

            /// order id and stepper of order status
            _buildOrderTrackCard(),

            /// delivery address
            _buildDeliveryAddressWidget(),
          ],
        ),
      ),
    );
  }

  Widget _buildOrderTrackCard(){
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          /// date and order id text
          _buildDateAndOrderIDText(),

          const KDivider(height: 20),

          /// order status
          _buildOrderStatusStepperWidget(),
        ],
      ),
    );
  }

  Widget _buildDateAndOrderIDText(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Wed, 12 Feb',
          style: h4.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          'Order ID: ${orderData!.id}',
          style: h4.copyWith(
            fontWeight: FontWeight.bold,
            color: kGreyTextColor,
          ),
        ),
      ],
    );
  }

  Widget _buildOrderStatusStepperWidget(){
    return Column(
      children: [
        OrderTrackStepperItemBuilder(
          leadingAssetIcon: AssetPath.aboutUs,
          title: 'অর্ডার গ্রহণ করা হয়েছে।',
          subtitle: 'Wed, 12 Feb 2023 02:23 PM',
          isCompleted: true,
          isFirstStepperItem:
          true, // since it's the first stepper so we don't need the divider between 2 stepper item
        ),
        OrderTrackStepperItemBuilder(
          leadingAssetIcon: AssetPath.aboutUs,
          title: 'অর্ডার প্রস্তুতিকরণ চলছে।',
          subtitle: 'Wed, 12 Feb 2023 04:03 PM',
          isCompleted: true,
        ),
        OrderTrackStepperItemBuilder(
          leadingAssetIcon: AssetPath.aboutUs,
          title: 'অর্ডার প্রস্তুত হয়েছে।',
          subtitle: 'Wed, 12 Feb 2023 04:03 PM',
          isCompleted: true,
        ),
        OrderTrackStepperItemBuilder(
          leadingAssetIcon: AssetPath.deliveryIcon,
          title: 'অর্ডার করিয়ার এর কাছে।',
          subtitle: 'Wed, 12 Feb 2023 04:03 PM',
          isCompleted: true,
        ),
        OrderTrackStepperItemBuilder(
          leadingAssetIcon: AssetPath.deliveryIcon,
          title: 'সম্ভাব্য ডেলিভারি।',
          subtitle: 'Wed, 13 Feb 2023 04:03 PM',
          isCompleted: false,
        ),
      ],
    );
  }

  Widget _buildDeliveryAddressWidget() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: ListTile(
        leading: const Icon(
          Icons.home_outlined,
          size: 40,
        ),
        title: const Text('Delivery Address'),
        contentPadding: EdgeInsets.zero,
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 4),
            Text(
              'Uttara, Dhaka, Bangladesh',
              style: h5.copyWith(
                color: kGrey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// old order track page
// import 'package:book_store/src/core/extensions/build_context_extension.dart';
// import 'package:book_store/src/core/fake_data/fake_data.dart';
// import 'package:book_store/src/core/utils/app_constants.dart';
// import 'package:book_store/src/core/utils/asset_path.dart';
// import 'package:book_store/src/core/utils/color.dart';
// import 'package:book_store/src/core/utils/styles.dart';
// import 'package:book_store/src/core/widgets/dashed_line_painter.dart';
// import 'package:book_store/src/core/widgets/k_divider.dart';
// import 'package:book_store/src/core/widgets/row_text.dart';
// import 'package:book_store/src/customer_features/orders/presentation/widgets/order_track_book_item_builder.dart';
// import 'package:flutter/material.dart';
//
// class OrderTrackPage extends StatefulWidget {
//   const OrderTrackPage({Key? key}) : super(key: key);
//
//   @override
//   State<OrderTrackPage> createState() => _OrderTrackPageState();
// }
//
// class _OrderTrackPageState extends State<OrderTrackPage> {
//   /// receive from my parent page
//   FakeOrderData? orderData;
//
//   @override
//   void didChangeDependencies() {
//     orderData = context.getArguments;
//     super.didChangeDependencies();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Order Track'),
//         centerTitle: true,
//         elevation: 2,
//       ),
//       floatingActionButton: FloatingActionButton.small(
//         onPressed: () {},
//         child: const Icon(
//           Icons.message,
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: Stack(
//           alignment: Alignment.center,
//           children: [
//             Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Container(
//                   height: 250,
//                   width: context.screenWidth,
//                   alignment: Alignment.center,
//                   padding: const EdgeInsets.all(20),
//                   decoration: BoxDecoration(
//                     gradient: LinearGradient(
//                       begin: Alignment.centerLeft,
//                       end: Alignment.centerRight,
//                       colors: [
//                         kOrange,
//                         kDeepOrange,
//                       ],
//                     ),
//                   ),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       const SizedBox(height: 20),
//                       Text(
//                         'Order Track',
//                         style: h2.copyWith(
//                           color: kWhite,
//                           fontWeight: FontWeight.w700,
//                         ),
//                       ),
//                       const SizedBox(height: 5),
//                       Text(
//                         'Mar 28, 2023',
//                         style: h1.copyWith(
//                           color: kWhite,
//                           fontWeight: FontWeight.w700,
//                         ),
//                       ),
//                       Text(
//                         'Estimate Delivery Date',
//                         style: h3.copyWith(
//                           color: kWhite,
//                           fontWeight: FontWeight.w700,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   width: context.screenWidth,
//                   color: kGreyLight,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       const SizedBox(height: 115),
//                       _buildOrderSummery(),
//                       _buildDeliveryAddress(),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//             Positioned(
//               top: 175,
//               child: _buildOrderStatus(),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildOrderSummery() {
//     return Container(
//       margin: const EdgeInsets.symmetric(
//         horizontal: 15,
//         vertical: 8,
//       ),
//       padding: const EdgeInsets.all(15),
//       decoration: BoxDecoration(
//         color: kWhite,
//         borderRadius: BorderRadius.circular(
//           10,
//         ),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisSize: MainAxisSize.min,
//         children: [
//           /// order summary title text
//           Text(
//             'Order Summary',
//             style: h3.copyWith(
//               fontWeight: FontWeight.w700,
//             ),
//           ),
//           const KDivider(height: 20),
//
//           /// list of ordered books
//           ListView.separated(
//             shrinkWrap: true,
//             physics: const NeverScrollableScrollPhysics(),
//             itemCount: orderData!.books.length,
//             itemBuilder: (context, index) => OrderTrackBookItemBuilder(
//               bookData: orderData!.books[index],
//             ),
//             separatorBuilder: (context, index) => const KDivider(
//               height: 20,
//             ),
//           ),
//           const KDivider(height: 20),
//
//           /// total items, sub-total, shipping charge, payable total etc
//           const SizedBox(height: 10),
//           RowText(
//             title: 'Total Items',
//             titleStyle: h4.copyWith(
//               color: kGrey,
//             ),
//             paddingBottom: 5,
//             value: '${orderData!.books.length} products(s)',
//           ),
//           RowText(
//             title: 'Sub-total',
//             titleStyle: h4.copyWith(
//               color: kGrey,
//             ),
//             paddingBottom: 5,
//             value: '${AppConstants.currency} 150',
//           ),
//           RowText(
//             title: 'Shipping Charge',
//             titleStyle: h4.copyWith(
//               color: kGrey,
//             ),
//             paddingBottom: 5,
//             value: '${AppConstants.currency} 48',
//           ),
//           const SizedBox(height: 8),
//           SizedBox(
//             width: context.screenWidth,
//             child: CustomPaint(
//               painter: DashedLinePainter(),
//             ),
//           ),
//           const SizedBox(height: 8),
//           RowText(
//             title: 'Payable Total',
//             titleStyle: h3.copyWith(
//               fontWeight: FontWeight.w700,
//             ),
//             paddingBottom: 5,
//             value: '${AppConstants.currency} 0',
//           ),
//         ],
//       ),
//     );
//   }
//
//   Widget _buildDeliveryAddress() {
//     return Container(
//       margin: const EdgeInsets.symmetric(
//         horizontal: 15,
//         vertical: 8,
//       ),
//       padding: const EdgeInsets.all(15),
//       decoration: BoxDecoration(
//         color: kWhite,
//         borderRadius: BorderRadius.circular(
//           10,
//         ),
//       ),
//       child: ListTile(
//         leading: Icon(
//           Icons.location_on,
//           color: kGrey,
//           size: 35,
//         ),
//         title: const Text('Delivery Address'),
//         contentPadding: EdgeInsets.zero,
//         subtitle: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             const SizedBox(height: 4),
//             Text(
//               'Uttara, Dhaka, Bangladesh',
//               style: h5.copyWith(
//                 color: kGrey,
//               ),
//             ),
//             const SizedBox(height: 4),
//             Text(
//               'Phone: 8801885256220',
//               style: h5.copyWith(
//                 color: kGrey,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildOrderStatus() {
//     return Container(
//       width: context.screenWidth - 30,
//       height: 170,
//       margin: const EdgeInsets.symmetric(
//         vertical: 8,
//       ),
//       padding: const EdgeInsets.all(15),
//       decoration: BoxDecoration(
//         color: kWhite,
//         borderRadius: BorderRadius.circular(
//           10,
//         ),
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Text(
//                 'Order Number:',
//                 style: h4,
//               ),
//               Text(
//                 orderData!.id,
//                 style: h4,
//               ),
//             ],
//           ),
//           const KDivider(height: 20),
//           ListTile(
//             leading: CircleAvatar(
//               backgroundImage: AssetImage(AssetPath.profile), // give image based on order status
//             ),
//             title: const Text('Order Placed'),
//             contentPadding: EdgeInsets.zero,
//             subtitle: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 const SizedBox(height: 4),
//                 Text(
//                   'Mar 27, 2023 09:31 AM',
//                   style: h5.copyWith(
//                     color: kGrey,
//                   ),
//                 ),
//                 const SizedBox(height: 4),
//                 Text(
//                   'আর্ডারটি গ্রহন করা হয়েছে। কনফার্মেশনের জন্য অপেক্ষামান।',
//                   style: h5.copyWith(
//                     color: kGrey,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
