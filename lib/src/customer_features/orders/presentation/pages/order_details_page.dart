import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/app_expansion_tile.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/row_text.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/order_details_book_item_builder.dart';
import 'package:flutter/material.dart';

class OrderDetailsPage extends StatefulWidget {
  const OrderDetailsPage({Key? key}) : super(key: key);

  @override
  State<OrderDetailsPage> createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends State<OrderDetailsPage> {
  /// receive from my parent page
  FakeOrderData? orderData;

  ///to expand and hide order
  bool isHideExpansionTile = false;

  @override
  void didChangeDependencies() {
    orderData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Order Details'),
        centerTitle: true,
        elevation: 2,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(
            top: 15,
            left: 15,
            right: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Order ID #${orderData!.id} (${orderData!.books.length} Items)',
                style: h4.copyWith(
                  color: kGrey,
                ),
              ),
              _buildItemShowingExpansionTile(),
              const KDivider(height: 20),
              RowText(
                title: 'Total Items',
                titleStyle: h4.copyWith(
                  color: kGrey,
                ),
                paddingBottom: 5,
                value: '${orderData!.books.length}',
              ),
              RowText(
                title: 'Sub-total',
                titleStyle: h4.copyWith(
                  color: kGrey,
                ),
                paddingBottom: 5,
                value: '${AppConstants.currency} 150',
              ),
              RowText(
                title: 'Shipping Charge',
                titleStyle: h4.copyWith(
                  color: kGrey,
                ),
                paddingBottom: 5,
                value: '${AppConstants.currency} 48',
              ),
              RowText(
                title: 'Discount',
                titleStyle: h4.copyWith(
                  color: kGrey,
                ),
                paddingBottom: 5,
                value: '${AppConstants.currency} 0',
              ),
              RowText(
                title: 'Payable Total',
                titleStyle: h4.copyWith(
                  color: kGrey,
                ),
                paddingBottom: 5,
                value: '${AppConstants.currency} 0',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItemShowingExpansionTile() {
    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: AppExpansionTile(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Order Total',
              style: h4.copyWith(
                color: kGrey,
              ),
            ),
            Text(
              '${AppConstants.currency} 150',

              /// total price
              style: h4.copyWith(
                color: mainColor,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
        trailing: isHideExpansionTile
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Hide Items',
                    style: h5.copyWith(
                      color: kGrey,
                    ),
                  ),
                  const SizedBox(width: 5),
                  Icon(
                    Icons.keyboard_arrow_up,
                    size: 20,
                    color: kBlackLight,
                  ),
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Show Items',
                    style: h5.copyWith(
                      color: kGrey,
                    ),
                  ),
                  const SizedBox(width: 5),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 20,
                    color: kBlackLight,
                  ),
                ],
              ),
        childrenPadding: const EdgeInsets.all(8),
        expandedAlignment: Alignment.centerLeft,
        tilePadding: const EdgeInsets.symmetric(
          horizontal: 0,
          vertical: 5,
        ),
        initiallyExpanded: isHideExpansionTile,
        children: [
          /// list of ordered books
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: orderData!.books.length,
            itemBuilder: (context, index) => OrderDetailsBookItemBuilder(
              bookData: orderData!.books[index],
            ),
            separatorBuilder: (context, index) => const KDivider(
              height: 20,
            ),
          ),
        ],
        onExpansionChanged: (s) {
          setState(() {
            isHideExpansionTile = !isHideExpansionTile;
          });
        },
      ),
    );
  }
}
