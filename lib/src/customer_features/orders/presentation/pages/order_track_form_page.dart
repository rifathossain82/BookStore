import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/select_country_code.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/order_track_phone_text_field_builder.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/order_track_text_form_field_builder.dart';
import 'package:flutter/material.dart';

class OrderTrackFormPage extends StatefulWidget {
  const OrderTrackFormPage({Key? key}) : super(key: key);

  @override
  State<OrderTrackFormPage> createState() => _OrderTrackFormPageState();
}

class _OrderTrackFormPageState extends State<OrderTrackFormPage> {
  final orderNumberTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  FakeCountryInfo selectedCountry = fakeCountryList.first;
  bool isPhoneTextFieldValid = false;
  final trackOrderFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Track Your Order'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Form(
          key: trackOrderFormKey,
          child: Column(
            children: [
              /// order number textField
              OrderTrackTextFormFieldBuilder(
                controller: orderNumberTextController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Order Number required!';
                  }
                  return null;
                },
                labelText: 'Order Number:',
                hintText: 'Enter Order No.',
                maxLine: 1,
                textInputAction: TextInputAction.next,
                textInputType: TextInputType.number,
              ),

              const SizedBox(height: 15),

              /// phone number text field
              OrderTrackPhoneTextFieldBuilder(
                controller: phoneTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().length ==
                        selectedCountry.maxPhoneNoLength) {
                      isPhoneTextFieldValid = true;
                    } else {
                      isPhoneTextFieldValid = false;
                    }
                  });
                },
                isValid: isPhoneTextFieldValid,
                hintText: 'Ex: 171',
                labelText: 'Phone Number:',
                maxLength: selectedCountry.maxPhoneNoLength,
                selectedCountry: selectedCountry,
                onTapPrefixIcon: () async {
                  var result = await selectCountryCode(context);
                  if (result != null) {
                    setState(() {
                      selectedCountry = result;
                    });
                  }
                },
              ),

              /// submit button
              const SizedBox(height: 30),
              KButton(
                onPressed: submitMethod,
                width: context.screenWidth * 0.4,
                child: Text(
                  'Submit',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: kWhite,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void submitMethod() {
    if (trackOrderFormKey.currentState!.validate()) {
      Navigator.pushNamed(
        context,
        RouteGenerator.orderTrack,
        arguments: fakeOrderList[0],
      );
    }
  }
}
