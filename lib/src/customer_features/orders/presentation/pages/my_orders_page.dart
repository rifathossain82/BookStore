import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/my_order_item_builder.dart';
import 'package:flutter/material.dart';

class MyOrdersPage extends StatefulWidget {
  const MyOrdersPage({Key? key}) : super(key: key);

  @override
  State<MyOrdersPage> createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  final orderIdTextController = TextEditingController();

  String selectedOrderStatus = 'Select Any';
  final orderStatus = [
    'Select Any',
    'Processing',
    'Approved',
    'On Shipping',
    'Shipped',
    'Completed',
    'Cancelled',
    'Returned',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        title: const Text('My Orders'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Column(
        children: [
          _buildHeaderWidget(),
          Expanded(
            child: _buildOrderList(),
          ),
        ],
      ),
    );
  }

  Widget _buildHeaderWidget() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 50,
          width: context.screenWidth,
          color: kGreyLight,
          alignment: Alignment.center,
          padding: const EdgeInsets.all(10),
          child: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'Your Total Order: ',
                  style: h4.copyWith(
                    color: kGrey,
                  ),
                ),
                TextSpan(
                  text: '2', // total order count
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          color: kWhite,
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
            vertical: 10,
          ),
          child: Row(
            children: [
              /// order id text and text field
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Order ID:',
                      style: h4.copyWith(
                        color: kGrey,
                      ),
                    ),
                    Container(
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(
                          color: kGrey,
                          width: 1,
                        ),
                      ),
                      child: TextField(
                        controller: orderIdTextController,
                        style: h4,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: const EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 5,
                          ),
                          border: InputBorder.none,
                          labelText: 'Enter Order ID',
                          labelStyle: h5.copyWith(
                            color: kGrey,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              const SizedBox(width: 10),

              /// order status text and drop down
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Order Status:',
                      style: h4.copyWith(
                        color: kGrey,
                      ),
                    ),
                    Container(
                      height: 45,
                      width: double.infinity,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        border: Border.all(
                          color: kGrey,
                          width: 1,
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          items: orderStatus
                              .map(
                                (status) => DropdownMenuItem(
                                  value: status,
                                  child: Text(
                                    status,
                                    style: h4.copyWith(
                                      color: kGrey,
                                    ),
                                  ),
                                ),
                              )
                              .toList(),
                          onChanged: (newStatus) {
                            setState(() {
                              selectedOrderStatus = newStatus!;
                            });
                          },
                          isExpanded: false,
                          value: selectedOrderStatus,
                          icon: const Icon(Icons.keyboard_arrow_down_rounded),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildOrderList() {
    return ListView.builder(
      itemCount: fakeOrderList.length,
      itemBuilder: (context, index) => MyOrderItemBuilder(
        data: fakeOrderList[index],
      ),
    );
  }
}
