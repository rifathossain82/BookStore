import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/customer_features/orders/presentation/widgets/order_book_item_builder.dart';
import 'package:flutter/material.dart';

class MyOrderItemBuilder extends StatelessWidget {
  final FakeOrderData data;

  const MyOrderItemBuilder({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      color: kWhite,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Order ID #${data.id} (${data.books.length} Items)',
            style: h4.copyWith(
              color: kGrey,
            ),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Text(
                'Order Status:',
                style: h4.copyWith(
                  color: kGrey,
                ),
              ),
              const SizedBox(width: 10),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 4,
                ),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: data.status == 'Cancelled'
                      ? kOrange.withOpacity(0.1)
                      : mainColor.withOpacity(0.1),
                ),
                child: Text(
                  data.status,
                  textAlign: TextAlign.center,
                  style: h4,
                ),
              )
            ],
          ),
          const KDivider(height: 15),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: data.books.length,
            itemBuilder: (context, index) => OrderBookItemBuilder(
              bookData: data.books[index],
            ),
            separatorBuilder: (context, index) => const KDivider(height: 20),
          ),
          const KDivider(height: 30),
          data.status == 'Cancelled'
              ? KOutlinedButton(
                  onPressed: () => Navigator.pushNamed(
                    context,
                    RouteGenerator.orderDetails,
                    arguments: data,
                  ),
                  borderColor: mainColor,
                  borderRadius: 30,
                  child: Text(
                    'View Order Detail',
                    style: h4.copyWith(
                      color: mainColor,
                    ),
                  ),
                )
              : Row(
                  children: [
                    Expanded(
                      child: KOutlinedButton(
                        onPressed: () => Navigator.pushNamed(
                          context,
                          RouteGenerator.orderDetails,
                          arguments: data,
                        ),
                        borderColor: mainColor,
                        borderRadius: 30,
                        child: Text(
                          'View Order Detail',
                          style: h4.copyWith(
                            color: mainColor,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: KButton(
                        onPressed: () => Navigator.pushNamed(
                          context,
                          RouteGenerator.orderTrack,
                          arguments: data,
                        ),
                        borderRadius: 30,
                        child: Text(
                          'Track My Order',
                          style: h4.copyWith(
                            color: kWhite,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
        ],
      ),
    );
  }
}
