import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderTrackPhoneTextFieldBuilder extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged onChanged;
  final bool isValid;
  final int maxLength;
  final FakeCountryInfo selectedCountry;
  final VoidCallback onTapPrefixIcon;
  final String hintText;
  final String labelText;

  const OrderTrackPhoneTextFieldBuilder({
    Key? key,
    required this.controller,
    required this.onChanged,
    required this.isValid,
    required this.maxLength,
    required this.selectedCountry,
    required this.onTapPrefixIcon,
    required this.hintText,
    required this.labelText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          labelText,
          style: h4,
        ),
        const SizedBox(height: 5),
        TextFormField(
          controller: controller,
          validator: (value) {
            if (value.toString().isEmpty) {
              return 'Phone Number Required!';
            } else if (value.toString().length < maxLength) {
              return 'Invalid Phone!';
            }
            return null;
          },
          onChanged: onChanged,
          maxLines: 1,
          maxLength: maxLength,
          keyboardType: TextInputType.phone,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: h5.copyWith(
              color: kGrey,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: isValid ? mainColor : kRedDeep,
              ),
            ),
            isDense: true,
            contentPadding: const EdgeInsets.all(8),
            prefixIcon: GestureDetector(
              onTap: onTapPrefixIcon,
              child: Container(
                padding: const EdgeInsets.only(left: 2, right: 8),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.arrow_drop_down,
                      color: Colors.black.withOpacity(0.8),
                    ),
                    const SizedBox(
                      width: 4,
                    ),
                    Image.network(
                      selectedCountry.flagUrl,
                      height: 30,
                      width: 35,
                    ),
                    Text(selectedCountry.code),
                  ],
                ),
              ),
            ),
            suffix: isValid
                ? Container(
              alignment: Alignment.center,
              width: 24.0,
              height: 24.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: mainColor,
              ),
              child: const Icon(
                Icons.check,
                color: Colors.white,
                size: 13,
              ),
            )
                : Container(
              alignment: Alignment.center,
              width: 24.0,
              height: 24.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kRedDeep,
              ),
              child: Icon(
                Icons.close,
                color: kWhite,
                size: 13,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
