import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/material.dart';

class OrderDetailsBookItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const OrderDetailsBookItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: ProductImageBuilder(
              height: 100,
              width: 70,
              imgUrl: bookData.image!,
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: 4,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  bookData.name!,
                  style: h4,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 4),
                Text(
                  'Qty: 1', // enter the quantity of ordered book
                  style: h5,
                ),
                Text(
                  bookData.author!,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: h5.copyWith(
                    color: kGrey,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  '${AppConstants.currency} ${bookData.regularPrice}',
                  style: h4.copyWith(
                    color: kGrey,
                    decoration: TextDecoration.lineThrough,
                  ),
                ),
                Text(
                  '${AppConstants.currency} ${bookData.offerPrice}',
                  style: h4,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
