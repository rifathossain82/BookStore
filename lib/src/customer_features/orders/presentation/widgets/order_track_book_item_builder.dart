import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/material.dart';

class OrderTrackBookItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const OrderTrackBookItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: ProductImageBuilder(
              height: 100,
              width: 70,
              imgUrl: bookData.image!,
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: 7,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  bookData.name!,
                  style: h4,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 4),
                Text(
                  'Qty: 1', // enter the quantity of ordered book
                  style: h5,
                ),
                Text(
                  'Price: ${AppConstants.currency} 150.0',
                  maxLines: 1,
                  style: h5,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
