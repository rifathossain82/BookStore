import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class OrderTrackTextFormFieldBuilder extends StatelessWidget {
  final String labelText;
  final String hintText;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;
  final TextEditingController controller;
  final FormFieldValidator? validator;
  final int? maxLine;
  final int? minLine;

  const OrderTrackTextFormFieldBuilder({
    Key? key,
    required this.labelText,
    required this.hintText,
    required this.textInputType,
    required this.textInputAction,
    required this.controller,
    this.validator,
    this.maxLine,
    this.minLine,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          labelText,
          style: h4,
        ),
        const SizedBox(height: 5),
        TextFormField(
          controller: controller,
          validator: validator,
          maxLines: maxLine ?? 1,
          minLines: minLine,
          textInputAction: textInputAction,
          keyboardType: textInputType,
          decoration: InputDecoration(
            isDense: true,
            hintText: hintText,
            hintStyle: h5.copyWith(
              color: kGrey,
            ),
            border: const OutlineInputBorder(),
          ),
        ),
      ],
    );
  }
}
