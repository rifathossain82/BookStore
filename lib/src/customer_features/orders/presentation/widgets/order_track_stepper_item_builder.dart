import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class OrderTrackStepperItemBuilder extends StatelessWidget {
  final String leadingAssetIcon;
  final String title;
  final String subtitle;
  final bool isCompleted;
  final bool isFirstStepperItem;

  const OrderTrackStepperItemBuilder({
    Key? key,
    required this.leadingAssetIcon,
    required this.title,
    required this.subtitle,
    required this.isCompleted,
    this.isFirstStepperItem = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            isFirstStepperItem
                ? Container()
                : SizedBox(
                    width: 8,
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: 6,
                      itemBuilder: (context, index) => Container(
                        height: 8,
                        width: 8,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: isCompleted ? mainColor : kGrey,
                        ),
                      ),
                      separatorBuilder: (context, index) => const SizedBox(
                        height: 3,
                      ),
                    ),
                  ),
            isCompleted
                ? Icon(
                    Icons.check_circle_rounded,
                    color: kDeepOrange,
                    size: 70,
                  )
                : Container(
                    height: 70,
                    width: 70,
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: kGrey,
                        width: 1,
                      ),
                    ),
                    child: Image.asset(
                      leadingAssetIcon,
                      fit: BoxFit.cover,
                    ),
                  ),
          ],
        ),
        const SizedBox(width: 15),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title,
                style: h4.copyWith(
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                subtitle,
                style: h5,
              ),
              const SizedBox(height: 17),
            ],
          ),
        ),
      ],
    );
  }
}
