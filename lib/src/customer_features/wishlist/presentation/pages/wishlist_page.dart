import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/wishlist/presentation/widgets/wishlist_book_item_builder.dart';
import 'package:flutter/material.dart';

class WishlistPage extends StatelessWidget {
  WishlistPage({Key? key}) : super(key: key);

  final wishlistItems = [
    fakeBookList[0],
    fakeBookList[1],
    fakeBookList[2],
    fakeBookList[3],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('My Wish List'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(15),
            child: Text(
              'You have ${wishlistItems.length} products in your wishlist',
              style: h3,
            ),
          ),
          Expanded(
            child: ListView.separated(
              itemCount: wishlistItems.length,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              itemBuilder: (context, index) => WishlistBookItemBuilder(
                bookData: wishlistItems[index],
              ),
              separatorBuilder: (context, index) => const SizedBox(
                height: 15,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
