import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/material.dart';

class WishlistBookItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const WishlistBookItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              spreadRadius: 1,
              blurRadius: 4,
              color: kGreyMedium.withOpacity(0.5),
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              flex: 4,
              child: ProductImageBuilder(
                height: 140,
                width: 100,
                imgUrl: bookData.image!,
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 6,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    bookData.name!,
                    style: h4,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    bookData.author!,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: h5.copyWith(
                      color: kGrey,
                    ),
                  ),
                  const SizedBox(height: 5),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${AppConstants.currency} ${bookData.regularPrice}',
                        style: h4.copyWith(
                          color: kGrey,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Text(
                        '${AppConstants.currency} ${bookData.offerPrice}',
                        style: h4,
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: KOutlinedButton(
                          onPressed: (){
                            Navigator.pushNamed(context, RouteGenerator.cart);
                          },
                          borderColor: kOrange,
                          height: 40,
                          child: Text(
                            'Add To Cart',
                            style: h5.copyWith(
                              color: kOrange,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.delete,
                            color: kGrey,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
