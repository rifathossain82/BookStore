import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/html_view_builder.dart';
import 'package:flutter/material.dart';

class ReturnPolicyPage extends StatelessWidget {
  const ReturnPolicyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Return Policy'),
        centerTitle: true,
        elevation: 2,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
          ),
          child: HTMLViewBuilder(
            htmlData: fakeReturnPolicyHTMLData,
          ),
        ),
      ),
    );
  }
}
