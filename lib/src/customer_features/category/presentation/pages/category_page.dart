import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/category/presentation/widgets/category_item_builder.dart';
import 'package:flutter/material.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({Key? key}) : super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  int? selectedCategoryIndex;

  final List<String> categoryList = [
    'কৃষি ও কৃষক',
    'ড্রয়িং, পেইন্টিং ডিজাইন ও ফটোগ্রাফি',
    'বাংলাদেশ',
    'জীবনী, স্মৃতিচারণ ও সাক্ষাৎকার',
    'ব্যবসা, বিনিয়োগ ও আর্থনীতি',
    'শিশু-কিশোর বই',
    'রচনাসমগ্র/সংকলন',
    'কমিক্স, নকশা ও ছবির গল্প',
    'কৃষি ও কৃষক',
    'ড্রয়িং, পেইন্টিং ডিজাইন ও ফটোগ্রাফি',
    'বাংলাদেশ',
    'জীবনী, স্মৃতিচারণ ও সাক্ষাৎকার',
    'ব্যবসা, বিনিয়োগ ও আর্থনীতি',
    'শিশু-কিশোর বই',
    'রচনাসমগ্র/সংকলন',
    'কমিক্স, নকশা ও ছবির গল্প',
    'কৃষি ও কৃষক',
    'ড্রয়িং, পেইন্টিং ডিজাইন ও ফটোগ্রাফি',
    'বাংলাদেশ',
    'জীবনী, স্মৃতিচারণ ও সাক্ষাৎকার',
    'ব্যবসা, বিনিয়োগ ও আর্থনীতি',
    'শিশু-কিশোর বই',
    'রচনাসমগ্র/সংকলন',
    'কমিক্স, নকশা ও ছবির গল্প',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            size: 30,
          ),
        ),
        title: Text(
          'Search your category',
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        height: context.screenHeight,
        width: context.screenWidth,
        color: kWhite,
        margin: const EdgeInsets.only(
          top: 8,
          left: 8,
          right: 8,
        ),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 15,
            crossAxisSpacing: 20,
            childAspectRatio: 1.5 / 1,
          ),
          padding: const EdgeInsets.only(
            top: 12,
            left: 8,
            right: 8,
            bottom: 20,
          ),
          itemCount: categoryList.length,
          itemBuilder: (context, index) => CategoryItemBuilder(
            onTap: () {
              setState(() {
                selectedCategoryIndex = index;
              });
              Navigator.pushNamed(
                context,
                RouteGenerator.categoryWiseBookList,
                arguments: categoryList[index],
              );
            },
            name: categoryList[index],
            isSelected: selectedCategoryIndex == index,
          ),
        ),
      ),
    );
  }
}
