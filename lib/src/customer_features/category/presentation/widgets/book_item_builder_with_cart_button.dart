import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookItemBuilderWithCartButton extends StatelessWidget {
  final FakeBookData bookData;

  const BookItemBuilderWithCartButton({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Container(
        width: 130,
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              spreadRadius: 0,
              blurRadius: 3,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: ProductImageBuilder(
                imgUrl: bookData.image!,
                width: 100,
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 8),
                Text(
                  bookData.name!,
                  textAlign: TextAlign.center,
                  style: h4,
                ),
                Text(
                  bookData.author!,
                  textAlign: TextAlign.center,
                  style: h5.copyWith(
                    color: kGrey,
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      '${AppConstants.currency} ${bookData.regularPrice}',
                      style: h4.copyWith(
                        color: kGrey,
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                    const SizedBox(width: 8),
                    Text(
                      '${AppConstants.currency} ${bookData.offerPrice}',
                      style: h4,
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            KOutlinedButton(
              borderRadius: 4,
              width: context.screenWidth * 0.35,
              borderColor: kOrange,
              onPressed: (){},
              child: Text(
                'Add To Cart',
                style: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kOrange,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
