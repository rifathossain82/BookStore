import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CategoryItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String name;
  final bool isSelected;

  const CategoryItemBuilder({
    Key? key,
    required this.onTap,
    required this.name,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        splashColor: mainColor.withOpacity(0.2),
        highlightColor: mainColor.withOpacity(0.2),
        borderRadius: BorderRadius.circular(4),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(0, 3),
                    spreadRadius: 0,
                    blurRadius: 2,
                    color: kItemShadowColor,
                  ),
                ],
                image: const DecorationImage(
                  opacity: 0.2,
                  fit: BoxFit.cover,
                  image: AssetImage(
                    AssetPath.categoryBgImage,
                  ),
                ),
              ),
              alignment: Alignment.center,
              child: Text(
                name,
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            isSelected
                ? Positioned(
                    child: Container(
                      decoration: BoxDecoration(
                        color: mainColor.withOpacity(0.9),
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [
                          BoxShadow(
                            offset: const Offset(0, 3),
                            spreadRadius: 0,
                            blurRadius: 2,
                            color: kItemShadowColor,
                          ),
                        ],
                      ),
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.check_circle_outline_rounded,
                        color: kWhite,
                        size: 60,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
