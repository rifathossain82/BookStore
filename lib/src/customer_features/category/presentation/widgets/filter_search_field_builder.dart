import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class FilterSearchFieldBuilder extends StatelessWidget {
  final TextEditingController controller;

  const FilterSearchFieldBuilder({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        hintText: 'Search',
        isDense: true,
        focusedBorder: OutlineInputBorder(
          gapPadding: 0,
          borderRadius: BorderRadius.circular(4),
          borderSide: const BorderSide(
            color: Colors.transparent,
            width: 0,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          gapPadding: 0,
          borderRadius: BorderRadius.circular(4),
          borderSide: const BorderSide(
            color: Colors.transparent,
            width: 0,
          ),
        ),
        filled: true,
        fillColor: kWhite,
      ),
    );
  }
}
