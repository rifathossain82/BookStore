import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class FilterCheckboxTileBuilder extends StatelessWidget {
  final ValueChanged onChanged;
  final bool value;
  final String title;

  const FilterCheckboxTileBuilder({
    Key? key,
    required this.onChanged,
    required this.value,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      onChanged: onChanged,
      value: value,
      visualDensity: const VisualDensity(
        vertical: VisualDensity.minimumDensity,
        horizontal: VisualDensity.minimumDensity,
      ),
      title: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: h4,
      ),
    );
  }
}
