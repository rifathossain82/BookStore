import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:book_store/src/customer_features/account/presentation/widgets/account_item_builder.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

openAccountBottomSheet(BuildContext context) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(20),
      ),
    ),
    builder: (context) {
      return SizedBox(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.6,
          padding: const EdgeInsets.only(
            top: 15,
            left: 15,
            right: 15,
          ),
          child: Column(
            children: [
              Container(
                height: 4,
                width: 60,
                decoration: BoxDecoration(
                  color: kGreyMedium,
                  borderRadius: BorderRadius.circular(4),
                ),
              ),
              Expanded(
                child: StatefulBuilder(builder: (context, setState) {
                  return Padding(
                    padding: MediaQuery.of(context).viewInsets,
                    child: SingleChildScrollView(
                      child: Container(
                        padding: const EdgeInsets.all(15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            /// My Account Title
                            Text(
                              'My Account',
                              textAlign: TextAlign.center,
                              style: h2.copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),

                            /// profile image
                            const SizedBox(height: 10),
                            KProfileImage(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.profile,
                              ),
                              height: 70,
                              width: 70,
                              imgURL: '',
                            ),

                            /// user name and phone number
                            const SizedBox(height: 10),
                            Text(
                              "Rifat Hossain",
                              style: GoogleFonts.roboto(
                                textStyle: h2.copyWith(
                                  fontWeight: FontWeight.w700,
                                  height: 1.2,
                                ),
                              ),
                            ),
                            const SizedBox(height: 5),
                            Text(
                              "01885256220",
                              style: h4.copyWith(
                                color: kGreyMedium,
                              ),
                            ),
                            const SizedBox(height: 20),

                            /// profile, order, wishlist and e-library items
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                IconWithTextButtonBuilder(
                                  onTap: () => Navigator.pushNamed(
                                    context,
                                    RouteGenerator.profile,
                                  ),
                                  icon: Icon(
                                    Icons.person_outline_outlined,
                                    color: kWhite,
                                  ),
                                  title: 'Profile',
                                  bgColor: kDeepOrange,
                                ),
                                IconWithTextButtonBuilder(
                                  onTap: () => Navigator.pushNamed(
                                    context,
                                    RouteGenerator.myOrders,
                                  ),
                                  icon: Icon(
                                    Icons.shopping_cart_outlined,
                                    color: kWhite,
                                  ),
                                  title: 'Order',
                                  bgColor: kGreen,
                                ),
                                IconWithTextButtonBuilder(
                                  onTap: () => Navigator.pushNamed(
                                    context,
                                    RouteGenerator.wishlist,
                                  ),
                                  icon: Icon(
                                    Icons.favorite_outline,
                                    color: kWhite,
                                  ),
                                  title: 'Wishlist',
                                  bgColor: mainColor,
                                ),
                                IconWithTextButtonBuilder(
                                  onTap: () {},
                                  icon: Icon(
                                    Icons.shopping_bag_outlined,
                                    color: kWhite,
                                  ),
                                  title: 'Pre-Order',
                                  bgColor: kOrange,
                                ),
                              ],
                            ),
                            const SizedBox(height: 20),

                            /// list tile items of my account
                            AccountItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.wishlist,
                              ),
                              leading: const Icon(Icons.list),
                              title: 'My Wishlists',
                            ),
                            KDivider(
                              height: 10,
                              color: kGreyMedium,
                            ),
                            AccountItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.ratingsAndReviews,
                              ),
                              leading: const Icon(Icons.reviews),
                              title: 'My Reviews & Ratings',
                            ),
                            KDivider(
                              height: 10,
                              color: kGreyMedium,
                            ),
                            AccountItemBuilder(
                              onTap: () => Navigator.pushNamed(
                                context,
                                RouteGenerator.signIn,
                              ),
                              leading: const Icon(Icons.logout),
                              title: 'Sign Out',
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ],
          ),
        ),
      );
    },
  );
}
