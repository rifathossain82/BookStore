import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AccountItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final Widget leading;
  final String title;

  const AccountItemBuilder({
    Key? key,
    required this.onTap,
    required this.leading,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      visualDensity: const VisualDensity(
        horizontal: VisualDensity.minimumDensity,
      ),
      contentPadding: EdgeInsets.zero,
      onTap: onTap,
      leading: leading,
      title: Text(
        title,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: h4,
      ),
      trailing: Icon(
        Icons.arrow_forward_ios,
        color: kGrey,
        size: 15,
      ),
    );
  }
}