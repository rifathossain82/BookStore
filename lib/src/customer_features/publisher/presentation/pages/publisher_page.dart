import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/customer_features/publisher/presentation/widgets/publisher_item_builder.dart';
import 'package:flutter/material.dart';

class PublisherPage extends StatefulWidget {
  const PublisherPage({Key? key}) : super(key: key);

  @override
  State<PublisherPage> createState() => _AuthorPageState();
}

class _AuthorPageState extends State<PublisherPage> {
  bool _isSearch = false;
  TextEditingController searchTextController = TextEditingController();

  void _startSearch() {
    setState(() {
      _isSearch = true;
    });
  }

  void _stopSearch() {
    setState(() {
      _isSearch = false;
      searchTextController.clear();
    });
  }

  Widget _buildSearchField() {
    return TextField(
      controller: searchTextController,
      autofocus: true,
      decoration: InputDecoration(
        hintText: 'Search your favorite company',
        border: InputBorder.none,
        hintStyle: h4.copyWith(
          color: kGrey,
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Text(
      'COMPANY',
      style: h2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: _isSearch ? _buildSearchField() : _buildTitle(),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              _isSearch ? Icons.close : Icons.search,
            ),
            onPressed: () {
              if (_isSearch) {
                _stopSearch();
              } else {
                _startSearch();
              }
            },
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 6,
        ),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
        ),
        child: ListView.separated(
          itemCount: fakePublisherList.length,
          itemBuilder: (context, index) => PublisherItemBuilder(
            publisherData: fakePublisherList[index],
          ),
          separatorBuilder: (context, index) => const SizedBox(
            height: 15,
          ),
        ),
      ),
    );
  }
}
