import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:flutter/material.dart';

class PublisherItemBuilder extends StatelessWidget {
  final FakePublisherData publisherData;

  const PublisherItemBuilder({
    Key? key,
    required this.publisherData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.publisherBookListPage,
        arguments: publisherData,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          KProfileImage(
            height: 60,
            width: 60,
            imgURL: publisherData.image,
            errorAssetImgPath: AssetPath.publisherIcon,
            opacity: 0.2,
          ),
          const SizedBox(width: 15),
          Expanded(
            child: Text(
              publisherData.name,
              textAlign: TextAlign.start,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: h4.copyWith(),
            ),
          )
        ],
      ),
    );
  }
}
