import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:flutter/material.dart';

class PublisherCardBuilder extends StatelessWidget {
  final FakePublisherData publisherData;

  const PublisherCardBuilder({
    Key? key,
    required this.publisherData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      alignment: Alignment.center,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        image: const DecorationImage(
          opacity: 0.2,
          fit: BoxFit.cover,
          image: AssetImage(
            AssetPath.categoryBgImage,
          ),
        ),
      ),
      child: ListTile(
        dense: true,
        leading: KProfileImage(
          height: 50,
          width: 50,
          imgURL: publisherData.image,
        ),
        title: Text(
          publisherData.name,
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          '20 items',
          style: h5,
        ),
      ),
    );
  }
}
