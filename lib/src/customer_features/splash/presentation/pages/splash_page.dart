import 'dart:async';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/customer_features/splash/presentation/widgets/select_dashboard_dialog.dart';
import 'package:book_store/src/customer_features/splash/presentation/widgets/splash_bottom_text.dart';
import 'package:book_store/src/customer_features/splash/presentation/widgets/splash_logo_builder.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashPage> {
  setSplashDuration() async {
    return Timer(
      const Duration(seconds: 3),
      () => pageNavigation(),
    );
  }

  void pageNavigation() async {
    Navigator.pop(context);
    showDialog(
      context: context,
      builder: (context) => const SelectDashboardDialog(),
    );

    /// to navigate directly in customer dashboard page
    // Navigator.pushReplacementNamed(
    //   context,
    //   RouteGenerator.dashboard,
    // );
  }

  @override
  void initState() {
    /// set duration and control what next after splash duration
    setSplashDuration();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                kDeepOrange,
                kOrange,
              ],
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SplashLogoBuilder(
                height: 150,
                width: 150,
              ),
              SizedBox(height: context.screenHeight * 0.15),
              const SplashBottomText(),
            ],
          ),
        ),
      ),
    );
  }
}
