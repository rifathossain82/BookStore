import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class SplashBottomText extends StatelessWidget {
  const SplashBottomText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25),
      child: Text(
        'Change Your Life With ${AppConstants.appName}...',
        style: h4.copyWith(
          color: kGreyLight,
        ),
      ),
    );
  }
}
