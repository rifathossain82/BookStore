import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:flutter/material.dart';

class SelectDashboardDialog extends StatefulWidget {

  const SelectDashboardDialog({
    Key? key
  }) : super(key: key);

  @override
  _SelectDashboardDialogState createState() => _SelectDashboardDialogState();
}

class _SelectDashboardDialogState extends State<SelectDashboardDialog> {

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Choose Dashboard'),
      actionsPadding: const EdgeInsets.all(15),
      scrollable: true,
      content: Column(
        children: [
          KButton(
            onPressed: (){
              Navigator.pushReplacementNamed(
                context,
                RouteGenerator.dashboard,
              );
            },
            child: Text(
              'Customer Dashboard',
              style: h4.copyWith(
                color: kWhite,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 20),
          KButton(
            onPressed: (){
              Navigator.pushReplacementNamed(
                context,
                RouteGenerator.courierDashboard,
              );
            },
            child: Text(
              'Courier Dashboard',
              style: h4.copyWith(
                color: kWhite,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
