import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:flutter/material.dart';

class SplashLogoBuilder extends StatelessWidget {
  final double height;
  final double width;

  const SplashLogoBuilder({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Image.asset(
              AssetPath.bookStoreAppLogo,
            ),
          ),
          const SizedBox(height: 15),
          Expanded(
            flex: 1,
            child: Image.asset(
              AssetPath.bookStoreLogo,
            ),
          ),
        ],
      ),
    );
  }
}
