import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/expandable_text.dart';
import 'package:book_store/src/core/widgets/k_profile_image.dart';
import 'package:book_store/src/core/widgets/star_rating_builder.dart';
import 'package:flutter/material.dart';

class ReviewItemBuilder extends StatelessWidget {
  const ReviewItemBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Column(
        children: [
          /// customer
          ListTile(
            contentPadding: EdgeInsets.zero,
            leading: KProfileImage(
              height: 40,
              width: 40,
              imgURL: '',
            ),
            title: Row(
              children: [
                Text(
                  'By',
                  style: h5,
                ),
                const SizedBox(width: 8),
                Text(
                  'Tasfia Jahan',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            subtitle: Row(
              children: [
                Text(
                  'Dec 25, 2023',
                  style: h5.copyWith(
                    fontStyle: FontStyle.italic,
                  ),
                ),
                const SizedBox(width: 20),
                StarRatingBuilder(
                  color: kOrange,
                  starCount: 5,
                  rating: 5,
                  starSize: 15,
                ),
              ],
            ),
          ),

          /// review comment
          const ExpandableText(
            text: 'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.',
          ),
          const SizedBox(height: 8),

          /// like and dislike button
          Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: kGreyLight.withOpacity(0.5),
              borderRadius: BorderRadius.circular(1),
            ),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    'Was this review helpful to you?',
                    style: h5,
                  ),
                ),
                Text(
                  '3',
                  style: h4.copyWith(
                    color: mainColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(width: 5),
                _buildLikeOrDislikeButton(
                  onTap: () {
                    kPrint('Like button pressed!');
                  },
                  iconData: Icons.thumb_up_off_alt_outlined,
                ),
                const SizedBox(width: 8),
                _buildLikeOrDislikeButton(
                  onTap: () {
                    kPrint('Dislike button pressed!');
                  },
                  iconData: Icons.thumb_down_off_alt_outlined,
                ),
                const SizedBox(width: 5),
                Text(
                  '0',
                  style: h4.copyWith(
                    color: mainColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLikeOrDislikeButton({
    required VoidCallback onTap,
    required IconData iconData,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 5,
        ),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(2),
        ),
        child: Icon(
          iconData,
          color: kGrey,
          size: 20,
        ),
      ),
    );
  }
}
