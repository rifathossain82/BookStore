import 'package:book_store/src/customer_features/home/presentation/widgets/category_wise_books_builder.dart';
import 'package:flutter/material.dart';

class CategoryWiseBooksWidget extends StatelessWidget {
  CategoryWiseBooksWidget({Key? key}) : super(key: key);

  final List<String> categoryList = [
    'Book Fair 2023',
    'শিশু-কিশোর বই',
    'ইসলামিক বই',
    'বইমেলা ২০২৩ঃ কথাপ্রকাশের সেরা বই',
    'প্রাথমিক শিক্ষক নিয়োগ গাইড',
    'ইলাননুর পাবলিকেশন এর সকল বই',
    'গনিত, বিজ্ঞান ও প্রযুক্তি',
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: categoryList.length,
      itemBuilder: (context, index) => CategoryWiseBooksBuilder(
        categoryName: categoryList[index],
      ),
    );
  }
}
