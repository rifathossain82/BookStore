import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class TopHomeItems extends StatelessWidget {
  const TopHomeItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      decoration: BoxDecoration(
        color: kGreyLight,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconWithTextButtonBuilder(
            onTap: () => Navigator.pushNamed(
              context,
              RouteGenerator.category,
            ),
            icon: Icon(
              Icons.book_sharp,
              color: kWhite,
            ),
            title: 'Books',
            bgColor: mainColor,
          ),
          IconWithTextButtonBuilder(
            onTap: () {},
            icon: Icon(
              Icons.lightbulb_circle_sharp,
              color: kWhite,
            ),
            title: 'Electronics',
            bgColor: kGreen,
          ),
          IconWithTextButtonBuilder(
            onTap: () {},
            icon: Icon(
              Icons.keyboard_hide,
              color: kWhite,
            ),
            title: 'Kids Zone',
            bgColor: kBlueDark,
          ),
          IconWithTextButtonBuilder(
            onTap: () {},
            icon: Icon(
              Icons.star,
              color: kWhite,
            ),
            title: 'Monihari',
            bgColor: kOrange,
          ),
        ],
      ),
    );
  }
}
