import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class ExamItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String assetPath;
  final String title;
  final Color bgColor;

  const ExamItemBuilder({
    Key? key,
    required this.onTap,
    required this.assetPath,
    required this.title,
    required this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = 120;
    double width = context.screenWidth * 0.28;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: 7,
              child: Container(
                width: width,
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: bgColor,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                ),
                child: Image.asset(
                  assetPath,
                  height: 60,
                  width: 60,
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                width: width,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: kBlueMedium,
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                  ),
                ),
                child: Text(
                  '$title >',
                  textAlign: TextAlign.center,
                  style: h4.copyWith(
                    color: kWhite,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
