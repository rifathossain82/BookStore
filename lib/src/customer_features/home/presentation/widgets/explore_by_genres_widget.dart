import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/product_item_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/exam_item_item_builder.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class ExploreByGenresWidget extends StatelessWidget {
  ExploreByGenresWidget({Key? key}) : super(key: key);

  final List<String> genresList = [
    'অমর একুশে বইমেলা ২০২৩',
    'শিশু-কিশোর বই',
    'ইসলামিক বই',
    'উপন্যাস',
    'প্রোগ্রামিং, আউটসোর্সিং',
    'আত্ম-উন্নয়ন',
    'গনিত, বিজ্ঞান ও প্রযুক্তি',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: 15,
        bottom: 15,
        left: 15,
        right: 30,
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Explore by Genres',
            style: h2,
          ),
          KDivider(
            height: 20,
            color: kGrey,
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: genresList.length,
            itemBuilder: (context, index) => _genresItemBuilder(
              context,
              genresList[index],
            ),
            separatorBuilder: (context, index) => KDivider(
              color: kGrey,
            ),
          ),
        ],
      ),
    );
  }

  Widget _genresItemBuilder(BuildContext context, String genre) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.categoryWiseBookList,
        arguments: genre,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              genre,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: h4,
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: kGrey,
              size: 15,
            )
          ],
        ),
      ),
    );
  }
}
