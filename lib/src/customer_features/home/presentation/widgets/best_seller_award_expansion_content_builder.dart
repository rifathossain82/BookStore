import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/book_category_dropdown_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/best_seller_book_item_builder.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class BestSellerAwardExpansionContentBuilder extends StatefulWidget {
  final String category;
  final Color color;

  const BestSellerAwardExpansionContentBuilder({
    Key? key,
    required this.category,
    required this.color,
  }) : super(key: key);

  @override
  State<BestSellerAwardExpansionContentBuilder> createState() =>
      _BestSellerAwardExpansionContentBuilderState();
}

class _BestSellerAwardExpansionContentBuilderState
    extends State<BestSellerAwardExpansionContentBuilder> {
  int selectedBestSellerOptionIndex = 0;
  int selectedBestSellerOption2Index = 0;

  @override
  void initState() {
    super.initState();
  }

  final bestSellerOption = [
    'বই',
    'লেখক',
  ];

  final bestSellerOption2 = [
    'TODAY',
    'ALL',
  ];

  String selectedSubCategory = 'ফিকশন বই';
  final subCategories = [
    'ফিকশন বই',
    'নন ফিকশন বই',
    'ধর্মীয় বই',
    'ক্যারিয়ার ও একাডেমিক বই',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(4),
      ),
      child: Column(
        children: [
          /// বই / লেখক selector widget
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: List.generate(
              bestSellerOption.length,
              (index) => _buildBookOrAuthorItem(
                title: bestSellerOption[index],
                isSelected: selectedBestSellerOptionIndex == index,
                onTap: () {
                  setState(() {
                    selectedBestSellerOptionIndex = index;
                  });
                },
              ),
            ),
          ),

          /// subcategory dropdown
          Container(
            height: 70,
            width: context.screenWidth,
            color: kGreyLight,
            alignment: Alignment.center,
            child: BookCategoryDropdownBuilder(
              selectedItem: selectedSubCategory,
              items: subCategories,
              onChanged: (value) {
                setState(() {
                  selectedSubCategory = value;
                });
              },
            ),
          ),

          /// today / all tabBar and tabView
          DefaultTabController(
            length: 2,
            animationDuration: const Duration(seconds: 1),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    top: 15,
                    left: context.screenWidth * 0.15,
                    right: context.screenWidth * 0.15,
                  ),
                  color: kGreyLight,
                  child: TabBar(
                    onTap: (value) {},
                    indicatorSize: TabBarIndicatorSize.label,
                    labelPadding: const EdgeInsets.only(bottom: 10),
                    padding: EdgeInsets.zero,
                    tabs: [
                      _tabItemBuilder('TODAY'),
                      _tabItemBuilder('ALL'),
                    ],
                  ),
                ),
                Container(
                  height: 300,
                  child: TabBarView(
                    children: [
                      _buildTodayBookList(),
                      _buildAllBookList(),
                    ],
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 5),

          /// view all button
          KOutlinedButton(
            width: context.screenWidth * 0.3,
            height: 40,
            borderColor: mainColor,
            child: Text(
              'View All',
              style: h4.copyWith(
                color: mainColor,
                fontWeight: FontWeight.w500,
              ),
            ),
            onPressed: () {},
          ),
        ],
      ),
    );
  }

  Widget _buildBookOrAuthorItem({
    required String title,
    required bool isSelected,
    required VoidCallback onTap,
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: context.screenWidth * 0.35,
        height: 50,
        decoration: BoxDecoration(
          color: isSelected ? widget.color : kGreyMedium,
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(5),
          ),
        ),
        child: Container(
          margin: const EdgeInsets.only(
            top: 4,
            left: 2,
            right: 2,
          ),
          decoration: BoxDecoration(
            color: kWhite,
            borderRadius: const BorderRadius.vertical(
              top: Radius.circular(5),
            ),
          ),
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.vertical(
                top: Radius.circular(5),
              ),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  isSelected ? widget.color.withOpacity(0.15) : kWhite,
                  isSelected ? widget.color.withOpacity(0.05) : kGreyLight,
                ],
              ),
            ),
            child: Text(
              title,
              style: h3.copyWith(
                color: isSelected ? widget.color : kBlackLight,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _tabItemBuilder(String title) {
    return Text(
      title.toUpperCase(),
      style: h3.copyWith(
        fontWeight: FontWeight.w700,
      ),
    );
  }

  Widget _buildTodayBookList() {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      itemCount: fakeBookList.length,
      itemBuilder: (context, index) => BestSellerBookItemBuilder(
        bookData: fakeBookList[index],
      ),
      separatorBuilder: (context, index) => KDivider(
        height: 5,
        color: kGrey,
      ),
    );
  }

  Widget _buildAllBookList() {
    return ListView.separated(
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      itemCount: fakeBookList.length,
      itemBuilder: (context, index) => BestSellerBookItemBuilder(
        bookData: fakeBookList[index],
      ),
      separatorBuilder: (context, index) => KDivider(
        height: 5,
        color: kGrey,
      ),
    );
  }
}
