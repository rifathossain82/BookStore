import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/star_rating_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BestSellerBookItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const BestSellerBookItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      padding: const EdgeInsets.all(5),
      child: Row(
        children: [
          /// position index show -> 1,2,3 etc
          Expanded(
            flex: 1,
            child: Text(
              '1',
              textAlign: TextAlign.left,
              style: h1.copyWith(
                color: kOrange,
              ),
            ),
          ),

          /// book image
          Expanded(
            flex: 3,
            child: ProductImageBuilder(
              imgUrl: bookData.image!,
              height: 100,
              enableShadow: false,
              borderRadius: 0,
            ),
          ),
          const SizedBox(width: 10),

          /// book name, author name, ratings etc
          Expanded(
            flex: 8,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  bookData.name!,
                  style: h3,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  bookData.author!,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: h5.copyWith(
                    color: kGrey,
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    StarRatingBuilder(
                      color: kOrange,
                      rating: bookData.ratings ?? 0.0,
                      starSize: 15,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      '(${bookData.totalRatings})',
                      style: h5.copyWith(
                        color: kGrey,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

          /// show the icon based on status, upgrade/downgrade
          Expanded(
            flex: 1,
            child: Icon(
              Icons.arrow_upward,
              color: kGreen,
            ),
          ),
        ],
      ),
    );
  }
}
