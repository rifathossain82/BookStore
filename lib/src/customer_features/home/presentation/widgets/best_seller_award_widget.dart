import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/app_expansion_tile.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/best_seller_award_expansion_content_builder.dart';
import 'package:flutter/material.dart';

class BestSellerAwardWidget extends StatefulWidget {
  const BestSellerAwardWidget({Key? key}) : super(key: key);

  @override
  State<BestSellerAwardWidget> createState() => _BestSellerAwardWidgetState();
}

class _BestSellerAwardWidgetState extends State<BestSellerAwardWidget> {
  ///to expand only expansionTile
  int expansionTileIndex = -1;

  final bestSellerCategory = [
    'ফিকশন বই',
    'নন ফিকশন বই',
    'ধর্মীয় বই',
    'ক্যারিয়ার ও একাডেমিক বই',
  ];

  final bestSellerCategoryColors = [
    const Color(0xFF8dcdf7),
    const Color(0xFFf9b678),
    const Color(0xFFbdd476),
    const Color(0xFFdabdf9),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AssetPath.bookStoreLogo,
            height: 55,
            width: context.screenWidth * 0.6,
          ),
          const SizedBox(height: 20),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: bestSellerCategory.length,
            itemBuilder: (context, index) => buildBestSellerItem(index),
            separatorBuilder: ((context, index) => const SizedBox(height: 10)),
          ),
        ],
      ),
    );
  }

  Widget buildBestSellerItem(int index) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: bestSellerCategoryColors[index],
      ),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: AppExpansionTile(
          title: Text(
            bestSellerCategory[index],
            style: h2.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          trailing: expansionTileIndex == index
              ? Icon(
                  Icons.keyboard_arrow_up,
                  size: 25,
                  color: kBlackLight,
                )
              : Icon(
                  Icons.keyboard_arrow_down,
                  size: 25,
                  color: kBlackLight,
                ),
          childrenPadding: const EdgeInsets.only(
            left: 8,
            right: 8,
            bottom: 8,
          ),
          expandedAlignment: Alignment.centerLeft,
          initiallyExpanded: expansionTileIndex == index,
          children: [
            BestSellerAwardExpansionContentBuilder(
              category: bestSellerCategory[index],
              color: bestSellerCategoryColors[index],
            ),
          ],

          ///to select single tile at a time
          onExpansionChanged: (s) {
            if (expansionTileIndex == index) {
              expansionTileIndex = -1;
              setState(() {});
            } else {
              setState(() {
                expansionTileIndex = index;
              });
            }
          },
        ),
      ),
    );
  }
}
