import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class BookCategoryDropdownBuilder extends StatelessWidget {
  final String selectedItem;
  final List<String> items;
  final ValueChanged onChanged;

  const BookCategoryDropdownBuilder({
    Key? key,
    required this.selectedItem,
    required this.items,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.screenWidth * 0.8,
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(20)
      ),
      padding: const EdgeInsets.all(12),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          borderRadius: BorderRadius.circular(4),
          isDense: true,
          items: items
              .map(
                (item) => DropdownMenuItem(
                  value: item,
                  child: Text(
                    item,
                    style: h4,
                  ),
                ),
              )
              .toList(),
          onChanged: onChanged,
          value: selectedItem,
        ),
      ),
    );
  }
}