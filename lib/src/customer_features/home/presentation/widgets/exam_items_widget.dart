import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/product_item_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/exam_item_item_builder.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class ExamItemsWidget extends StatelessWidget {
  const ExamItemsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Browse by Exams',
            style: h2,
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'Medical',
                ),
                assetPath: AssetPath.medical,
                title: 'Medical',
                bgColor: kGreen.withOpacity(0.1),
              ),
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'Engineering',
                ),
                assetPath: AssetPath.engineer,
                title: 'Engg.',
                bgColor: kDeepOrange.withOpacity(0.1),
              ),
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'University',
                ),
                assetPath: AssetPath.university,
                title: 'University',
                bgColor: mainColor.withOpacity(0.1),
              ),
            ],
          ),
          const SizedBox(height: 12),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'Nursing',
                ),
                assetPath: AssetPath.nursing,
                title: 'Nursing',
                bgColor: kBlue.withOpacity(0.1),
              ),
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'College',
                ),
                assetPath: AssetPath.college,
                title: 'College',
                bgColor: kOrange.withOpacity(0.1),
              ),
              ExamItemBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: 'School',
                ),
                assetPath: AssetPath.school,
                title: 'School',
                bgColor: kRed.withOpacity(0.1),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
