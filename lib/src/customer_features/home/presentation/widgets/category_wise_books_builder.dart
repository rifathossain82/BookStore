import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/product_item_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/exam_item_item_builder.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class CategoryWiseBooksBuilder extends StatelessWidget {
  final String categoryName;

  CategoryWiseBooksBuilder({
    Key? key,
    required this.categoryName,
  }) : super(key: key);

  final List<FakeBookData> bookList = [...fakeBookList];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: 15,
        bottom: 15,
        left: 15,
        right: 30,
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  categoryName,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: h2,
                ),
              ),
              const SizedBox(width: 8),
              GestureDetector(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.categoryWiseBookList,
                  arguments: categoryName,
                ),
                child: Text(
                  'See More',
                  textAlign: TextAlign.end,
                  style: h4.copyWith(
                    color: mainColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          SizedBox(
            height: 250,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: bookList.length,
              itemBuilder: (context, index) => ProductItemBuilder(
                bookData: bookList[index],
              ),
              separatorBuilder: (context, index) => const SizedBox(width: 15),
            ),
          ),
        ],
      ),
    );
  }
}
