import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/product_item_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/exam_item_item_builder.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class FavoriteBooksWidget extends StatelessWidget {
  FavoriteBooksWidget({Key? key}) : super(key: key);

  final List<FakeBookData> favoriteBookList1 = [
    fakeBookList[0],
    fakeBookList[1],
    fakeBookList[2],
  ];
  final List<FakeBookData> favoriteBookList2 = [
    fakeBookList[0],
    fakeBookList[1],
    fakeBookList[2],
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'জনপ্রিয় বই',
            style: h2,
          ),
          KDivider(
            height: 20,
            color: kGrey,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 8,
                child: _buildLeftSideFavoriteBooks(context),
              ),
              const SizedBox(width: 20),
              Expanded(
                flex: 3,
                child: _buildRightSideFavoriteBooks(context),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildLeftSideFavoriteBooks(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// title -> আত্ম-উন্নয়ন
        Text(
          'আত্ম-উন্নয়ন',
          textAlign: TextAlign.start,
          style: h2,
        ),
        const SizedBox(height: 15),

        /// book list
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: favoriteBookList1.length,
          itemBuilder: (context, index) => _leftSideBookItemBuilder(
            favoriteBookList1[index],
          ),
          separatorBuilder: (context, index) => KDivider(
            height: 30,
            color: kGrey,
          ),
        ),
        KDivider(
          height: 30,
          color: kGrey,
        ),

        /// see more button
        GestureDetector(
          onTap: () => Navigator.pushNamed(
            context,
            RouteGenerator.categoryWiseBookList,
            arguments: 'আত্ম-উন্নয়ন',
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'See More',
                textAlign: TextAlign.start,
                style: h4.copyWith(
                  color: mainColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: mainColor,
                size: 15,
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _leftSideBookItemBuilder(FakeBookData bookData) {
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: ProductImageBuilder(
            height: 120,
            width: 80,
            imgUrl: bookData.image!,
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          flex: 6,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                bookData.name!,
                style: h3,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                bookData.author!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: h5.copyWith(
                  color: kGrey,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildRightSideFavoriteBooks(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        /// title -> ধর্মীয় বই
        Text(
          'ধর্মীয় বই',
          textAlign: TextAlign.center,
          style: h2,
        ),
        const SizedBox(height: 15),

        /// book list
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: favoriteBookList2.length,
          itemBuilder: (context, index) => ProductImageBuilder(
            height: 120,
            width: 80,
            imgUrl: favoriteBookList2[index].image!,
          ),
          separatorBuilder: (context, index) => KDivider(
            height: 30,
            color: kGrey,
          ),
        ),
        KDivider(
          height: 30,
          color: kGrey,
        ),

        /// see more button
        GestureDetector(
          onTap: () => Navigator.pushNamed(
            context,
            RouteGenerator.categoryWiseBookList,
            arguments: 'ধর্মীয় বই',
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'See More',
                textAlign: TextAlign.start,
                style: h4.copyWith(
                  color: mainColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: mainColor,
                size: 15,
              )
            ],
          ),
        ),
      ],
    );
  }
}
