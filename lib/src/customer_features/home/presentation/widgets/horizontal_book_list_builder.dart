import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/product_item_builder.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class HorizontalBookListBuilder extends StatelessWidget {
  final String title;
  final List<FakeBookData> books;

  const HorizontalBookListBuilder({
    Key? key,
    required this.title,
    required this.books,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: 10,
        bottom: 30,
        left: 15,
        right: 15,
      ),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: h2,
          ),
          const SizedBox(height: 15),
          SizedBox(
            height: 250,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: books.length,
              itemBuilder: (context, index) => ProductItemBuilder(
                bookData: books[index],
              ),
              separatorBuilder: (context, index) => const SizedBox(width: 15),
            ),
          ),
        ],
      ),
    );
  }
}
