import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/icon_with_text_button_builder.dart';
import 'package:flutter/material.dart';

class MiddleHomeItems extends StatelessWidget {
  const MiddleHomeItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kWhite,
            kGreyLight,
          ],
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          KButton(
            width: context.screenWidth - 30,
            child: Text(
              'অমর একুশে বইমেলা ২০২৩',
              style: h3.copyWith(
                color: kWhite,
              ),
            ),
            onPressed: () {},
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconWithTextButtonBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.authorPage,
                ),
                icon: Icon(
                  Icons.book_sharp,
                  color: kWhite,
                ),
                title: 'Authors',
                bgColor: mainColor,
                size: 50,
              ),
              IconWithTextButtonBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.category,
                ),
                icon: Icon(
                  Icons.lightbulb_circle_sharp,
                  color: kWhite,
                ),
                title: 'Category',
                bgColor: kGreen,
                size: 50,
              ),
              IconWithTextButtonBuilder(
                onTap: () {},
                icon: Icon(
                  Icons.shopping_bag_outlined,
                  color: kWhite,
                ),
                title: 'Pre-Order',
                bgColor: kBlueDark,
                size: 50,
              ),
              IconWithTextButtonBuilder(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteGenerator.publisherPage,
                ),
                icon: Icon(
                  Icons.star,
                  color: kWhite,
                ),
                title: 'Publisher',
                bgColor: kOrange,
                size: 50,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
