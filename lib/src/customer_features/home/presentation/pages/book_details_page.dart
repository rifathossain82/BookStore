import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/expandable_text.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/core/widgets/star_rating_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/horizontal_book_list_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/review_item_builder.dart';
import 'package:flutter/material.dart';

class BookDetailsPage extends StatefulWidget {
  const BookDetailsPage({Key? key}) : super(key: key);

  @override
  State<BookDetailsPage> createState() => _BookDetailsPageState();
}

class _BookDetailsPageState extends State<BookDetailsPage> {
  final reviewTextController = TextEditingController();
  final questionTextController = TextEditingController();
  late FakeBookData bookData;
  double selectedRatings = 0.0;
  bool isAddedInCart = false;

  @override
  void didChangeDependencies() {
    bookData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 20),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    _buildBookImageNamePriceEtc(),
                    _buildInStockRatingReviewsAndPopMenuButton(),
                    KDivider(color: kGrey, height: 0),
                    _buildCategoryText(),
                    _buildSpecialOffers(),
                    KDivider(color: kGrey),
                    _buildAddToListAndLookInsideButton(),
                    KDivider(
                      color: kGrey,
                      height: 15,
                    ),
                    _buildSummary(),
                    KDivider(
                      color: kGrey,
                      height: 15,
                    ),
                    HorizontalBookListBuilder(
                      title: 'Customer have also bought',
                      books: fakeBookList,
                    ),
                    HorizontalBookListBuilder(
                      title: 'Category Best Selling',
                      books: fakeBookList,
                    ),
                    HorizontalBookListBuilder(
                      title: 'Related Product',
                      books: fakeBookList,
                    ),
                    _buildCustomerReviews(),
                    _buildRateThisProduct(),
                    _buildReviews(),
                    _buildProductQA(),

                    /// give space from bottom
                    const SizedBox(height: 60),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
            Positioned(
              bottom: 0,
              child: _buildAddToCartWidget(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAddToCartWidget() {
    return Container(
      height: 60,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(3, 0),
            spreadRadius: 2,
            blurRadius: 4,
            color: kGreyMedium,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                'Price:',
                style: h5,
              ),
              Text(
                '${AppConstants.currency} ${bookData.regularPrice}',
                style: h3,
              ),
            ],
          ),
          KButton(
            onPressed: () {
              if (isAddedInCart) {
                Navigator.pushReplacementNamed(context, RouteGenerator.cart);
              } else {
                setState(() {
                  isAddedInCart = true;
                });
              }
            },
            borderRadius: 30,
            width: context.screenWidth * 0.4,
            child: Text(
              isAddedInCart ? 'Go to Cart' : 'Add to Cart',
              style: h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildBookImageNamePriceEtc() {
    return Container(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 30,
        bottom: 5,
      ),
      child: Column(
        children: [
          Container(
            width: context.screenWidth * 0.5,
            padding: const EdgeInsets.only(bottom: 5),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'একটু পড়ে দেখুন',
                  style: h2.copyWith(
                    color: kRedDeep,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(width: 5),
                Icon(
                  Icons.arrow_downward_outlined,
                  size: 20,
                  color: kGreen,
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                RouteGenerator.pdfView,
                arguments: 'https://www.africau.edu/images/default/sample.pdf',
              );
            },
            child: ProductImageBuilder(
              imgUrl: bookData.image!,
              height: 250,
              width: context.screenWidth * 0.5,
              borderRadius: 0,
              enableShadow: false,
            ),
          ),
          const SizedBox(height: 15),
          Text(
            bookData.name ?? "",
            style: h2,
            textAlign: TextAlign.center,
          ),
          Text(
            '(${bookData.type})',
            style: h3.copyWith(
              color: kGreyTextColor,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 4),
          Text(
            bookData.author ?? "",
            style: h3,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 8),
          Text(
            '${AppConstants.currency} ${bookData.regularPrice}',
            style: h1.copyWith(fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 5),
          Text(
            'Total 220 Users Purchased',
            style: h5.copyWith(),
          ),
        ],
      ),
    );
  }

  Widget _buildInStockRatingReviewsAndPopMenuButton() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Column(
              children: [
                bookData.inStock!
                    ? Container(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: Row(
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: kGreen,
                            ),
                            const SizedBox(width: 8),
                            Text(
                              'In Stock',
                              style: h4,
                            )
                          ],
                        ),
                      )
                    : Container(),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    StarRatingBuilder(
                      color: kOrange,
                      starSize: 20,
                      rating: bookData.ratings ?? 0.0,
                    ),
                    const SizedBox(width: 10),
                    Text(
                      '${bookData.ratings}',
                      style: h3,
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                const SizedBox(height: 5),
                IntrinsicHeight(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '${bookData.totalRatings} Ratings',
                        style: h4.copyWith(
                          color: kGrey,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      VerticalDivider(
                        width: 15,
                        color: kGrey,
                        thickness: 1,
                        indent: 3,
                        endIndent: 3,
                      ),
                      Text(
                        '${bookData.totalReviews} Reviews',
                        style: h4.copyWith(
                          color: kGrey,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              PopupMenuButton(
                onSelected: (value) {
                  /// your logic
                },
                child: Container(
                  height: 30,
                  width: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: kGreyMedium.withOpacity(0.5),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(
                    Icons.more_horiz,
                    color: kBlackLight,
                    size: 20,
                  ),
                ),
                itemBuilder: (BuildContext bc) {
                  return [
                    PopupMenuItem(
                      value: '1',
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.reviews,
                            color: mainColor,
                            size: 18,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            "See Reviews",
                            style: h3,
                          ),
                        ],
                      ),
                    ),
                    PopupMenuItem(
                      value: '2',
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.save,
                            color: mainColor,
                            size: 18,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            "Save",
                            style: h3,
                          ),
                        ],
                      ),
                    ),
                    PopupMenuItem(
                      value: '3',
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.share,
                            color: mainColor,
                            size: 18,
                          ),
                          const SizedBox(width: 8),
                          Text(
                            "Share",
                            style: h3,
                          ),
                        ],
                      ),
                    ),
                  ];
                },
              ),
              const SizedBox(height: 15),
              GestureDetector(
                onTap: () {},
                behavior: HitTestBehavior.opaque,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(
                      Icons.share,
                      color: mainColor,
                      size: 15,
                    ),
                    const SizedBox(width: 8),
                    Text(
                      "Share",
                      style: h4,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildCategoryText() {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Category:',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(width: 20),
          Expanded(
            child: Text(
              '${bookData.category}',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: h3.copyWith(
                color: mainColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSpecialOffers() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Special Offers:',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 15),
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: bookData.specialOffers!.length,
            itemBuilder: (context, index) => Text(
              '* ${bookData.specialOffers![index]}',
              style: h3,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAddToListAndLookInsideButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        KOutlinedButton(
          onPressed: () {},
          width: context.screenWidth * 0.35,
          borderRadius: 30,
          borderColor: mainColor,
          child: Text(
            'Add to Wishlist',
            style: h4.copyWith(
              color: mainColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        const SizedBox(width: 10),
        KOutlinedButton(
          onPressed: () => Navigator.pushNamed(
            context,
            RouteGenerator.pdfView,
            arguments: 'https://www.africau.edu/images/default/sample.pdf',
          ),
          width: context.screenWidth * 0.35,
          borderRadius: 30,
          borderColor: mainColor,
          child: Text(
            'Look Inside',
            style: h4.copyWith(
              color: mainColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildSummary() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Summary:',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 15),
          Text(
            bookData.summary ?? "",
            textAlign: TextAlign.start,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
            style: h3,
          ),
          ExpandableText(text: bookData.summary ?? ""),
        ],
      ),
    );
  }

  Widget _buildCustomerReviews() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Customer Reviews',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 15),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    '4.14', // here may be average ratings
                    style: TextStyle(
                      fontSize: 35,
                    ),
                  ),
                  Text(
                    'Out of 5.0',
                    style: h3.copyWith(
                      fontWeight: FontWeight.w300,
                      color: kGrey,
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  _starWiseRatingPercentBuilder(starCount: 5, percent: 55),
                  _starWiseRatingPercentBuilder(starCount: 4, percent: 12),
                  _starWiseRatingPercentBuilder(starCount: 3, percent: 0),
                  _starWiseRatingPercentBuilder(starCount: 2, percent: 0),
                  _starWiseRatingPercentBuilder(starCount: 1, percent: 0),
                  const SizedBox(height: 8),
                  Text(
                    '${bookData.totalRatings} Ratings',
                    style: h4.copyWith(
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _starWiseRatingPercentBuilder({
    required int starCount,
    required double percent,
  }) {
    return Row(
      children: [
        ...List.generate(
          starCount,
          (index) => Icon(
            Icons.star,
            size: 18,
            color: kOrange,
          ),
        ),
        const SizedBox(width: 10),
        Stack(
          children: [
            Container(
              height: 7,
              width: 100,
              color: kGreyMedium.withOpacity(0.5),
            ),
            Positioned(
              child: Container(
                height: 7,
                width: 100 * percent / 100,
                color: kGrey,
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildRateThisProduct() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Rate this product',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8),
          StarRatingBuilder(
            color: kOrange,
            blankStarColor: kOrange,
            starSize: 25,
            rating: selectedRatings,
            onRatingChanged: (rating) {
              setState(() {
                selectedRatings = rating;

                /// TODO: your others method here
              });
            },
          ),
          const SizedBox(height: 20),
          TextFormField(
            controller: reviewTextController,
            maxLines: 5,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            textAlign: TextAlign.start,
            decoration: InputDecoration(
              hintText: 'Write your review...',
              hintStyle: const TextStyle(
                fontStyle: FontStyle.italic,
              ),
              isDense: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
          const SizedBox(height: 20),
          Align(
            alignment: Alignment.centerRight,
            child: KOutlinedButton(
              onPressed: () {
                if (reviewTextController.text.isEmpty) {
                  kSnackBar(
                    context: context,
                    message: 'Please write some review',
                    bgColor: failedColor,
                  );
                }
              },
              borderRadius: 30,
              height: 40,
              width: context.screenWidth * 0.32,
              borderColor: mainColor,
              child: Text(
                'Submit',
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildReviews() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: kWhite,
            borderRadius: BorderRadius.circular(4),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 3),
                spreadRadius: 0,
                blurRadius: 4,
                color: kItemShadowColor,
              ),
            ],
          ),
          child: ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: 2,
            itemBuilder: (context, index) => ReviewItemBuilder(),
            // pass your data
            separatorBuilder: (context, index) => const KDivider(
              height: 10,
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(15),
          margin: const EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: kWhite,
            borderRadius: BorderRadius.circular(4),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 3),
                spreadRadius: 0,
                blurRadius: 4,
                color: kItemShadowColor,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'See All Reviews',
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(width: 8),
              const Icon(
                Icons.arrow_forward_ios_outlined,
                size: 20,
              ),
            ],
          ),
        ),
        const SizedBox(height: 20),
      ],
    );
  }

  Widget _buildProductQA() {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Product Q/A',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 20),
          TextFormField(
            controller: questionTextController,
            maxLines: 5,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            textAlign: TextAlign.start,
            decoration: InputDecoration(
              hintText: 'Write your question...',
              hintStyle: const TextStyle(
                fontStyle: FontStyle.italic,
              ),
              isDense: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
          const SizedBox(height: 20),
          Align(
            alignment: Alignment.centerRight,
            child: KOutlinedButton(
              onPressed: () {
                if (questionTextController.text.isEmpty) {
                  kSnackBar(
                    context: context,
                    message: 'Please write some question',
                    bgColor: failedColor,
                  );
                }
              },
              borderRadius: 30,
              height: 40,
              width: context.screenWidth * 0.32,
              borderColor: mainColor,
              child: Text(
                'Submit',
                style: h3.copyWith(
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
