import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/best_seller_award_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/category_wise_books_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/exam_items_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/explore_by_genres_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/favorite_books_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/home_slider_widget.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/middle_home_items.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/recently_sold_items.dart';
import 'package:book_store/src/customer_features/home/presentation/widgets/top_home_items.dart';
import 'package:flutter/material.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const HomeSliderWidget(
            sliderList: [
              AssetPath.sliderImage1,
              AssetPath.sliderImage2,
            ],
          ),
          const MiddleHomeItems(),
          const RecentlySoldItems(),
          const ExamItemsWidget(),
          const BestSellerAwardWidget(),
          FavoriteBooksWidget(),
          ExploreByGenresWidget(),
          CategoryWiseBooksWidget()
        ],
      ),
    );
  }
}
