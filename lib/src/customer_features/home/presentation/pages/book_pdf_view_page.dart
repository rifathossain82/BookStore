import 'dart:async';

import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';

class BookPdfViewPage extends StatefulWidget {
  const BookPdfViewPage({Key? key}) : super(key: key);

  @override
  State<BookPdfViewPage> createState() => _BookPdfViewPageState();
}

class _BookPdfViewPageState extends State<BookPdfViewPage> {
  final Completer<PDFViewController> _pdfViewController =
      Completer<PDFViewController>();
  final StreamController<String> _pageCountController =
      StreamController<String>();

  int totalPage = 0;
  int currentPage = 0;

  @override
  Widget build(BuildContext context) {
    final String pdfUrl = context.getArguments;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            PDF(
              swipeHorizontal: true,
              pageFling: true,
              onPageChanged: (int? _current, int? total) {
                _pageCountController.add('${_current! + 1} - $total');
                setState(() {
                  currentPage = _current + 1;
                  totalPage = total!;
                });
              },
              onViewCreated: (PDFViewController pdfViewController) async {
                _pdfViewController.complete(pdfViewController);
                final int _currentPage =
                    await pdfViewController.getCurrentPage() ?? 0;
                final int? pageCount = await pdfViewController.getPageCount();
                setState(() {
                  currentPage = _currentPage + 1;
                  totalPage = pageCount!;
                });
              },
            ).cachedFromUrl(
              pdfUrl,
              placeholder: (progress) => Center(
                child: Image.asset(
                  AssetPath.placeholder,
                  fit: BoxFit.cover,
                ),
              ),
              errorWidget: (error) => Center(
                child: Icon(
                  size: 30,
                  Icons.image_not_supported,
                  color: kBlueGrey,
                ),
              ),
            ),
            Positioned(
              bottom: 20,
              child: Container(
                height: 8,
                alignment: Alignment.center,
                width: context.screenWidth,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: totalPage,
                  itemBuilder: (context, index) => Container(
                    width: 8,
                    color: currentPage - 1 == index ? kBlack : kGreyMedium,
                  ),
                  separatorBuilder: (context, index) => const SizedBox(width: 5),
                ),
              ),
            ),
            Positioned(
              top: 30,
              left: 15,
              child: _buildBackButton(context),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBackButton(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          color: kBlack,
          shape: BoxShape.circle,
        ),
        alignment: Alignment.center,
        child: Icon(
          Icons.close,
          color: kWhite,
          size: 15,
        ),
      ),
    );
  }
}
