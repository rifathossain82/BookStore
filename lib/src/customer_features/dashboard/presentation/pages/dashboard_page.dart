import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/widgets/icon_button_builder.dart';
import 'package:book_store/src/customer_features/account/presentation/pages/account_bottom_sheet.dart';
import 'package:book_store/src/customer_features/dashboard/presentation/widgets/bottom_navigation_item_builder.dart';
import 'package:book_store/src/customer_features/home/presentation/pages/home_page.dart';
import 'package:book_store/src/customer_features/menu/presentation/pages/menu_bottom_sheet.dart';
import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2,
        title: Image.asset(
          AssetPath.bookStoreLogo,
          width: 70,
        ),
        actions: [
          IconButtonBuilder(
            onTap: () => Navigator.pushNamed(
              context,
              RouteGenerator.notifications,
            ),
            icon: Icons.notifications_none,
            color: kGrey,
          ),
          IconButtonBuilder(
            onTap: () => Navigator.pushNamed(
              context,
              RouteGenerator.searchPage,
            ),
            icon: Icons.search,
            color: kGrey,
          ),
        ],
      ),
      body: const Homepage(),
      bottomNavigationBar: Container(
        height: 60,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          color: kWhite,
          boxShadow: [
            BoxShadow(
              color: kBlackLight.withOpacity(0.2),
              spreadRadius: 0,
              blurRadius: 5,
              offset: const Offset(0, -3),
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            BottomNavigationItemBuilder(
              onTap: () {},
              iconData: Icons.home_outlined,
              name: 'Home',
              isSelected: true,
            ),
            BottomNavigationItemBuilder(
              onTap: () => Navigator.pushNamed(
                context,
                RouteGenerator.cart,
              ),
              iconData: Icons.shopping_cart_outlined,
              name: 'Cart',
              isSelected: false,
            ),
            BottomNavigationItemBuilder(
              onTap: () => openAccountBottomSheet(context),
              iconData: Icons.person_rounded,
              name: 'Account',
              isSelected: false,
            ),
            BottomNavigationItemBuilder(
              onTap: () {},
              iconData: Icons.local_library_rounded,
              name: 'E-Library',
              isSelected: false,
            ),
            BottomNavigationItemBuilder(
              onTap: () => openMenuBottomSheet(context),
              iconData: Icons.menu_rounded,
              name: 'Menu',
              isSelected: false,
            ),
          ],
        ),
      ),
    );
  }
}
