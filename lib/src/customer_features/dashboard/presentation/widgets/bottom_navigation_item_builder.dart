import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class BottomNavigationItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final IconData iconData;
  final String name;
  final bool isSelected;

  const BottomNavigationItemBuilder({
    Key? key,
    required this.onTap,
    required this.iconData,
    required this.name,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            iconData,
            color: isSelected ? mainColor : kGrey,
          ),
          Text(
            name,
            style: h5.copyWith(
              color: isSelected ? mainColor : kGrey,
            ),
          )
        ],
      ),
    );
  }
}
