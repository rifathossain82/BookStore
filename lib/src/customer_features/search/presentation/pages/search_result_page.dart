import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/customer_features/search/presentation/widgets/search_book_item_builder.dart';
import 'package:flutter/material.dart';

class SearchResultPage extends StatelessWidget {
  final List<FakeBookData> searchResult;

  const SearchResultPage({
    Key? key,
    required this.searchResult,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.builder(
          itemCount: searchResult.length,
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
          ),
          itemBuilder: (context, index) => SearchBookItemBuilder(
            bookData: searchResult[index],
          ),
        ),
      ),
    );
  }
}
