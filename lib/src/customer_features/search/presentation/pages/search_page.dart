import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/customer_features/search/presentation/pages/not_found_in_search_page.dart';
import 'package:book_store/src/customer_features/search/presentation/pages/search_result_page.dart';
import 'package:book_store/src/customer_features/search/presentation/widgets/search_tag_item_builder.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final searchTextController = TextEditingController();
  final searchTextFocusNode = FocusNode();
  bool hasResult = false;

  final tagList = [
    'Ebook',
    'নতুন উপন্যাস',
    'বিজ্ঞানবাক্স',
    'Baby Care',
    'Xiaomi Original',
    'Laptop bag',
    'স্টকে আছে',
    'গনিত অলিম্পিয়াড',
  ];

  @override
  void initState() {
    searchTextFocusNode.requestFocus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            /// back button and search text field
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 35,
                      color: kBlackLight,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: searchTextController,
                      focusNode: searchTextFocusNode,
                      onChanged: (value) {
                        setState(() {
                          if (value.isNotEmpty) {
                            if (value.contains('a')) {
                              hasResult = true;
                            }
                          } else {
                            hasResult = false;
                          }
                        });
                      },
                      decoration: InputDecoration(
                        isDense: true,
                        filled: true,
                        fillColor: kWhite,
                        hintText: 'What are you looking for?',
                        border: const OutlineInputBorder(),
                        suffixIcon: const Icon(
                          Icons.search,
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            /// search page content
            Expanded(
              child: searchTextController.text.isNotEmpty && hasResult
                  ? SearchResultPage(
                      searchResult: fakeBookList,
                    )
                  : searchTextController.text.isNotEmpty && !hasResult
                      ? const NotFoundInSearchPage()
                      : _buildSearchTag(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSearchTag() {
    return GridView.builder(
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4, // Number of columns
        childAspectRatio: 6 / 2, // Width-to-height ratio of items
        crossAxisSpacing: 2, // Spacing between columns
        mainAxisSpacing: 10, // Spacing between rows
      ),
      itemCount: tagList.length,
      itemBuilder: (context, index) => SearchTagItemBuilder(
        tagName: tagList[index],
        onTap: (){
          searchTextController.text = tagList[index];
          setState(() {
            hasResult = true;
          });
        },
      ),
    );
  }
}
