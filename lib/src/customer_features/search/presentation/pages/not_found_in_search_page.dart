import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:flutter/material.dart';

class NotFoundInSearchPage extends StatelessWidget {
  const NotFoundInSearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Result 0',
              style: h3.copyWith(
                color: kGreyTextColor,
              ),
            ),
          ),
          SizedBox(height: context.screenHeight * 0.1),
          Text(
            'Not Found Book In Search',
            textAlign: TextAlign.center,
            style: h2.copyWith(
              color: kGreyTextColor,
            ),
          ),
          const SizedBox(height: 20),
          KButton(
            width: context.screenWidth * 0.5,
            bgColor: kDeepOrange,
            onPressed: () => Navigator.pushNamed(
              context,
              RouteGenerator.notFoundSearchBookRequest,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Get a Book Request',
                  style: h4.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(width: 15),
                Icon(
                  Icons.arrow_forward_ios_outlined,
                  color: kWhite,
                  size: 15,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
