import 'dart:io';

import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/services/image_services.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/search/presentation/widgets/book_request_text_form_field_builder.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class NotFoundSearchBookRequestPage extends StatefulWidget {
  const NotFoundSearchBookRequestPage({Key? key}) : super(key: key);

  @override
  State<NotFoundSearchBookRequestPage> createState() =>
      _NotFoundSearchBookRequestPageState();
}

class _NotFoundSearchBookRequestPageState
    extends State<NotFoundSearchBookRequestPage> {
  final referenceTextController = TextEditingController();
  final bookNameTextController = TextEditingController();
  final subjectTextController = TextEditingController();
  final descriptionTextController = TextEditingController();

  List<File> attachmentList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Not Found Search Book Request'),
        elevation: 2,
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              BookRequestTextFormFieldBuilder(
                label: 'Hyper-reference/Url',
                textInputType: TextInputType.url,
                textInputAction: TextInputAction.next,
                controller: referenceTextController,
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return Message.emptyReference;
                  }
                  return null;
                },
              ),
              BookRequestTextFormFieldBuilder(
                label: 'Book Name',
                textInputType: TextInputType.name,
                textInputAction: TextInputAction.next,
                controller: bookNameTextController,
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return Message.emptyBookName;
                  }
                  return null;
                },
              ),
              BookRequestTextFormFieldBuilder(
                label: 'Subject',
                textInputType: TextInputType.text,
                textInputAction: TextInputAction.next,
                controller: subjectTextController,
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return Message.emptySubject;
                  }
                  return null;
                },
              ),
              _buildAttachmentWidget(),
              BookRequestTextFormFieldBuilder(
                label: 'Description',
                maxLine: 5,
                minLine: 5,
                textInputType: TextInputType.text,
                textInputAction: TextInputAction.done,
                controller: descriptionTextController,
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return Message.emptyDescription;
                  }
                  return null;
                },
              ),
              KButton(
                onPressed: () {},
                bgColor: kDeepOrange,
                child: Text(
                  'Submit',
                  style: h4.copyWith(
                    color: kWhite,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAttachmentWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Attachment',
            style: GoogleFonts.roboto(
              textStyle: h3.copyWith(
                color: kBlackLight.withOpacity(0.8),
              ),
            )),
        const SizedBox(height: 5),
        Container(
          height: 150,
          width: context.screenWidth,
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            border: Border.all(
              color: kGrey,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(4),
          ),
          child: ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: attachmentList.length + 1,
            itemBuilder: (context, index) {
              if (attachmentList.length == index) {
                if (attachmentList.length < 3) {
                  return _buildAttachmentAddButton();
                } else {
                  return Container();
                }
              } else {
                return _buildAttachmentItem(index);
              }
            },
            separatorBuilder: (context, index) => const SizedBox(
              width: 15,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildAttachmentAddButton() {
    return GestureDetector(
      onTap: () async {
        final img = await ImageServices.galleryImage();
        var imageFile = await ImageServices.getImageFile(img);
        setState(() {
          attachmentList.add(imageFile);
        });
      },
      child: Container(
        width: context.screenWidth * 0.25,
        decoration: BoxDecoration(
          color: kGrey.withOpacity(0.2),
          borderRadius: BorderRadius.circular(2),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.camera_alt,
            ),
            const SizedBox(height: 8),
            Text(
              '(${attachmentList.length}/3)',
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAttachmentItem(int index) {
    return Stack(
      children: [
        Container(
          width: context.screenWidth * 0.25,
          decoration: BoxDecoration(
            color: kGrey,
            borderRadius: BorderRadius.circular(2),
          ),
          child: Image.file(
            attachmentList[index],
          ),
        ),
        Positioned(
          top: -5,
          right: -5,
          child: GestureDetector(
            onTap: () {
              setState(() {
                attachmentList.removeAt(index);
              });
            },
            child: Container(
              height: 30,
              width: 30,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kWhite,
              ),
              child: Icon(
                Icons.close,
                color: kRed,
                size: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
