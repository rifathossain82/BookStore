import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class SearchTagItemBuilder extends StatelessWidget {
  final String tagName;
  final VoidCallback onTap;

  const SearchTagItemBuilder({
    Key? key,
    required this.tagName,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(100),
          border: Border.all(
            color: kGrey,
            width: 1,
          ),
        ),
        child: Center(
          child: Text(
            tagName,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: h6,
          ),
        ),
      ),
    );
  }
}
