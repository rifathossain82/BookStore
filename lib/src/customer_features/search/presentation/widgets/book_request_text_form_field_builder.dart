import 'package:flutter/material.dart';

class BookRequestTextFormFieldBuilder extends StatelessWidget {
  final String label;
  final TextInputType? textInputType;
  final TextInputAction? textInputAction;
  final TextEditingController controller;
  final FormFieldValidator? validator;
  final int? maxLine;
  final int? minLine;

  const BookRequestTextFormFieldBuilder({
    Key? key,
    required this.label,
    required this.textInputType,
    required this.textInputAction,
    required this.controller,
    this.validator,
    this.maxLine,
    this.minLine,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: TextFormField(
        controller: controller,
        validator: validator,
        maxLines: maxLine ?? 1,
        minLines: minLine,
        textInputAction: textInputAction,
        keyboardType: textInputType,
        decoration: InputDecoration(
          isDense: true,
          labelText: label,
          border: const OutlineInputBorder(),
        ),
      ),
    );
  }
}
