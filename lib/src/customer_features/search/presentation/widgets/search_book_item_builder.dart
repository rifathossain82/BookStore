import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/material.dart';

class SearchBookItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const SearchBookItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        child: Row(
          children: [

            /// book image
            Expanded(
              flex: 1,
              child: ProductImageBuilder(
                height: 60,
                imgUrl: bookData.image!,
                borderRadius: 0,
                enableShadow: false,
              ),
            ),
            const SizedBox(width: 10),

            /// books name
            Expanded(
              flex: 5,
              child: Text(
                bookData.name!,
                style: h4,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),

            /// in stock text
            Expanded(
              flex: 1,
              child: Text(
                'In Stock',
                style: h5.copyWith(
                  color: kGreen,
                ),
              ),
            ),

            /// books prices
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Text(
                    '${bookData.regularPrice} Tk',
                    style: h4.copyWith(
                      color: kRed,
                      decoration: TextDecoration.lineThrough,
                    ),
                  ),
                  const SizedBox(width: 8),
                  Text(
                    '${bookData.offerPrice} Tk',
                    style: h4,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
