import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/add_new_address_drop_down_builder.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/add_new_address_text_field_builder.dart';
import 'package:flutter/material.dart';

class AddNewAddressPage extends StatefulWidget {
  const AddNewAddressPage({Key? key}) : super(key: key);

  @override
  State<AddNewAddressPage> createState() => _AddNewAddressPageState();
}

class _AddNewAddressPageState extends State<AddNewAddressPage> {

  final nameTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  final altPhoneTextController = TextEditingController();
  final addressTextController = TextEditingController();

  String? selectedCountry;
  String? selectedCity;
  String? selectedArea;
  AddressType? selectedAddress;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Add New Address'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _buildAddNewAddressCard(),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomWidget(),
          ),
        ],
      ),
    );
  }

  /// total items count, total price text and next to save address button
  Widget _buildBottomWidget() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: KOutlinedButton(
        onPressed: saveAddressMethod,
        borderColor: mainColor,
        child: Text(
          'Save Address',
          style: h3.copyWith(
            fontWeight: FontWeight.bold,
            color: mainColor,
          ),
        ),
      ),
    );
  }

  Widget _buildAddNewAddressCard() {
    return Container(
      margin: const EdgeInsets.all(15),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(4),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              spreadRadius: 1,
              blurRadius: 4,
              color: kItemShadowColor,
            ),
          ]),
      child: Column(
        children: [
          AddNewAddressTextFieldBuilder(
            controller: nameTextController,
            validator: (value) {
              if (value.toString().isEmpty) {
                return Message.emptyName;
              }
              return null;
            },
            hintText: '* Name',
          ),
          Row(
            children: [
              Expanded(
                child: AddNewAddressTextFieldBuilder(
                  controller: phoneTextController,
                  validator: (value) {
                    if (value.toString().isEmpty) {
                      return Message.emptyPhone;
                    }
                    return null;
                  },
                  hintText: '* Phone',
                ),
              ),
              const SizedBox(width: 15),
              Expanded(
                child: AddNewAddressTextFieldBuilder(
                  controller: altPhoneTextController,
                  hintText: 'Alt. Phone',
                ),
              ),
            ],
          ),
          AddNewAddressDropDownBuilder(
            items: fakeCountryNameList,
            selectedValue: selectedCountry,
            onChanged: (value) {
              setState(() {
                selectedCountry = value;
              });
            },
            validator: (value) {
              if (selectedCountry == null) {
                return 'Country is required!';
              }
              return null;
            },
            hintText: '* Select Country',
          ),
          Row(
            children: [
              Expanded(
                child: AddNewAddressDropDownBuilder(
                  items: fakeCityNameList,
                  selectedValue: selectedCity,
                  onChanged: (value) {
                    setState(() {
                      selectedCity = value;
                    });
                  },
                  validator: (value) {
                    if (selectedCity == null) {
                      return 'City is required!';
                    }
                    return null;
                  },
                  hintText: '* Select City',
                ),
              ),
              const SizedBox(width: 15),
              Expanded(
                child: AddNewAddressDropDownBuilder(
                  items: fakeAreaNameList,
                  selectedValue: selectedArea,
                  onChanged: (value) {
                    setState(() {
                      selectedArea = value;
                    });
                  },
                  validator: (value) {
                    if (selectedArea == null) {
                      return 'City is area!';
                    }
                    return null;
                  },
                  hintText: '* Select Area',
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: RadioListTile(
                  visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity,
                  ),
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    'Home',
                    style: h4,
                  ),
                  value: AddressType.home,
                  groupValue: selectedAddress,
                  onChanged: (value) {
                    setState(() {
                      selectedAddress = value;
                    });
                  },
                ),
              ),
              const SizedBox(width: 15),
              Expanded(
                child: RadioListTile(
                  visualDensity: const VisualDensity(
                    horizontal: VisualDensity.minimumDensity,
                    vertical: VisualDensity.minimumDensity,
                  ),
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    'Office',
                    style: h4,
                  ),
                  value: AddressType.office,
                  groupValue: selectedAddress,
                  onChanged: (value) {
                    setState(() {
                      selectedAddress = value;
                    });
                  },
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          AddNewAddressTextFieldBuilder(
            controller: addressTextController,
            validator: (value) {
              if (value.toString().isEmpty) {
                return 'Address is required!';
              }
              return null;
            },
            maxLine: 4,
            minLine: 4,
            hintText: '* Address',
          ),
        ],
      ),
    );
  }

  void saveAddressMethod() {
    /// TODO: Your Methods
    Navigator.pop(context);
  }
}
