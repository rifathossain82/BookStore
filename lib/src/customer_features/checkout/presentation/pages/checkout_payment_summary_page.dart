import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/add_new_address_button.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/add_promo_code_text_field.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_address_item_builder.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_stepper_builder.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/gift_wrap_checkbox_widget.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/no_address_found_widget.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/payment_summary_widget.dart';
import 'package:flutter/material.dart';

class CheckoutPaymentSummaryPage extends StatefulWidget {
  const CheckoutPaymentSummaryPage({Key? key}) : super(key: key);

  @override
  State<CheckoutPaymentSummaryPage> createState() =>
      _CheckoutPaymentSummaryPageState();
}

class _CheckoutPaymentSummaryPageState
    extends State<CheckoutPaymentSummaryPage> {

  /// received from cart page
  FakeCheckoutData? checkoutData;

  AddressType? selectedAddressType;
  bool isGiftWrapChecked = true;
  final promoCodeTextController = TextEditingController();
  final List<FakeAddressData> addressList = [];

  @override
  void didChangeDependencies() {
    checkoutData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Checkout (1/3)'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: SingleChildScrollView(
              child: addressList.isEmpty
                  ? const NoAddressFoundWidget()
                  : _buildPaymentSummaryContent(),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomWidget(),
          ),
        ],
      ),
    );
  }

  /// stepper, address list, add new address button etc........
  Widget _buildPaymentSummaryContent() {
    return Column(
      children: [
        /// checkout page custom stepper
        const CheckoutStepperBuilder(
          deliveryAddressStatus: CheckoutStepperStatus.running,
          paymentStatus: CheckoutStepperStatus.uncompleted,
          orderPlacedStatus: CheckoutStepperStatus.uncompleted,
        ),

        /// address list
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: fakeAddressList.length,
          itemBuilder: (context, index) => CheckoutAddressItemBuilder(
            data: fakeAddressList[index],
            selectedAddressType: selectedAddressType,
            onChanged: (value) {
              setState(() {
                selectedAddressType = value;
              });
            },
          ),
        ),

        /// add new address button
        AddNewAddressButton(
          onTap: () => Navigator.pushNamed(
            context,
            RouteGenerator.addNewAddress,
            arguments: checkoutData,
          ),
        ),

        /// gift wrap checkbox
        GiftWrapCheckboxButton(
          isGiftWrapChecked: isGiftWrapChecked,
          onChanged: (value) {
            setState(() {
              isGiftWrapChecked = value;
            });
          },
        ),

        /// add promo code / gift voucher text field and submit button
        AddPromoCodeTextField(
          controller: promoCodeTextController,
          onSubmit: () {},
        ),

        /// payment summary widget
        PaymentSummaryWidget(
          checkoutData: checkoutData!,
          isGiftWrapSelected: isGiftWrapChecked,
        ),

        /// give space since bottom widget of stake take 65 px
        const SizedBox(height: 70),
      ],
    );
  }

  /// total items count, total price text and next to payment button
  Widget _buildBottomWidget() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(3, 0),
            spreadRadius: 2,
            blurRadius: 4,
            color: kGreyMedium,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${checkoutData!.items.length} items in cart',
                style: h5.copyWith(
                  color: kGrey,
                ),
              ),
              Text(
                'Total: ${checkoutData!.total} ${AppConstants.currency}',
                style: h4,
              ),
            ],
          ),
          KButton(
            onPressed: nextToPaymentMethod,
            borderRadius: 20,
            width: context.screenWidth * 0.4,
            child: Text(
              'Next to Payment',
              style: h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void nextToPaymentMethod() {
    Navigator.pushNamed(
      context,
      RouteGenerator.checkoutPaymentMethod,
      arguments: checkoutData,
    );

    // if (selectedAddressType != null) {
    //   Navigator.pushNamed(
    //     context,
    //     RouteGenerator.checkoutPaymentMethod,
    //     arguments: checkoutData,
    //   );
    // } else {
    //   kSnackBar(
    //     context: context,
    //     message: 'Please select any address!',
    //     bgColor: failedColor,
    //   );
    // }
  }
}
