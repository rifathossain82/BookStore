import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_payment_method_builder.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_payment_method_rules_widget.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_stepper_builder.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/payable_amount_text_widget.dart';
import 'package:flutter/material.dart';

class CheckoutPaymentMethodPage extends StatefulWidget {
  const CheckoutPaymentMethodPage({Key? key}) : super(key: key);

  @override
  State<CheckoutPaymentMethodPage> createState() =>
      _CheckoutPaymentMethodPageState();
}

class _CheckoutPaymentMethodPageState extends State<CheckoutPaymentMethodPage> {
  /// received from checkout payment summary page
  FakeCheckoutData? checkoutData;

  String? selectedPaymentMethod;
  bool isAgreeWithRules = true;

  @override
  void didChangeDependencies() {
    checkoutData = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Checkout (2/3)'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Stack(
        children: [
          SizedBox(
            height: context.screenHeight,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  /// checkout stepper
                  const CheckoutStepperBuilder(
                    deliveryAddressStatus: CheckoutStepperStatus.completed,
                    paymentStatus: CheckoutStepperStatus.running,
                    orderPlacedStatus: CheckoutStepperStatus.uncompleted,
                  ),

                  /// payable text here
                  PayableAmountTextWidget(
                    payableAmount: checkoutData!.payableTotal,
                  ),

                  /// cash on delivery payment method
                  CheckoutPaymentMethodBuilder(
                    paymentMethodValue: 'Cash On delivery',
                    paymentMethodGroupValue: selectedPaymentMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedPaymentMethod = value;
                      });
                    },
                    title: 'Cash on delivery',
                    subtitle:
                        'After receiving the parcel, pay to the delivery man',
                    instructions: const [
                      'Click Confirm Order button bellow',
                    ],
                  ),

                  /// bkash payment method
                  CheckoutPaymentMethodBuilder(
                    paymentMethodValue: 'bkash',
                    paymentMethodGroupValue: selectedPaymentMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedPaymentMethod = value;
                      });
                    },
                    title: 'bkash',
                    subtitle: 'Pay from your bkash account or any local agent',
                    instructions: const [
                      'Click Confirm Order button bellow',
                      'Get your order id in the next page',
                      'Follow payment instructions in the next page to complete payment.',
                    ],
                  ),

                  /// nagad payment method
                  CheckoutPaymentMethodBuilder(
                    paymentMethodValue: 'Nagad',
                    paymentMethodGroupValue: selectedPaymentMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedPaymentMethod = value;
                      });
                    },
                    title: 'Nagad',
                    subtitle: 'Pay from your Nagad account or any local agent',
                    instructions: const [
                      'Click Confirm Order button bellow',
                      'Get your order id in the next page',
                      'Follow payment instructions in the next page to complete payment.',
                    ],
                  ),

                  /// rocket payment method
                  CheckoutPaymentMethodBuilder(
                    paymentMethodValue: 'Rocket',
                    paymentMethodGroupValue: selectedPaymentMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedPaymentMethod = value;
                      });
                    },
                    title: 'Rocket',
                    subtitle: 'Pay from your Rocket account or any local agent',
                    instructions: const [
                      'Click Confirm Order button bellow',
                      'Get your order id in the next page',
                      'Follow payment instructions in the next page to complete payment.',
                    ],
                  ),

                  /// card payment method
                  CheckoutPaymentMethodBuilder(
                    paymentMethodValue: 'Card Payment',
                    paymentMethodGroupValue: selectedPaymentMethod,
                    onChanged: (value) {
                      setState(() {
                        selectedPaymentMethod = value;
                      });
                    },
                    title: 'Card Payment',
                    subtitle: 'You will be redirected to your bank payment',
                    instructions: const [
                      'Click Confirm Order button bellow',
                      'Please wait for payment box',
                      'Follow payment instruction to complete payment',
                    ],
                  ),

                  /// customer rules to confirm order
                  CheckoutPaymentMethodRulesWidget(
                    isSelected: isAgreeWithRules,
                    onChanged: (value) {
                      setState(() {
                        isAgreeWithRules = value;
                      });
                    },
                  ),

                  /// give space since bottom widget of stake take 65 px
                  const SizedBox(height: 70),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomWidget(),
          ),
        ],
      ),
    );
  }

  /// total items count, total price text and next to payment button
  Widget _buildBottomWidget() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(3, 0),
            spreadRadius: 2,
            blurRadius: 4,
            color: kGreyMedium,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${checkoutData!.items.length} items in cart',
                style: h5.copyWith(
                  color: kGrey,
                ),
              ),
              Text(
                'Total: ${checkoutData!.total} ${AppConstants.currency}',
                style: h4,
              ),
            ],
          ),
          KButton(
            onPressed: confirmOrderMethod,
            borderRadius: 20,
            width: context.screenWidth * 0.4,
            child: Text(
              'Confirm Order',
              style: h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void confirmOrderMethod() {
    if (selectedPaymentMethod == null) {
      kSnackBar(
        context: context,
        message: 'Please select any payment method!',
        bgColor: failedColor,
      );
    } else if (isAgreeWithRules) {

      /// TODO: Confirm Order methods

      /// after confirm order method,
      /// remove all previous page and go to the checkout confirm order page
      Navigator.pushNamedAndRemoveUntil(
        context,
        RouteGenerator.checkoutConfirmOrder,
            (route) => false,
        arguments: '8746549654',
      );

    }
  }
}
