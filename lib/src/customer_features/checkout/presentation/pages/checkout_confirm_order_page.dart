import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class CheckoutConfirmOrderPage extends StatefulWidget {
  const CheckoutConfirmOrderPage({Key? key}) : super(key: key);

  @override
  State<CheckoutConfirmOrderPage> createState() =>
      _CheckoutConfirmOrderPageState();
}

class _CheckoutConfirmOrderPageState extends State<CheckoutConfirmOrderPage> {
  /// received from checkout payment method page
  String? orderID;

  @override
  void didChangeDependencies() {
    orderID = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Your Order has been placed successfully!',
                  textAlign: TextAlign.center,
                  style: h2.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Lottie.asset(
                  AssetPath.acceptedLottie,
                  height: 150,
                  width: 150,
                ),
                const SizedBox(height: 10),
                Text(
                  "প্রিয় পাঠক, আপানার কষ্টের টাকার কেনা পণ্যটি সুন্দরভাবে পাঠানোর জন্য আমাদের বুকস বিডি পরিবারের চেষ্টার কোন ত্রুটি থাকবে না। তবে আমরা কেউ ভুলের ঊর্ধ্বে নই। আপনার পণ্যটি ডেলিভারি ম্যান বা কুরিয়ার থেকে নিশ্চিন্তে বুঝে নিন। প্যাকেট খুলে কোন রকম সমস্যা যেমনঃ পণ্য মিসিং, পরিবহনে পণ্য ক্ষতিগ্রস্ত বা অন্য যে কোন সমস্যা পেলে ৭ দিনের মধ্যে আমাদেরকে sales@booksBD.com ইমেইল করে অথবা ফেসবুকে facebook.com/booksBD এ জানিয়ে দিন। দ্রুততম সময়ে আপনার সমস্যাটি অগ্রাধিকার ভিত্তিতে সমাধান করার প্রয়োজনীয় ব্যবস্থা নেয়া হবে ইন শা আল্লাহ। বিচলিত না হয়ে আস্থা রাখুন আপনার প্রিয় বুকস বিডিতে।",
                  textAlign: TextAlign.start,
                  style: h4,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Track Your Orders',
                      textAlign: TextAlign.start,
                      style: h4.copyWith(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold,
                        color: mainColor,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    KButton(
                      onPressed: () => Navigator.popAndPushNamed(
                        context,
                        RouteGenerator.dashboard,
                      ),
                      borderRadius: 2,
                      width: context.screenWidth * 0.4,
                      child: Text(
                        'Go to Shopping',
                        textAlign: TextAlign.center,
                        style: h3.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                    KButton(
                      onPressed: () {},
                      borderRadius: 2,
                      width: context.screenWidth * 0.4,
                      bgColor: kOrange,
                      child: Text(
                        'Cancel Orders',
                        textAlign: TextAlign.center,
                        style: h3.copyWith(
                          fontWeight: FontWeight.bold,
                          color: kWhite,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
