import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CheckoutAddressItemBuilder extends StatelessWidget {
  final FakeAddressData data;
  final AddressType? selectedAddressType;
  final ValueChanged onChanged;

  const CheckoutAddressItemBuilder({
    Key? key,
    required this.data,
    required this.selectedAddressType,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: RadioListTile(
        value: data.addressType,
        groupValue: selectedAddressType,
        onChanged: onChanged,
        dense: true,
        visualDensity: const VisualDensity(
          horizontal: VisualDensity.minimumDensity,
          vertical: VisualDensity.minimumDensity,
        ),
        contentPadding: EdgeInsets.zero,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              data.addressType == AddressType.home ? Icons.home : Icons.work,
              size: 20,
              color: kGrey,
            ),
            const SizedBox(width: 8),
            Text(
              data.area,
              style: h4,
            ),
            const SizedBox(width: 5),
            Text(
              '(${data.addressType.name.toUpperCase()})',
              style: h5.copyWith(
                color: kGrey,
              ),
            ),
          ],
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 5),
            Text(
              'Name: ${data.name}',
              style: h5,
            ),
            const SizedBox(height: 2),
            Text(
              'Phone: ${data.phone}',
              style: h5,
            ),
            const SizedBox(height: 5),
            Text(
              '${data.address}, ${data.area}, ${data.city}, ${data.country}',
              style: h6.copyWith(
                color: kGrey,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  onPressed: () {
                    kPrint('Edit');
                  },
                  icon: Icon(
                    Icons.edit,
                    color: kGrey,
                    size: 25,
                  ),
                ),
                const SizedBox(width: 10),
                IconButton(
                  onPressed: () {
                    kPrint('Delete');
                  },
                  icon: Icon(
                    Icons.delete,
                    color: kGrey,
                    size: 25,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
