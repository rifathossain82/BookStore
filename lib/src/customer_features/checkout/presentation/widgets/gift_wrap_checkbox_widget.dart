import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class GiftWrapCheckboxButton extends StatelessWidget {
  final bool isGiftWrapChecked;
  final ValueChanged onChanged;

  const GiftWrapCheckboxButton({
    Key? key,
    required this.isGiftWrapChecked,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: CheckboxListTile(
        value: isGiftWrapChecked,
        onChanged: onChanged,
        controlAffinity: ListTileControlAffinity.leading,
        visualDensity: const VisualDensity(
          horizontal: VisualDensity.minimumDensity,
          vertical: VisualDensity.minimumDensity,
        ),
        contentPadding: EdgeInsets.zero,
        title: Text(
          'Gift wrap for 20 Tk.',
          style: h4.copyWith(
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }
}
