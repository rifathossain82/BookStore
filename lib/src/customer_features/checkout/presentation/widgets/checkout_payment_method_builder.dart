import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CheckoutPaymentMethodBuilder extends StatelessWidget {
  final String paymentMethodValue;
  final String? paymentMethodGroupValue;
  final ValueChanged onChanged;
  final String title;
  final String subtitle;
  final List<String> instructions;

  const CheckoutPaymentMethodBuilder({
    Key? key,
    required this.paymentMethodValue,
    required this.paymentMethodGroupValue,
    required this.onChanged,
    required this.title,
    required this.subtitle,
    required this.instructions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          /// build radio list tile
          RadioListTile(
            value: paymentMethodValue,
            groupValue: paymentMethodGroupValue,
            onChanged: onChanged,
            dense: true,
            visualDensity: const VisualDensity(
              horizontal: VisualDensity.minimumDensity,
              vertical: VisualDensity.minimumDensity,
            ),
            contentPadding: EdgeInsets.zero,
            title: ListTile(
              dense: true,
              contentPadding: EdgeInsets.zero,
              visualDensity: const VisualDensity(
                horizontal: VisualDensity.minimumDensity,
                vertical: VisualDensity.minimumDensity,
              ),
              title: Text(
                title,
                style: h4.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                subtitle,
                style: h6,
              ),
            ),
          ),

          /// build instructions
          paymentMethodGroupValue == paymentMethodValue
              ? Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 5),
                      const Divider(),
                      ...List.generate(
                        instructions.length,
                        (index) => Padding(
                          padding: const EdgeInsets.only(
                            bottom: 5,
                          ),
                          child: Text(
                            'Step ${index + 1}: ${instructions[index]}',
                            style: h5.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
              )
              : Container(),
        ],
      ),
    );
  }
}
