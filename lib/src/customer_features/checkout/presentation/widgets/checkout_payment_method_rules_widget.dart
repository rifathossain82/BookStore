import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CheckoutPaymentMethodRulesWidget extends StatelessWidget {
  final bool isSelected;
  final ValueChanged onChanged;

  const CheckoutPaymentMethodRulesWidget({
    Key? key,
    required this.isSelected,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// rules heading text
          Text(
            'গ্রাহকদের সর্বোচ্চ সেবা নিশ্চিত করতে রকমারিতে রয়েছে সুবিশাল ইনভেন্টরি। স্টকে না থাকা প্রোডাক্টগুলো সাপ্লায়ারের নিকট থেকে সংগ্রহ করতে হয় - ',
            style: h4,
          ),
          const SizedBox(height: 8),

          /// all rules
          _buildRulesText(
            '১. আপনার অর্ডারের প্রোডাক্টগুলো সাপ্লায়ারের কাছে না থাকলে সেগুলো ব্যাতিত অবশিষ্ট প্রোডাক্টগুলো পাঠিয়ে দেয়া হবে। এসব ক্ষেত্রে আপনাকে ইমেইলে/এসএমএস/হোয়াটসঅ্যাপ/ফোন এর মাধ্যমে জানিয়ে দেয়া হবে।',
          ),
          _buildRulesText(
            '২. সাপ্লায়ারের পক্ষ থেকে প্রোডাক্টের মূল্য পরিবর্তন হতে পারে। এসব ক্ষেত্রে আপনাকে ইমেইল/এসএমএস/হোয়াটসঅ্যাপ/ফোন এর মাধ্যমে জানিয়ে দেয়া হবে।',
          ),
          _buildRulesText(
            '৩. অমর একুশে বইমেলা শেষ হবার পরে বেশিরভাগ প্রকাশক লম্বা ছুটি কাটান। সেক্ষেত্রে বই সংগ্রহে আমাদের কিছুটা বিলম্ব হতে পারে।',
          ),
          const SizedBox(height: 8),

          /// build checkbox
          CheckboxListTile(
            value: isSelected,
            onChanged: onChanged,
            dense: true,
            visualDensity: const VisualDensity(
              horizontal: VisualDensity.minimumDensity,
              vertical: VisualDensity.minimumDensity,
            ),
            contentPadding: EdgeInsets.zero,
            controlAffinity: ListTileControlAffinity.leading,
            title: Text(
              'এই শর্তগুলো মেনে অর্ডার প্রদান করছি।',
              style: h3,
            ),
          ),
          const SizedBox(height: 5),
          if (!isSelected)
            Text(
              'Please accept the terms and conditions!',
              style: h5.copyWith(
                color: kRed,
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildRulesText(String rule) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 4,
      ),
      child: Text(
        rule,
        style: h5,
      ),
    );
  }
}
