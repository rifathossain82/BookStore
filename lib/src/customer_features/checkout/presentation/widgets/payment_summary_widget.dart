import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/core/widgets/row_text.dart';
import 'package:flutter/material.dart';

class PaymentSummaryWidget extends StatelessWidget {
  final FakeCheckoutData checkoutData;
  final bool isGiftWrapSelected;

  const PaymentSummaryWidget({
    Key? key,
    required this.checkoutData,
    required this.isGiftWrapSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          Text(
            'Payment Summary',
            style: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          const KDivider(height: 30),
          RowText(
            title: 'Subtotal',
            titleStyle: h4,
            value: '${AppConstants.currency} 511',
            valueStyle: h4.copyWith(
              fontWeight: FontWeight.bold,
            ),
            paddingBottom: 8,
          ),
          RowText(
            title: 'Subtotal',
            titleStyle: h4,
            value: '${AppConstants.currency} ${checkoutData.subtotal}',
            valueStyle: h4.copyWith(
              fontWeight: FontWeight.bold,
            ),
            paddingBottom: 8,
          ),
          RowText(
            title: 'Shipping (Changeable)',
            titleStyle: h4,
            value: '${AppConstants.currency} ${checkoutData.shippingCharge}',
            valueStyle: h4.copyWith(
              fontWeight: FontWeight.bold,
            ),
            paddingBottom: 8,
          ),
          isGiftWrapSelected
              ? RowText(
                  title: 'Gift Wrap',
                  titleStyle: h4,
                  value: '${AppConstants.currency} 20',
                  valueStyle: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                  paddingBottom: 8,
                )
              : Container(),
          RowText(
            title: 'Total',
            titleStyle: h4,
            value: '${AppConstants.currency} ${checkoutData.total}',
            valueStyle: h4.copyWith(
              fontWeight: FontWeight.bold,
            ),
            paddingBottom: 8,
          ),
          const KDivider(height: 20),
          RowText(
            title: 'Payable Total',
            titleStyle: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
            value: '${AppConstants.currency} ${checkoutData.payableTotal}',
            valueStyle: h3.copyWith(
              fontWeight: FontWeight.bold,
            ),
            paddingBottom: 8,
          ),
        ],
      ),
    );
  }
}
