import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AddNewAddressTextFieldBuilder extends StatelessWidget {
  final TextEditingController? controller;
  final FormFieldValidator? validator;
  final String hintText;
  final int maxLine;
  final int minLine;

  const AddNewAddressTextFieldBuilder({Key? key,
    required this.controller,
    this.validator,
    required this.hintText,
    this.maxLine = 1,
    this.minLine = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 15,
      ),
      child: TextFormField(
        controller: controller,
        validator: validator,
        minLines: minLine,
        maxLines: maxLine,
        decoration: InputDecoration(
            isDense: true,
            hintText: hintText,
            hintStyle: h4.copyWith(
              color: kGrey,
              fontStyle: FontStyle.italic,
            ),
            border: const OutlineInputBorder()
        ),
      ),
    );
  }
}
