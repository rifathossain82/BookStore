import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class AddPromoCodeTextField extends StatelessWidget {
  final TextEditingController controller;
  final VoidCallback onSubmit;

  const AddPromoCodeTextField({
    Key? key,
    required this.controller,
    required this.onSubmit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Add Promo code or Gift voucher',
            style: h4.copyWith(
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 8),
          TextField(
            controller: controller,
            decoration: InputDecoration(
              isDense: true,
              hintText: 'Type promo/gift voucher code',
              hintStyle: h5.copyWith(
                fontStyle: FontStyle.italic,
                color: kGrey,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(100),
              ),
              suffixIcon: GestureDetector(
                onTap: onSubmit,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 15,
                  ),
                  decoration: BoxDecoration(
                    color: mainColor,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Text(
                    'Submit',
                    style: h4.copyWith(
                      color: kWhite,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
