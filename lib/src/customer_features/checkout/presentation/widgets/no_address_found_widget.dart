import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/add_new_address_button.dart';
import 'package:book_store/src/customer_features/checkout/presentation/widgets/checkout_stepper_builder.dart';
import 'package:flutter/material.dart';

class NoAddressFoundWidget extends StatelessWidget {

  const NoAddressFoundWidget({
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          alignment: Alignment.bottomCenter,
          children: [
            SizedBox(
              height: 200,
              width: double.infinity,
              child: CustomPaint(
                painter: RoundedRectPainter(),
              ),
            ),
            Image.asset(
              AssetPath.globalAddressIcon,
              width: 80,
              height: 80,
            ),
          ],
        ),
        const SizedBox(height: 60),
        Text(
          'No Address Found',
          style: h1.copyWith(
            fontWeight: FontWeight.bold,
            color: mainColor,
          ),
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 5,
          ),
          child: Text(
            "Dear user you haven't saved any address yet. Please press New address to add an address",
            style: h4,
            textAlign: TextAlign.center,
          ),
        ),

        /// checkout page custom stepper
        const CheckoutStepperBuilder(
          deliveryAddressStatus: CheckoutStepperStatus.running,
          paymentStatus: CheckoutStepperStatus.uncompleted,
          orderPlacedStatus: CheckoutStepperStatus.uncompleted,
        ),

        /// add new address button
        AddNewAddressButton(
          onTap: () => Navigator.pushNamed(
            context,
            RouteGenerator.addNewAddress,
          ),
        ),
      ],
    );
  }
}

class RoundedRectPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = kWhite
      ..style = PaintingStyle.fill;

    final path = Path()
      ..lineTo(0, 0)
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..quadraticBezierTo(size.width / 2, size.height * 1.3, 0, size.height)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
