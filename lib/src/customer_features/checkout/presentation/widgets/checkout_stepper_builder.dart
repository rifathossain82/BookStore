import 'package:book_store/src/core/enums/app_enum.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/dashed_line_painter.dart';
import 'package:flutter/material.dart';

class CheckoutStepperBuilder extends StatelessWidget {
  final CheckoutStepperStatus deliveryAddressStatus;
  final CheckoutStepperStatus paymentStatus;
  final CheckoutStepperStatus orderPlacedStatus;

  const CheckoutStepperBuilder({
    Key? key,
    required this.deliveryAddressStatus,
    required this.paymentStatus,
    required this.orderPlacedStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 10,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildStepperItem(
            status: deliveryAddressStatus,
            title: 'Delivery Address',
          ),
          Expanded(
            child: SizedBox(
              height: 15,
              child: CustomPaint(
                painter: DashedLinePainter(),
              ),
            ),
          ),
          _buildStepperItem(
            status: paymentStatus,
            title: 'Payment',
          ),
          Expanded(
            child: SizedBox(
              height: 15,
              child: CustomPaint(
                painter: DashedLinePainter(),
              ),
            ),
          ),
          _buildStepperItem(
            status: orderPlacedStatus,
            title: 'Order Placed',
          ),
        ],
      ),
    );
  }

  Widget _buildStepperItem({
    required CheckoutStepperStatus status,
    required String title,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildStepIcon(status),
        Text(
          title,
          style: h6,
        ),
      ],
    );
  }

  Widget _buildStepIcon(CheckoutStepperStatus status) {
    return status == CheckoutStepperStatus.running
        ? Radio(
            visualDensity: const VisualDensity(
              horizontal: VisualDensity.minimumDensity,
              vertical: VisualDensity.minimumDensity,
            ),
            activeColor: kBlack,
            value: true,
            groupValue: true,
            onChanged: (value) {},
          )
        : status == CheckoutStepperStatus.uncompleted
            ? Radio(
                visualDensity: const VisualDensity(
                  horizontal: VisualDensity.minimumDensity,
                  vertical: VisualDensity.minimumDensity,
                ),
                activeColor: kBlack,
                value: false,
                groupValue: null,
                onChanged: (value) {},
              )
            : Icon(
                Icons.check_circle_rounded,
                color: mainColor,
              );
  }
}
