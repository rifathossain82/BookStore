import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/services/url_launcher_service.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class NotificationsItemBuilder extends StatelessWidget {
  final FakeNotificationData notificationData;

  const NotificationsItemBuilder({
    Key? key,
    required this.notificationData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => LauncherServices.openURL(
        context,
        notificationData.url,
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              spreadRadius: 1,
              blurRadius: 4,
              color: kItemShadowColor,
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              child: Text(
                notificationData.notificationMsg,
                style: h4,
              ),
            ),
            const SizedBox(width: 10),
            Text(
              notificationData.date,
              style: h5.copyWith(
                color: kGrey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
