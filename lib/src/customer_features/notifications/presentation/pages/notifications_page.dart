import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/customer_features/notifications/presentation/widgets/notifications_item_builder.dart';
import 'package:flutter/material.dart';

class NotificationsPage extends StatelessWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('Notifications'),
        centerTitle: true,
        elevation: 2,
      ),
      body: ListView.builder(
        physics: const BouncingScrollPhysics(),
        padding: const EdgeInsets.all(15),
        itemCount: fakeNotificationList.length,
        itemBuilder: (context, index) => NotificationsItemBuilder(
          notificationData: fakeNotificationList[index],
        ),
      ),
    );
  }
}
