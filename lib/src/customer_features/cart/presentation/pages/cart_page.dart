import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/cart/presentation/widgets/cart_item_builder.dart';
import 'package:book_store/src/customer_features/cart/presentation/widgets/cart_page_instruction_widget.dart';
import 'package:flutter/material.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  /// initially select all checkbox is active
  bool isSelectAll = true;

  final cartItems = [
    fakeBookList[0],
    fakeBookList[2],
    fakeBookList[3],
  ];

  /// to store selected cart items
  final selectedCartItems = [];

  @override
  void initState() {
    /// initially all cart items are selected
    selectedCartItems.addAll(cartItems);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        title: const Text('My Cart'),
        centerTitle: true,
        elevation: 2,
      ),
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /// select all and total amount here
              Container(
                color: kWhite,
                child: Row(
                  children: [
                    Expanded(
                      child: CheckboxListTile(
                        value: isSelectAll,
                        controlAffinity: ListTileControlAffinity.leading,
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 5,
                        ),
                        visualDensity: const VisualDensity(
                          horizontal: VisualDensity.minimumDensity,
                        ),
                        onChanged: (value) {
                          setState(() {
                            isSelectAll = !isSelectAll;

                            /// if isSelect is true then clear all items and add again
                            /// if isSelect is false then clear all items from selectedCartItems list
                            if (isSelectAll) {
                              selectedCartItems.clear();
                              selectedCartItems.addAll(cartItems);
                            } else {
                              selectedCartItems.clear();
                            }
                          });
                        },
                        title: Text(
                          'Select All (2/2 items)',
                          style: h4,
                        ),
                      ),
                    ),
                    Text(
                      'Total: 512 Tk.',
                      style: h3.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ),

              /// cart items and cart page instruction here
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      /// build cart items
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: cartItems.length,
                        itemBuilder: (context, index) => CartItemBuilder(
                          bookData: cartItems[index],
                          isSelected:
                              selectedCartItems.contains(cartItems[index]),
                          onChanged: (value) {
                            setState(() {
                              /// change the value of specific cart items
                              if (selectedCartItems
                                  .contains(cartItems[index])) {
                                selectedCartItems.remove(cartItems[index]);
                              } else {
                                selectedCartItems.add(cartItems[index]);
                              }

                              /// if selectedItems and all cart items length are same that means select all should be true
                              /// otherwise set false
                              if (selectedCartItems.length ==
                                  cartItems.length) {
                                isSelectAll = true;
                              } else {
                                isSelectAll = false;
                              }
                            });
                          },
                        ),
                      ),

                      /// build cart page instructions
                      const CartPageInstructionWidget(),
                      const SizedBox(height: 70),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            child: _buildBottomWidget(),
          ),
        ],
      ),
    );
  }

  /// order as a gift and next to shipping button here
  Widget _buildBottomWidget() {
    return Container(
      height: 65,
      width: context.screenWidth,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(15),
        ),
        boxShadow: [
          BoxShadow(
            offset: const Offset(3, 0),
            spreadRadius: 2,
            blurRadius: 4,
            color: kGreyMedium,
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () {},
            child: Text(
              'Order as a Gift',
              style: h4.copyWith(
                color: mainColor,
              ),
            ),
          ),
          KButton(
            onPressed: nextToShippingMethod,
            borderRadius: 20,
            width: context.screenWidth * 0.4,
            child: Text(
              'Next to Shipping',
              style: h3.copyWith(
                fontWeight: FontWeight.bold,
                color: kWhite,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void nextToShippingMethod() {
    if (selectedCartItems.isEmpty) {
      kSnackBar(
        context: context,
        message: 'Please select at least one item!',
        bgColor: failedColor,
      );
    } else{
      Navigator.pushNamed(
        context,
        RouteGenerator.checkoutPaymentSummary,
        arguments: FakeCheckoutData(
          items: cartItems,
          shippingCharge: 48,
          subtotal: 511,
          total: 559,
          payableTotal: 559,
        ),
      );
    }
  }
}
