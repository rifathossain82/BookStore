import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CartPageInstructionWidget extends StatelessWidget {
  const CartPageInstructionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        children: [
          _buildInstructionItem(
            assetPath: AssetPath.cashOnDelivery,
            title: 'Cash on Delivery Available',
          ),
          _buildInstructionItem(
            assetPath: AssetPath.replacement,
            title: '7 Days Replacement Policy',
          ),
          _buildInstructionItem(
            assetPath: AssetPath.moneyBackGuarantee,
            title: '100% Money Back Guarantee',
          ),
          _buildInstructionItem(
            assetPath: AssetPath.purchase,
            title: 'Purchase & Earn Points',
          ),
          _buildInstructionItem(
            assetPath: AssetPath.original,
            title: '100% Original Product',
          ),
        ],
      ),
    );
  }

  Widget _buildInstructionItem({
    required String assetPath,
    required String title,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 8,
      ),
      child: Row(
        children: [
          Image.asset(
            assetPath,
            height: 22,
            width: 22,
            color: kBlackLight,
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              title,
              style: h5.copyWith(
                fontWeight: FontWeight.w700,
              ),
            ),
          )
        ],
      ),
    );
  }
}
