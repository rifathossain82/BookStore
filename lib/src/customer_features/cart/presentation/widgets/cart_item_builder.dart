import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:book_store/src/customer_features/cart/presentation/widgets/qty_increment_decrement_widget.dart';
import 'package:flutter/material.dart';

class CartItemBuilder extends StatelessWidget {
  /// it should be cart data model
  final FakeBookData bookData;
  final bool isSelected;
  final ValueChanged onChanged;

  const CartItemBuilder({
    Key? key,
    required this.bookData,
    required this.isSelected,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Checkbox(
                value: isSelected, // value from cart data model
                onChanged: onChanged, // apply your methods
              ),
            ),
            Expanded(
              flex: 3,
              child: ProductImageBuilder(
                height: 130,
                width: 90,
                imgUrl: bookData.image!,
                borderRadius: 0,
                enableShadow: false,
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 7,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  /// cart item name
                  Text(
                    bookData.name!,
                    style: h4,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5),

                  /// cart item price and increment decrement widgets
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${AppConstants.currency} ${bookData.regularPrice}',
                        style: h4.copyWith(
                          color: kGrey,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Text(
                        '${AppConstants.currency} ${bookData.offerPrice}',
                        style: h4,
                      ),
                      const Spacer(),
                      QtyIncrementDecrementWidget(
                        qty: 1,
                        onIncrement: () {
                          /// TODO: cart qty increment method
                        },
                        onDecrement: () {
                          /// TODO: cart qty decrement method
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),

                  /// delete and move to wishlist buttons
                  GestureDetector(
                    onTap: () {
                      /// TODO: Method to move cart items in wishlist
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.delete,
                            color: kGrey,
                            size: 20,
                          ),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.favorite,
                              color: kGrey,
                              size: 20,
                            ),
                            const SizedBox(width: 10),
                            Text(
                              'Move To Wishlist',
                              style: h5.copyWith(
                                color: kGrey,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
