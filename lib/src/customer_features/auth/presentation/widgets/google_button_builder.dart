import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:flutter/material.dart';

class GoogleButtonBuilder extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;

  const GoogleButtonBuilder({
    Key? key,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KOutlinedButton(
      onPressed: onPressed,
      height: 45,
      borderColor: kGrey,
      borderRadius: 4,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            AssetPath.googleIcon,
            height: 20,
          ),
          const SizedBox(width: 15),
          Text(
            title,
            style: h4.copyWith(
              color: kBlackLight.withOpacity(0.8),
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
