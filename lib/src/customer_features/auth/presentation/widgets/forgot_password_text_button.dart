import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class ForgotPasswordTextButton extends StatelessWidget {
  const ForgotPasswordTextButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () => Navigator.pushNamed(
          context,
          RouteGenerator.forgotPassword,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
          ),
          child: Text(
            'Forgot Password?',
            style: h4.copyWith(
              fontWeight: FontWeight.w600,
              color: mainColor,
            ),
          ),
        ),
      ),
    );
  }
}
