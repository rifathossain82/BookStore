import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class EmailTextFieldBuilder extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged onChanged;
  final bool isValid;

  const EmailTextFieldBuilder({
    Key? key,
    required this.controller,
    required this.onChanged,
    required this.isValid,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyEmail;
        } else if(!value.toString().isValidEmail){
          return Message.invalidEmail;
        }
        return null;
      },
      onChanged: onChanged,
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        labelText: 'Email',
        prefixIcon: const Icon(
          Icons.mail_outline_rounded,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(
            color: isValid ? mainColor : kRedDeep,
          ),
        ),
        isDense: true,
        contentPadding: const EdgeInsets.all(8),
        suffix: isValid
            ? Container(
                alignment: Alignment.center,
                width: 24.0,
                height: 24.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: mainColor,
                ),
                child: const Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 13,
                ),
              )
            : Container(
                alignment: Alignment.center,
                width: 24.0,
                height: 24.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: kRedDeep,
                ),
                child: Icon(
                  Icons.close,
                  color: kWhite,
                  size: 13,
                ),
              ),
      ),
    );
  }
}
