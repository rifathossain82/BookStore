import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class OrTextWidget extends StatelessWidget {
  const OrTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Text(
        'Or',
        style: h3.copyWith(
          color: kGrey,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
