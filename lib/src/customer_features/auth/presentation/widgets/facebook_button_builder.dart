import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:flutter/material.dart';

class FacebookButtonBuilder extends StatelessWidget {
  final VoidCallback onPressed;
  final String title;

  const FacebookButtonBuilder({
    Key? key,
    required this.onPressed,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return KButton(
      onPressed: onPressed,
      height: 45,
      borderRadius: 4,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            AssetPath.facebookIcon,
            height: 20,
            color: kWhite,
          ),
          const SizedBox(width: 15),
          Text(
            title,
            style: h4.copyWith(
              color: kWhite,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
