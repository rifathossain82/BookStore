import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class LogoAndHeaderTextBuilder extends StatelessWidget {
  final String logoAssetPath;
  final String title;
  final String? subtitle;

  const LogoAndHeaderTextBuilder({
    Key? key,
    required this.logoAssetPath,
    required this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 30),
        Image.asset(
          logoAssetPath,
          height: 100,
        ),
        const SizedBox(height: 15),
        Text(
          title,
          style: h2,
        ),
        const SizedBox(height: 10),
        Text(
          subtitle ?? "",
          textAlign: TextAlign.center,
          style: h4,
        ),
      ],
    );
  }
}
