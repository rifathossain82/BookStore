import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class PasswordTextFieldBuilder extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged onChanged;
  final bool isValid;
  final bool passwordVisibility;
  final VoidCallback onChangedPasswordVisibility;

  const PasswordTextFieldBuilder({
    Key? key,
    required this.controller,
    required this.onChanged,
    required this.isValid,
    required this.passwordVisibility,
    required this.onChangedPasswordVisibility,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyPassword;
        } else if (value.toString().length < 8) {
          return Message.invalidPassword;
        }
        return null;
      },
      onChanged: onChanged,
      maxLines: 1,
      keyboardType: TextInputType.visiblePassword,
      textInputAction: TextInputAction.done,
      obscureText: passwordVisibility,
      decoration: InputDecoration(
        labelText: 'Password',
        prefixIcon: const Icon(
          Icons.lock_open,
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(
            color: isValid ? mainColor : kRedDeep,
          ),
        ),
        isDense: true,
        contentPadding: const EdgeInsets.all(8),
        suffixIcon: GestureDetector(
          onTap: onChangedPasswordVisibility,
          child: Icon(
            passwordVisibility ? Icons.visibility_off : Icons.visibility,
            color: kGrey,
            size: 25,
          ),
        ),
        suffix: isValid
            ? Container(
          alignment: Alignment.center,
          width: 24.0,
          height: 24.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: mainColor,
          ),
          child: const Icon(
            Icons.check,
            color: Colors.white,
            size: 13,
          ),
        )
            : Container(
          alignment: Alignment.center,
          width: 24.0,
          height: 24.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: kRedDeep,
          ),
          child: Icon(
            Icons.close,
            color: kWhite,
            size: 13,
          ),
        ),
      ),
    );
  }
}
