import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class BottomTextAndTextButtonBuilder extends StatelessWidget {
  final String questionText;
  final String buttonText;
  final VoidCallback? onPressed;

  const BottomTextAndTextButtonBuilder({
    Key? key,
    required this.questionText,
    required this.buttonText,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
           questionText,
            style: h4.copyWith(),
          ),
          const SizedBox(width: 10),
          GestureDetector(
            onTap: onPressed,
            child: Text(
              buttonText,
              style: h4.copyWith(
                color: mainColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
