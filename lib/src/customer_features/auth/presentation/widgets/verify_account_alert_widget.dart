import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class VerifyAccountAlertWidget extends StatelessWidget {
  const VerifyAccountAlertWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      width: context.screenWidth,
      decoration: BoxDecoration(
        color: mainColor.withOpacity(0.2),
        borderRadius: BorderRadius.circular(4),
      ),
      child: Row(
        children: [
          Icon(
            Icons.error,
            color: mainColor,
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              'You have successfully registered.\nPlease check your phone to verify your account.',
              textAlign: TextAlign.center,
              style: h5,
            ),
          ),
        ],
      ),
    );
  }
}
