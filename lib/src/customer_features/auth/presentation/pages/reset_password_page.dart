import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/logo_and_header_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/password_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/retype_password_text_field_builder.dart';
import 'package:flutter/material.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final resetPasswordFormKey = GlobalKey<FormState>();

  final passwordTextController = TextEditingController();
  final confirmPasswordTextController = TextEditingController();

  bool passwordVisibility = true;
  bool confirmPasswordVisibility = true;

  bool isPasswordTextFieldValid = false;
  bool isConfirmPasswordTextFieldValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo and sign in text here
                    const LogoAndHeaderTextBuilder(
                      logoAssetPath: AssetPath.signInLogo,
                      title: 'Reset Password',
                      subtitle:
                          "Your password must be at least 8 characters in length",
                    ),
                    const SizedBox(height: 20),

                    // rest password card here
                    _buildResetPasswordCard(),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildResetPasswordCard() {
    return Card(
      elevation: 4,
      child: Container(
        width: context.screenWidth,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Form(
          key: resetPasswordFormKey,
          child: Column(
            children: [
              /// password text field
              PasswordTextFieldBuilder(
                controller: passwordTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.length >= 8) {
                      isPasswordTextFieldValid = true;
                    } else {
                      isPasswordTextFieldValid = false;
                    }
                  });
                },
                isValid: isPasswordTextFieldValid,
                passwordVisibility: passwordVisibility,
                onChangedPasswordVisibility: () {
                  setState(() {
                    passwordVisibility = !passwordVisibility;
                  });
                },
              ),
              const SizedBox(height: 15),

              /// confirm password text field
              ReTypePasswordTextFieldBuilder(
                controller: confirmPasswordTextController,
                previousPassword: passwordTextController.text,
                onChanged: (value) {
                  setState(() {
                    if (value.length >= 8) {
                      isConfirmPasswordTextFieldValid = true;
                    } else {
                      isConfirmPasswordTextFieldValid = false;
                    }
                  });
                },
                isValid: isConfirmPasswordTextFieldValid,
                passwordVisibility: confirmPasswordVisibility,
                onChangedPasswordVisibility: () {
                  setState(() {
                    confirmPasswordVisibility = !confirmPasswordVisibility;
                  });
                },
              ),
              const SizedBox(height: 20),

              /// set new password button
              _buildResetPasswordButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildResetPasswordButton() {
    return KButton(
      onPressed: () {
        if (resetPasswordFormKey.currentState!.validate()) {
          Navigator.popUntil(
            context,
            ModalRoute.withName(
              RouteGenerator.signIn,
            ),
          );
        }
      },
      borderRadius: 4,
      height: 45,
      child: Text(
        'Set New Password',
        style: h3.copyWith(
          color: kWhite,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
