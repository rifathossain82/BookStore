import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/email_or_phone_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/logo_and_header_text_builder.dart';
import 'package:flutter/material.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final forgotPasswordFormKey = GlobalKey<FormState>();
  final emailOrPhoneTextController = TextEditingController();
  bool isEmailOrPhoneTextFieldValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo and sign in text here
                    const LogoAndHeaderTextBuilder(
                      logoAssetPath: AssetPath.signInLogo,
                      title: 'Forgot Your Password',
                      subtitle:
                          "No worries, please enter your registered email/phone and we'll send you reset instructions.",
                    ),
                    const SizedBox(height: 20),

                    // forgot password card here
                    _buildForgotPasswordCard(),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildForgotPasswordCard() {
    return Card(
      elevation: 4,
      child: Container(
        width: context.screenWidth,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Form(
          key: forgotPasswordFormKey,
          child: Column(
            children: [
              EmailOrPhoneTextFieldBuilder(
                controller: emailOrPhoneTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().isOnlyDigit) {
                      if(value.toString().isValidPhone){
                        isEmailOrPhoneTextFieldValid = true;
                      } else{
                        isEmailOrPhoneTextFieldValid = false;
                      }
                    } else if (value.toString().isValidEmail) {
                      isEmailOrPhoneTextFieldValid = true;
                    } else {
                      isEmailOrPhoneTextFieldValid = false;
                    }
                  });
                },
                isValid: isEmailOrPhoneTextFieldValid,
              ),
              const SizedBox(height: 20),
              _buildGetOTPButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildGetOTPButton() {
    return KButton(
      onPressed: () {
        if (forgotPasswordFormKey.currentState!.validate()) {
          Navigator.pushNamed(
            context,
            RouteGenerator.otpVerification,
            arguments: emailOrPhoneTextController.text,
          );
        }
      },
      borderRadius: 4,
      height: 45,
      child: Text(
        'Get OTP',
        style: h3.copyWith(
          color: kWhite,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
