import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/verify_account_alert_widget.dart';
import 'package:flutter/material.dart';

class VerifyAccountPage extends StatefulWidget {
  const VerifyAccountPage({Key? key}) : super(key: key);

  @override
  _VerifyAccountPageState createState() => _VerifyAccountPageState();
}

class _VerifyAccountPageState extends State<VerifyAccountPage> {

  final verifyAccountFormKey = GlobalKey<FormState>();
  final resendCodeFormKey = GlobalKey<FormState>();
  final phoneTextController = TextEditingController();
  final verificationCodeTextController = TextEditingController();

  @override
  void didChangeDependencies() {
    phoneTextController.text = context.getArguments;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Center(
              child: Container(
                margin: const EdgeInsets.all(15),
                width: context.screenWidth,
                decoration: BoxDecoration(
                  color: kWhite,
                  borderRadius: BorderRadius.circular(8),
                  // boxShadow: [
                  //   BoxShadow(
                  //     offset: const Offset(0, 3),
                  //     spreadRadius: 0,
                  //     blurRadius: 4,
                  //     color: kItemShadowColor,
                  //   ),
                  // ],
                ),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const VerifyAccountAlertWidget(),
                      SizedBox(height: context.screenHeight * 0.05),
                      Text(
                        'Please Enter Verification Code to Verify Account',
                        textAlign: TextAlign.center,
                        style: h5,
                      ),
                      SizedBox(height: context.screenHeight * 0.05),
                      _buildVerifyAccountFormWidget(),
                      SizedBox(height: context.screenHeight * 0.05),
                      KButton(
                        onPressed: submitMethod,
                        borderRadius: 0,
                        child: Text(
                          'Submit',
                          style: h4.copyWith(
                            fontWeight: FontWeight.bold,
                            color: kWhite,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildVerifyAccountFormWidget(){
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            children: [
              Form(
                key: resendCodeFormKey,
                child: TextFormField(
                  controller: phoneTextController,
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    hintText: 'Phone Number',
                  ),
                  validator: (value){
                    if(value.toString().isEmpty){
                      return Message.emptyPhone;
                    } else if(!value.toString().isValidPhone){
                      return Message.invalidPhone;
                    }
                    return null;
                  },
                ),
              ),
              const SizedBox(height: 15),
              Form(
                key: verifyAccountFormKey,
                child: TextFormField(
                  controller: verificationCodeTextController,
                  keyboardType: TextInputType.phone,
                  decoration: const InputDecoration(
                    hintText: 'Verification Code',
                  ),
                  validator: (value){
                    if(value.toString().isEmpty){
                      return 'Verification code is required!';
                    }
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
        TextButton(
          onPressed: resendMethod,
          child: Text(
            'Resend',
            style: h4.copyWith(
              color: kGreen,
            ),
          ),
        ),
      ],
    );
  }

  void resendMethod(){
    if(resendCodeFormKey.currentState!.validate()){
      ///TODO: Yours Method
    }
  }

  void submitMethod(){
    if(verifyAccountFormKey.currentState!.validate()){
      /// TODO: Yours Method
    }
  }
}
