import 'dart:async';
import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/bottom_rich_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/logo_and_header_text_builder.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';

class OTPVerificationPage extends StatefulWidget {
  const OTPVerificationPage({Key? key}) : super(key: key);

  @override
  State<OTPVerificationPage> createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {
  final otpFormKey = GlobalKey<FormState>();
  final otpTextController = TextEditingController();

  int seconds = 0;

  ///a timer for countdown
  late Timer _timer;

  @override
  void initState() {
    setTimer();
    super.initState();
  }

  ///countdown of session time
  void setTimer() {
    ///initially I set session time of otp is 60 seconds or 1 minute
    seconds = 60;
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (seconds == 0) {
        setState(() {
          timer.cancel();
          kPrint('Time out');
        });
      } else {
        setState(() {
          seconds--;
        });
      }
    });
  }

  @override
  void dispose() {
    otpTextController.dispose();
    _timer.cancel();
    super.dispose();
  }

  ///format the total seconds into minute and seconds
  String getTime(int seconds) {
    var minute = (seconds ~/ 60).toString().padLeft(2, '0');
    var second = (seconds % 60).toString().padLeft(2, '0');

    return '$minute : $second';
  }

  @override
  Widget build(BuildContext context) {
    /// received from forgot password page
    final emailOrPhone = context.getArguments;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo and sign in text here
                    LogoAndHeaderTextBuilder(
                      logoAssetPath: AssetPath.signInLogo,
                      title: 'Verification',
                      subtitle:
                          'Enter 6 digit number that was sent to $emailOrPhone',
                    ),
                    const SizedBox(height: 20),

                    // otp verification card here
                    _buildOTPVerificationCard(),

                    seconds > 0
                        ? BottomTextAndTextButtonBuilder(
                            questionText: "Send Code in",
                            buttonText: '${getTime(seconds)} Sec',
                          )
                        : BottomTextAndTextButtonBuilder(
                            questionText: "Don't Receive Code?",
                            buttonText: 'Please Re-send',
                            onPressed: () {
                              otpTextController.clear();
                              setTimer();
                            },
                          ),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildOTPVerificationCard() {
    return Card(
      elevation: 4,
      child: Container(
        width: context.screenWidth,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Form(
          key: otpFormKey,
          child: Column(
            children: [
              _buildOTPInputTextField(),
              const SizedBox(height: 20),
              _buildVerifyButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildOTPInputTextField() {
    return Pinput(
      controller: otpTextController,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyPin;
        } else if (value.toString().length < 6) {
          return Message.invalidPin;
        }
        return null;
      },
      // onClipboardFound: (value) {
      //   debugPrint('onClipboardFound: $value');
      //   pinController.setText(value);
      // },
      hapticFeedbackType: HapticFeedbackType.lightImpact,
      onCompleted: (pin) {
        kPrint('onCompleted: $pin');
      },
      onChanged: (value) {
        kPrint('onChanged: $value');
      },
      defaultPinTheme: PinTheme(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: kGrey,
              width: 1,
            ),
          ),
        ),
      ),
      length: 6,
      cursor: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            width: 50,
            height: 1,
            color: mainColor,
          ),
        ],
      ),
    );
  }

  Widget _buildVerifyButton() {
    return KButton(
      onPressed: () {
        if (seconds != 0) {
          if (otpFormKey.currentState!.validate()) {
            Navigator.pushNamed(context, RouteGenerator.resetPassword);
          }
        } else {
          kSnackBar(
            context: context,
            message: 'Time Out!',
            bgColor: failedColor,
          );
        }
      },
      borderRadius: 4,
      height: 45,
      child: Text(
        'Verify',
        style: h3.copyWith(
          color: kWhite,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
