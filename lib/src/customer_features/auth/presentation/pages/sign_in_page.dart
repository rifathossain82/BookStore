import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/bottom_rich_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/email_or_phone_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/email_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/facebook_button_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/forgot_password_text_button.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/google_button_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/logo_and_header_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/or_text_widget.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/password_text_field_builder.dart';
import 'package:flutter/material.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {

  final signInFormKey = GlobalKey<FormState>();

  final emailOrPhoneTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  bool passwordVisibility = true;

  bool isEmailOrPhoneTextFieldValid = false;
  bool isPasswordTextFieldValid = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    /// logo and sign in text here
                    const LogoAndHeaderTextBuilder(
                      logoAssetPath: AssetPath.signInLogo,
                      title: 'Sign In',
                      subtitle:
                          'Welcome back! Enter your email/phone and password below to sign in.',
                    ),
                    const SizedBox(height: 20),

                    /// sign in card here
                    _buildSignInCard(),

                    /// don't have an account text and sign up text button
                    BottomTextAndTextButtonBuilder(
                      questionText: "Don't have and account?",
                      buttonText: 'Sign Up',
                      onPressed: () => Navigator.pushNamed(
                        context,
                        RouteGenerator.signUp,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSignInCard() {
    return Card(
      elevation: 4,
      child: Container(
        width: context.screenWidth,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Form(
          key: signInFormKey,
          child: Column(
            children: [
              EmailOrPhoneTextFieldBuilder(
                controller: emailOrPhoneTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().isOnlyDigit) {
                      if(value.toString().isValidPhone){
                        isEmailOrPhoneTextFieldValid = true;
                      } else{
                        isEmailOrPhoneTextFieldValid = false;
                      }
                    } else if (value.toString().isValidEmail) {
                      isEmailOrPhoneTextFieldValid = true;
                    } else {
                      isEmailOrPhoneTextFieldValid = false;
                    }
                  });
                },
                isValid: isEmailOrPhoneTextFieldValid,
              ),
              const SizedBox(height: 15),
              PasswordTextFieldBuilder(
                controller: passwordTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.length >= 8) {
                      isPasswordTextFieldValid = true;
                    } else {
                      isPasswordTextFieldValid = false;
                    }
                  });
                },
                isValid: isPasswordTextFieldValid,
                passwordVisibility: passwordVisibility,
                onChangedPasswordVisibility: () {
                  setState(() {
                    passwordVisibility = !passwordVisibility;
                  });
                },
              ),
              const ForgotPasswordTextButton(),
              _buildSigInButton(),
              const OrTextWidget(),
              FacebookButtonBuilder(
                onPressed: () {},
                title: 'Sign in with Facebook',
              ),
              const SizedBox(height: 15),
              GoogleButtonBuilder(
                onPressed: () {},
                title: 'Sign in with Google',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSigInButton() {
    return KButton(
      onPressed: () {
        if(signInFormKey.currentState!.validate()){
          kPrint('Validate!');
        }
      },
      borderRadius: 4,
      height: 45,
      child: Text(
        'Sign In',
        style: h3.copyWith(
          color: kWhite,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
