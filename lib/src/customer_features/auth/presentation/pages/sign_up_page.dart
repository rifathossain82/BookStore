import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/extensions/string_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/core/widgets/circular_back_button.dart';
import 'package:book_store/src/core/widgets/select_country_code.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/bottom_rich_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/email_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/facebook_button_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/google_button_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/logo_and_header_text_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/name_text_field_builder.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/or_text_widget.dart';
import 'package:book_store/src/customer_features/auth/presentation/widgets/password_text_field_builder.dart';
import 'package:book_store/src/core/widgets/phone_text_field_builder.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final signUpFormKey = GlobalKey<FormState>();

  final nameTextController = TextEditingController();
  final emailTextController = TextEditingController();
  final phoneTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  bool passwordVisibility = true;
  FakeCountryInfo selectedCountry = fakeCountryList.first;
  bool isAgreeTermsAndPolicy = true;

  bool isNameTextFieldValid = false;
  bool isEmailTextFieldValid = false;
  bool isPhoneTextFieldValid = false;
  bool isPasswordTextFieldValid = false;

  String? _checkboxErrorText;

  void _validateCheckbox(bool? value) {
    if (value == null || !value) {
      setState(() {
        _checkboxErrorText = 'Please accept the terms and conditions';
      });
    } else {
      setState(() {
        _checkboxErrorText = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // logo and sign up text here
                    const LogoAndHeaderTextBuilder(
                      logoAssetPath: AssetPath.signInLogo,
                      title: 'Sign Up',
                      subtitle:
                          'Sign up and enjoy life during the time you just saved on rokomari!',
                    ),
                    const SizedBox(height: 20),

                    // sign up card here
                    _buildSignUpCard(),

                    // joined us before text and login text button
                    BottomTextAndTextButtonBuilder(
                      questionText: "Joined us before?",
                      buttonText: 'Login',
                      onPressed: () => Navigator.pushNamed(
                        context,
                        RouteGenerator.signIn,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const Positioned(
              top: 15,
              left: 15,
              child: CircularBackButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSignUpCard() {
    return Card(
      elevation: 4,
      child: Container(
        width: context.screenWidth,
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 20,
        ),
        child: Form(
          key: signUpFormKey,
          child: Column(
            children: [
              NameTextFieldBuilder(
                controller: nameTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().isNotEmpty) {
                      isNameTextFieldValid = true;
                    } else {
                      isNameTextFieldValid = false;
                    }
                  });
                },
                isValid: isNameTextFieldValid,
              ),
              const SizedBox(height: 15),
              EmailTextFieldBuilder(
                controller: emailTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().isValidEmail) {
                      isEmailTextFieldValid = true;
                    } else {
                      isEmailTextFieldValid = false;
                    }
                  });
                },
                isValid: isEmailTextFieldValid,
              ),
              const SizedBox(height: 15),
              PhoneTextFieldBuilder(
                controller: phoneTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.toString().length ==
                        selectedCountry.maxPhoneNoLength) {
                      isPhoneTextFieldValid = true;
                    } else {
                      isPhoneTextFieldValid = false;
                    }
                  });
                },
                isValid: isPhoneTextFieldValid,
                labelText: 'Phone',
                maxLength: selectedCountry.maxPhoneNoLength,
                selectedCountry: selectedCountry,
                onTapPrefixIcon: () async {
                  var result = await selectCountryCode(context);
                  if (result != null) {
                    setState(() {
                      selectedCountry = result;
                    });
                  }
                },
              ),
              const SizedBox(height: 15),
              PasswordTextFieldBuilder(
                controller: passwordTextController,
                onChanged: (value) {
                  setState(() {
                    if (value.length >= 8) {
                      isPasswordTextFieldValid = true;
                    } else {
                      isPasswordTextFieldValid = false;
                    }
                  });
                },
                isValid: isPasswordTextFieldValid,
                passwordVisibility: passwordVisibility,
                onChangedPasswordVisibility: () {
                  setState(() {
                    passwordVisibility = !passwordVisibility;
                  });
                },
              ),
              const SizedBox(height: 20),
              _buildTermsAndPrivacyPolicyCheckbox(),
              const SizedBox(height: 20),
              _buildSigUpButton(),
              const OrTextWidget(),
              FacebookButtonBuilder(
                onPressed: () {},
                title: 'Sign up with Facebook',
              ),
              const SizedBox(height: 15),
              GoogleButtonBuilder(
                onPressed: () {},
                title: 'Sign up with Google',
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTermsAndPrivacyPolicyCheckbox() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CheckboxListTile(
          value: isAgreeTermsAndPolicy,
          visualDensity: const VisualDensity(
            horizontal: VisualDensity.minimumDensity,
          ),
          onChanged: (value) {
            setState(() {
              isAgreeTermsAndPolicy = !isAgreeTermsAndPolicy;
            });
            _validateCheckbox(value);
          },
          controlAffinity: ListTileControlAffinity.leading,
          dense: true,
          contentPadding: EdgeInsets.zero,
          title: RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: 'I agree to the ',
                  style: h4.copyWith(
                    color: kGrey,
                  ),
                ),
                TextSpan(
                  text: 'Terms of Use ',
                  style: h4.copyWith(
                    color: kBlackLight,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(
                  text: 'and ',
                  style: h4.copyWith(
                    color: kGrey,
                  ),
                ),
                TextSpan(
                  text: 'Privacy Policy ',
                  style: h4.copyWith(
                    color: kBlackLight,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ),
        const SizedBox(height: 5),
        if (_checkboxErrorText != null)
          Text(
            _checkboxErrorText!,
            style: h5.copyWith(
              color: kRed,
            ),
          ),
      ],
    );
  }

  Widget _buildSigUpButton() {
    return KButton(
      onPressed: () {
        if (signUpFormKey.currentState!.validate()) {
          if (_checkboxErrorText == null) {
            Navigator.pushNamed(
              context,
              RouteGenerator.verifyAccount,
              arguments: '${selectedCountry.code}${phoneTextController.text}',
            );
          }
        }
      },
      borderRadius: 4,
      height: 45,
      child: Text(
        'Sign Up',
        style: h3.copyWith(
          color: kWhite,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
