import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/widgets/courier_bottom_navigation.dart';
import 'package:book_store/src/courier_features/home/presentation/pages/courier_home_page.dart';
import 'package:flutter/material.dart';

class CourierDashboardPage extends StatefulWidget {
  const CourierDashboardPage({Key? key}) : super(key: key);

  @override
  State<CourierDashboardPage> createState() => _CourierDashboardPageState();
}

class _CourierDashboardPageState extends State<CourierDashboardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: courierBlueShadowColor,
        title: const Text('Dashboard'),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: const EdgeInsets.all(8),
              margin: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: kWhite,
                shape: BoxShape.circle,
              ),
              child: Image.asset(
                AssetPath.profile,
              ),
            ),
          )
        ],
      ),
      body: CourierHomepage(),
      bottomNavigationBar: const CourierBottomNavigation(),
    );
  }
}
