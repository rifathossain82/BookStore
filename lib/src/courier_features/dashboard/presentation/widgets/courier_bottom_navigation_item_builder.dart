import 'package:flutter/material.dart';

class CourierBottomNavigationItemBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String assetIconPath;
  final bool isSelected;

  const CourierBottomNavigationItemBuilder({
    Key? key,
    required this.onTap,
    required this.assetIconPath,
    required this.isSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Image.asset(
        assetIconPath,
        width: 30,
        height: 30,
      ),
    );
  }
}
