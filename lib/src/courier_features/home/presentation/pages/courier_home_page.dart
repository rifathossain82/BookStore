import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/widgets/courier_bottom_navigation.dart';
import 'package:book_store/src/courier_features/home/model/courier_home_item_model.dart';
import 'package:book_store/src/courier_features/home/presentation/widgets/courier_home_item_builder.dart';
import 'package:flutter/material.dart';

class CourierHomepage extends StatelessWidget {
  CourierHomepage({Key? key}) : super(key: key);

  final List<CourierHomeItemModel> homeItems = [
    CourierHomeItemModel(
      routeName: RouteGenerator.collectionRequest,
      title: 'Collection Request',
      totalCount: 30,
    ),
    CourierHomeItemModel(
      routeName: RouteGenerator.deliveryListPage,
      title: 'Delivery List',
      totalCount: 18,
    ),
    CourierHomeItemModel(
      routeName: RouteGenerator.deliveryListPage,
      title: 'Delivered List',
      totalCount: 15,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: courierGreenShadowColor,
      body: SafeArea(
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 15,
            mainAxisSpacing: 15,
          ),
          padding: const EdgeInsets.all(15),
          itemCount: homeItems.length,
          itemBuilder: (context, index) => CourierHomeItemBuilder(
            itemModel: homeItems[index],
          ),
        ),
      ),
    );
  }
}
