import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/courier_features/home/model/courier_home_item_model.dart';
import 'package:flutter/material.dart';

class CourierHomeItemBuilder extends StatelessWidget {
  final CourierHomeItemModel itemModel;

  const CourierHomeItemBuilder({
    Key? key,
    required this.itemModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        itemModel.routeName,
      ),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: courierGreyLightColor,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              itemModel.title,
              textAlign: TextAlign.center,
              style: h4.copyWith(
                color: courierSecondaryColor,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 2),
            Text(
              '${itemModel.totalCount}',
              textAlign: TextAlign.center,
              style: h1.copyWith(
                color: courierSecondaryColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
