class CourierHomeItemModel {
  final String routeName;
  final String title;
  final int totalCount;

  CourierHomeItemModel({
    required this.routeName,
    required this.title,
    required this.totalCount,
  });
}
