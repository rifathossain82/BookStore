import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/courier_bottom_navigation.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_info_widget.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_list_order_info_widget.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_list_order_status_widget.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/payment_info_widget.dart';
import 'package:flutter/material.dart';
import 'package:slide_to_act/slide_to_act.dart';

class DeliveryListPage extends StatelessWidget {
  const DeliveryListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        backgroundColor: courierGreyLightColor,
        title: const Text('View Delivery List'),
      ),
      bottomNavigationBar: const CourierBottomNavigation(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DeliveryListOrderStatusWidget(
              orderStatus: 'Pending to Delivered',
            ),
            DeliveryInfoWidget(),
            DeliveryListOrderInfoWidget(),
            PaymentInfoWidget(),
            Padding(
              padding: const EdgeInsets.all(15),
              child: SlideAction(
                onSubmit: () {},
                elevation: 1,
                innerColor: courierMainColor,
                outerColor: courierVioletShadowColor,
                alignment: Alignment.center,
                text: 'Swipe to Confirm for Delivered',
                textStyle: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: courierMainColor,
                ),
                sliderButtonIcon: Icon(
                  Icons.send_rounded,
                  color: kWhite,
                ),
                height: 50,
                sliderButtonIconPadding: 15,
                sliderButtonYOffset: -10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
