import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_list_row_text.dart';
import 'package:flutter/material.dart';

class DeliveryListOrderInfoWidget extends StatelessWidget {
  const DeliveryListOrderInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Order Info',
              textAlign: TextAlign.start,
              style: h4.copyWith(
                fontWeight: FontWeight.bold,
                color: courierMainColor,
              ),
            ),
          ),
          DeliveryListRowTextBuilder(
            title: 'Order ID:',
            value: 'OR-001',
            paddingTop: 10,
          ),
          DeliveryListRowTextBuilder(
            title: 'Order Date:',
            value: '29 Jan 2023, 10:25 AM',
          ),
          DeliveryListRowTextBuilder(
            title: 'Collection Point:',
            value: 'Cash on Delivery',
          ),
          const SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Books',
                style: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kBlackLight.withOpacity(0.8),
                ),
              ),
              Container(
                height: 30,
                width: 80,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: courierGreenShadowColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  '30',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: courierMainColor,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
