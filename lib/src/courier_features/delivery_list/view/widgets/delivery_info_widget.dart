import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_book_details_item_builder.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_list_row_text.dart';
import 'package:flutter/material.dart';

class DeliveryInfoWidget extends StatelessWidget {
  const DeliveryInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [

          /// delivery info title
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 4,
            ),
            decoration: BoxDecoration(
              color: courierVioletShadowColor,
              borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Delivery Info',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: courierMainColor,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: courierMainColor.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Icon(
                    Icons.phone,
                    color: courierMainColor,
                  ),
                ),
              ],
            ),
          ),

          /// delivery info data
          DeliveryListRowTextBuilder(
            title: 'Customer Name:',
            value: 'Nijum Sultana',
            paddingRight: 15,
            paddingLeft: 15,
            paddingTop: 10,
          ),
          DeliveryListRowTextBuilder(
            title: 'Phone Number:',
            value: '01885256220',
            paddingRight: 15,
            paddingLeft: 15,
            paddingTop: 10,
          ),
          DeliveryListRowTextBuilder(
            title: 'Location:',
            value: 'Sector 14, Uttara, Dhaka, Bangladesh.',
            paddingRight: 15,
            paddingLeft: 15,
            paddingTop: 10,
          ),
        ],
      ),
    );
  }
}
