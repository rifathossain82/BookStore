import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class DeliveryListOrderStatusWidget extends StatelessWidget {
  final String orderStatus;

  const DeliveryListOrderStatusWidget({
    Key? key,
    required this.orderStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: courierVioletShadowColor,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: kGreyMedium,
          width: 0.2,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Order Status:',
            style: h4.copyWith(
              fontWeight: FontWeight.bold,
              color: courierMainColor,
            ),
          ),
          Text(
            orderStatus,
            style: h4.copyWith(
              fontWeight: FontWeight.bold,
              color: courierMainColor,
            ),
          ),
        ],
      ),
    );
  }
}
