import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/courier_features/delivery_list/view/widgets/delivery_list_row_text.dart';
import 'package:flutter/material.dart';

class PaymentInfoWidget extends StatelessWidget {
  const PaymentInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [

          /// payment info title
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: courierVioletShadowColor,
              borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8),
              ),
            ),
            child: Text(
              'Payment Info',
              style: h4.copyWith(
                fontWeight: FontWeight.bold,
                color: courierMainColor,
              ),
            ),
          ),

          /// payment info data
          DeliveryListRowTextBuilder(
            title: 'Price:',
            value: '550 Tk',
            paddingRight: 15,
            paddingLeft: 15,
            paddingTop: 10,
          ),
          DeliveryListRowTextBuilder(
            title: 'Delivery Charge:',
            value: '50 Tk',
            paddingRight: 15,
            paddingLeft: 15,
            paddingTop: 10,
            paddingBottom: 20,
          ),

          /// grand total
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
              color: courierVioletShadowColor,
              borderRadius: const BorderRadius.vertical(
                bottom: Radius.circular(8),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Grand Total',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: courierSecondaryColor,
                  ),
                ),
                Text(
                  '550 Tk',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: courierSecondaryColor,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
