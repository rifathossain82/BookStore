import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_outlined_button.dart';
import 'package:flutter/material.dart';

class CollectionRequestItemBuilder extends StatelessWidget {
  final FakeCourierOrderData fakeCourierOrderData;

  const CollectionRequestItemBuilder({
    Key? key,
    required this.fakeCourierOrderData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            fakeCourierOrderData.id,
            textAlign: TextAlign.center,
            style: h4,
          ),
        ),
        Expanded(
          child: Text(
            '${fakeCourierOrderData.totalQuantity}',
            textAlign: TextAlign.center,
            style: h4,
          ),
        ),
        Expanded(
          child: KOutlinedButton(
            onPressed: () => Navigator.pushNamed(
              context,
              RouteGenerator.collectionRequestDetails,
              arguments: fakeCourierOrderData,
            ),
            borderColor: courierMainColor,
            bgColor: courierMainColor.withOpacity(0.05),
            child: Text(
              'View Details',
              style: h5.copyWith(
                fontWeight: FontWeight.bold,
                color: courierMainColor,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
