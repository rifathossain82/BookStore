import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_item_builder.dart';
import 'package:flutter/material.dart';

class CollectionRequestListCardWidget extends StatelessWidget {
  const CollectionRequestListCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 10,
      ),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 6,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  'Order Id',
                  textAlign: TextAlign.center,
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  'Qty',
                  textAlign: TextAlign.center,
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: Text(
                  'Action',
                  textAlign: TextAlign.center,
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          const KDivider(),
          Expanded(
            child: ListView.separated(
              itemCount: fakeCourierOrderList.length,
              itemBuilder: (context, index) => CollectionRequestItemBuilder(
                fakeCourierOrderData: fakeCourierOrderList[index],
              ),
              separatorBuilder: (context, index) => const KDivider(),
            ),
          ),
        ],
      ),
    );
  }
}
