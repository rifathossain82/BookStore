import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CollectionRequestBookDetailsItemBuilder extends StatelessWidget {
  final CourierBookData bookData;

  const CollectionRequestBookDetailsItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 15,
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(
              bookData.sl,
              textAlign: TextAlign.start,
              style: h4,
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              bookData.name,
              textAlign: TextAlign.start,
              style: h4,
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              height: 35,
              width: 80,
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
              ),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                color: kGreyMedium.withOpacity(0.4),
                borderRadius: BorderRadius.circular(5),
                border: Border.all(
                  color: kGreyMedium,
                  width: 1,
                ),
              ),
              child: Text(
                '${bookData.receivedQty}',
                style: h4,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
