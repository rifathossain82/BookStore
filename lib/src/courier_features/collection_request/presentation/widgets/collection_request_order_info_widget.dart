import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_row_text.dart';
import 'package:flutter/material.dart';

class CollectionRequestOrderInfoWidget extends StatelessWidget {
  final FakeCourierOrderData orderData;

  const CollectionRequestOrderInfoWidget({
    Key? key,
    required this.orderData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Order Info',
              textAlign: TextAlign.start,
              style: h4.copyWith(
                fontWeight: FontWeight.bold,
                color: courierMainColor,
              ),
            ),
          ),
          CollectionRequestRowTextBuilder(
            title: 'Order ID:',
            value: orderData.id,
          ),
          CollectionRequestRowTextBuilder(
            title: 'Order Date:',
            value: orderData.date,
          ),
          CollectionRequestRowTextBuilder(
            title: 'Collection Point:',
            value: orderData.collectionPoint,
          ),
          const SizedBox(height: 5),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Total Quantity',
                style: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kBlackLight.withOpacity(0.8),
                ),
              ),
              Container(
                height: 30,
                width: 80,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: courierGreenShadowColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  '${orderData.totalQuantity}',
                  style: h4.copyWith(
                    fontWeight: FontWeight.bold,
                    color: courierMainColor,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
