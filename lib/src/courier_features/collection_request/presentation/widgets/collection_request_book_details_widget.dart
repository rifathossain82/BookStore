import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_book_details_item_builder.dart';
import 'package:flutter/material.dart';

class CollectionRequestBookDetailsWidget extends StatelessWidget {
  final List<CourierBookData> bookList;

  const CollectionRequestBookDetailsWidget({
    Key? key,
    required this.bookList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 8,
      ),
      decoration: BoxDecoration(
        color: kWhite,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(15),
            width: context.screenWidth,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: courierVioletShadowColor.withOpacity(0.5),
              borderRadius: const BorderRadius.vertical(
                top: Radius.circular(8),
              ),
            ),
            child: Text(
              'Book Details',
              style: h4.copyWith(
                fontWeight: FontWeight.bold,
                color: courierMainColor,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(15),
            width: context.screenWidth,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: courierVioletShadowColor,
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    'SL',
                    textAlign: TextAlign.start,
                    style: h4,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    'Book Name',
                    textAlign: TextAlign.start,
                    style: h4,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    'Received Qty',
                    textAlign: TextAlign.start,
                    style: h4,
                  ),
                ),
              ],
            ),
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: bookList.length,
            itemBuilder: (context, index) =>
                CollectionRequestBookDetailsItemBuilder(
              bookData: bookList[index],
            ),
            separatorBuilder: (context, index) => const KDivider(),
          ),
        ],
      ),
    );
  }
}
