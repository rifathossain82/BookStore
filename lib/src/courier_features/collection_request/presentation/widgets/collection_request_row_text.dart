import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CollectionRequestRowTextBuilder extends StatelessWidget {
  final String title;
  final String value;
  final TextStyle? titleStyle;
  final TextStyle? valueStyle;
  final MainAxisAlignment mainAxisAlignment;
  final double paddingTop;
  final double paddingBottom;
  final double paddingLeft;
  final double paddingRight;

  const CollectionRequestRowTextBuilder({
    Key? key,
    required this.title,
    required this.value,
    this.titleStyle,
    this.valueStyle,
    this.mainAxisAlignment = MainAxisAlignment.spaceBetween,
    this.paddingTop = 5,
    this.paddingBottom = 5,
    this.paddingLeft = 0,
    this.paddingRight = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: paddingTop,
        bottom: paddingBottom,
        left: paddingLeft,
        right: paddingRight,
      ),
      child: Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          Expanded(
            child: Text(
              title,
              style: titleStyle ?? h5.copyWith(
                fontWeight: FontWeight.bold,
                color: kBlackLight.withOpacity(0.8),
              ),
            ),
          ),
          const SizedBox(width: 15),
          Expanded(
            child: Text(
              value,
              textAlign: TextAlign.end,
              style: valueStyle ?? h5.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
