import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class CollectionRequestDateSelectorBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final String title;

  const CollectionRequestDateSelectorBuilder({
    Key? key,
    required this.onTap,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 5,
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: kWhite,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                title,
                style: h5.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const Icon(
              Icons.calendar_month,
              size: 20,
            ),
          ],
        ),
      ),
    );
  }
}
