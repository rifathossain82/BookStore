import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/courier_bottom_navigation.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_book_details_widget.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_order_info_widget.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_order_status_widget.dart';
import 'package:flutter/material.dart';
import 'package:slide_to_act/slide_to_act.dart';

class CollectionRequestDetailsPage extends StatelessWidget {
  const CollectionRequestDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FakeCourierOrderData fakeCourierOrderData = context.getArguments;
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        backgroundColor: courierGreyLightColor,
        title: const Text('View Collection Request'),
      ),
      bottomNavigationBar: const CourierBottomNavigation(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CollectionRequestOrderStatusWidget(
              orderStatus: fakeCourierOrderData.status,
            ),
            CollectionRequestOrderInfoWidget(
              orderData: fakeCourierOrderData,
            ),
            CollectionRequestBookDetailsWidget(
              bookList: fakeCourierOrderData.books,
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: SlideAction(
                onSubmit: () {},
                elevation: 1,
                innerColor: courierMainColor,
                outerColor: courierVioletShadowColor,
                alignment: Alignment.center,
                text: 'Swipe to Confirm for Received',
                textStyle: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: courierMainColor,
                ),
                sliderButtonIcon: Icon(
                  Icons.send_rounded,
                  color: kWhite,
                ),
                height: 50,
                sliderButtonIconPadding: 15,
                sliderButtonYOffset: -10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
