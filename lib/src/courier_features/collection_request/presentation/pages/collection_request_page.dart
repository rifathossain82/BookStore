import 'package:book_store/src/core/extensions/build_context_extension.dart';
import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/appbar_leading_icon.dart';
import 'package:book_store/src/core/widgets/courier_bottom_navigation.dart';
import 'package:book_store/src/core/widgets/k_button.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collection_request_list_card_widget.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/widgets/collenction_request_date_selector_builder.dart';
import 'package:flutter/material.dart'; 
import 'package:intl/intl.dart';

class CollectionRequestPage extends StatefulWidget {
  const CollectionRequestPage({Key? key}) : super(key: key);

  @override
  State<CollectionRequestPage> createState() => _CollectionRequestPageState();
}

class _CollectionRequestPageState extends State<CollectionRequestPage> {
  bool isSearching = false;
  final searchTextController = TextEditingController();
  DateTime? startDate;
  DateTime? endDate;

  Future<void> _selectStartDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: startDate ?? DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2101));
    if (picked != null && picked != startDate) {
      setState(() {
        startDate = picked;
      });
    }
  }

  Future<void> _selectEndDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: endDate ?? DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2101));
    if (picked != null && picked != endDate) {
      setState(() {
        endDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kGreyLight,
      appBar: AppBar(
        leading: const AppBarLeadingIcon(),
        backgroundColor: courierGreyLightColor,
        title: isSearching ? _buildSearchTextField() : _buildAppbarTitleText(),
        actions: <Widget>[
          IconButton(
            icon: Icon(isSearching ? Icons.close : Icons.search),
            onPressed: () {
              setState(() {
                isSearching = !isSearching;
              });
            },
          ),
        ],
      ),
      bottomNavigationBar: const CourierBottomNavigation(),
      body: Column(
        children: [
          _buildSearchByDateWidget(),
          const SizedBox(height: 10),
          const Expanded(
            child: CollectionRequestListCardWidget(),
          ),
        ],
      ),
    );
  }

  Widget _buildSearchTextField() {
    return TextField(
      controller: searchTextController,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        hintText: 'Search by Order No.',
      ),
      autofocus: true,
    );
  }

  Widget _buildAppbarTitleText() {
    return const Text(
      'Collection Request List',
    );
  }

  Widget _buildSearchByDateWidget() {
    return Container(
      height: 60,
      width: context.screenWidth,
      padding: const EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 12,
      ),
      decoration: BoxDecoration(
        color: courierGreenShadowColor,
        borderRadius: const BorderRadius.vertical(
          bottom: Radius.circular(20),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: CollectionRequestDateSelectorBuilder(
              onTap: () => _selectStartDate(context),
              title: startDate == null
                  ? 'Start Date'
                  : DateFormat('dd-MM-yyyy').format(startDate!),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: 4,
            child: CollectionRequestDateSelectorBuilder(
              onTap: () => _selectEndDate(context),
              title: endDate == null
                  ? 'End Date'
                  : DateFormat('dd-MM-yyyy').format(endDate!),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            flex: 3,
            child: KButton(
              onPressed: searchByDateMethod,
              child: Text(
                'Search',
                style: h5.copyWith(
                  fontWeight: FontWeight.bold,
                  color: kWhite,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void searchByDateMethod(){
    if (startDate == null) {
      kSnackBar(
        context: context,
        message: 'Select Start Date!',
        bgColor: failedColor,
      );
    } else if(endDate == null){
      kSnackBar(
        context: context,
        message: 'Select End Date!',
        bgColor: failedColor,
      );
    } else{
      ///TODO: Your Methods
    }
  }
}
