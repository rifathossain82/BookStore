import 'package:book_store/src/core/helpers/helper_methods.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';

class LauncherServices {
  static void makeCall(BuildContext context, String number) async {
    var url = Uri.parse('tel: $number');

    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      kSnackBar(
        context: context,
        message: 'Could not make call!',
        bgColor: failedColor,
      );
    }
  }

  static void openURL(BuildContext context, String url) async {
    var uri = Uri.parse(url);

    if (await canLaunchUrl(uri)) {
      await launchUrl(
        uri,
        mode: LaunchMode.inAppWebView,
        webViewConfiguration: const WebViewConfiguration(
          enableJavaScript: true,
          enableDomStorage: true,
        ),
      );
    } else {
      kSnackBar(
        context: context,
        message: 'Could not launch url!',
        bgColor: failedColor,
      );
    }
  }

  static Future<void> openMap(
      BuildContext context, double latitude, double longitude) async {
    final Uri googleUrl =
        Uri.parse("google.navigation:q=$latitude,$longitude&mode=d");
    if (await canLaunchUrl(googleUrl)) {
      await launchUrl(googleUrl);
    } else {
      kSnackBar(
        context: context,
        message: 'Could not open the map!',
        bgColor: failedColor,
      );
    }
  }
}
