import 'package:book_store/src/courier_features/collection_request/presentation/pages/collection_request_details_page.dart';
import 'package:book_store/src/courier_features/collection_request/presentation/pages/collection_request_page.dart';
import 'package:book_store/src/courier_features/dashboard/presentation/pages/courier_dashboard_page.dart';
import 'package:book_store/src/courier_features/delivery_list/view/pages/delivery_list_page.dart';
import 'package:book_store/src/courier_features/home/presentation/pages/courier_home_page.dart';
import 'package:flutter/material.dart';
import 'package:book_store/src/customer_features/about_us/presentation/pages/about_us_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/forgot_password_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/otp_verification_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/reset_password_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/sign_in_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/sign_up_page.dart';
import 'package:book_store/src/customer_features/auth/presentation/pages/verify_account_page.dart';
import 'package:book_store/src/customer_features/author/presentation/pages/autho_book_list_page.dart';
import 'package:book_store/src/customer_features/author/presentation/pages/author_page.dart';
import 'package:book_store/src/customer_features/cart/presentation/pages/cart_page.dart';
import 'package:book_store/src/customer_features/category/presentation/pages/category_page.dart';
import 'package:book_store/src/customer_features/category/presentation/pages/category_wise_book_list_page.dart';
import 'package:book_store/src/customer_features/checkout/presentation/pages/add_new_address_page.dart';
import 'package:book_store/src/customer_features/checkout/presentation/pages/checkout_confirm_order_page.dart';
import 'package:book_store/src/customer_features/checkout/presentation/pages/checkout_payment_method_page.dart';
import 'package:book_store/src/customer_features/checkout/presentation/pages/checkout_payment_summary_page.dart';
import 'package:book_store/src/customer_features/contact_us/presentation/pages/contact_us_page.dart';
import 'package:book_store/src/customer_features/dashboard/presentation/pages/dashboard_page.dart';
import 'package:book_store/src/customer_features/home/presentation/pages/book_details_page.dart';
import 'package:book_store/src/customer_features/home/presentation/pages/book_pdf_view_page.dart';
import 'package:book_store/src/customer_features/home/presentation/pages/home_page.dart';
import 'package:book_store/src/customer_features/notifications/presentation/pages/notifications_page.dart';
import 'package:book_store/src/customer_features/orders/presentation/pages/my_orders_page.dart';
import 'package:book_store/src/customer_features/orders/presentation/pages/order_details_page.dart';
import 'package:book_store/src/customer_features/orders/presentation/pages/order_track_form_page.dart';
import 'package:book_store/src/customer_features/orders/presentation/pages/order_track_page.dart';
import 'package:book_store/src/customer_features/profile/presentation/pages/profile_page.dart';
import 'package:book_store/src/customer_features/publisher/presentation/pages/publisher_book_list_page.dart';
import 'package:book_store/src/customer_features/publisher/presentation/pages/publisher_page.dart';
import 'package:book_store/src/customer_features/ratings_reviews/presentation/pages/ratings_and_reviews_page.dart';
import 'package:book_store/src/customer_features/return_policy/presentation/pages/return_policy_page.dart';
import 'package:book_store/src/customer_features/search/presentation/pages/not_found_search_book_request_page.dart';
import 'package:book_store/src/customer_features/search/presentation/pages/search_page.dart';
import 'package:book_store/src/customer_features/splash/presentation/pages/splash_page.dart';
import 'package:book_store/src/customer_features/wishlist/presentation/pages/wishlist_page.dart';

class RouteGenerator {
  static const String splash = '/';
  static const String signIn = '/signIn';
  static const String signUp = '/signUp';
  static const String verifyAccount = '/verifyAccount';
  static const String forgotPassword = '/forgotPassword';
  static const String otpVerification = '/otpVerification';
  static const String resetPassword = '/resetPassword';
  static const String dashboard = '/dashboard';
  static const String homepage = '/homepage';
  static const String bookDetails = '/bookDetails';
  static const String pdfView = '/pdfView';
  static const String category = '/category';
  static const String categoryWiseBookList = '/categoryWiseBookList';
  static const String profile = '/profile';
  static const String myOrders = '/myOrders';
  static const String orderDetails = '/orderDetails';
  static const String orderTrack = '/orderTrack';
  static const String orderTrackForm = '/orderTrackForm';
  static const String wishlist = '/wishlist';
  static const String ratingsAndReviews = '/ratingsAndReviews';
  static const String cart = '/cart';
  static const String checkoutPaymentSummary = '/checkoutPaymentSummary';
  static const String addNewAddress = '/addNewAddress';
  static const String checkoutPaymentMethod = '/checkoutPaymentMethod';
  static const String checkoutConfirmOrder = '/checkoutConfirmOrder';
  static const String notifications = '/notifications';
  static const String searchPage = '/searchPage';
  static const String notFoundSearchBookRequest = '/notFoundSearchBookRequest';
  static const String aboutUs = '/aboutUs';
  static const String contactUs = '/contactUs';
  static const String returnPolicy = '/returnPolicy';
  static const String authorPage = '/authorPage';
  static const String authorBookListPage = '/authorBookListPage';
  static const String publisherPage = '/publisherPage';
  static const String publisherBookListPage = '/publisherBookListPage';

  /// courier features
  static const String courierDashboard = '/courierDashboard';
  static const String courierHomepage = '/courierHomepage';
  static const String collectionRequest = '/collectionRequest';
  static const String collectionRequestDetails = '/collectionRequestDetails';
  static const String deliveryListPage = '/deliveryListPage';

  RouteGenerator._();

  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case splash:
        return MaterialPageRoute(
          builder: (_) => const SplashPage(),
          settings: routeSettings,
        );
      case signIn:
        return MaterialPageRoute(
          builder: (_) => const SignInPage(),
          settings: routeSettings,
        );
      case signUp:
        return MaterialPageRoute(
          builder: (_) => const SignUpPage(),
          settings: routeSettings,
        );
      case verifyAccount:
        return MaterialPageRoute(
          builder: (_) => const VerifyAccountPage(),
          settings: routeSettings,
        );
      case forgotPassword:
        return MaterialPageRoute(
          builder: (_) => const ForgotPasswordPage(),
          settings: routeSettings,
        );
      case otpVerification:
        return MaterialPageRoute(
          builder: (_) => const OTPVerificationPage(),
          settings: routeSettings,
        );
      case resetPassword:
        return MaterialPageRoute(
          builder: (_) => const ResetPasswordPage(),
          settings: routeSettings,
        );
      case dashboard:
        return MaterialPageRoute(
          builder: (_) => const DashboardPage(),
          settings: routeSettings,
        );
      case homepage:
        return MaterialPageRoute(
          builder: (_) => const Homepage(),
          settings: routeSettings,
        );
      case bookDetails:
        return MaterialPageRoute(
          builder: (_) => const BookDetailsPage(),
          settings: routeSettings,
        );
      case pdfView:
        return MaterialPageRoute(
          builder: (_) => const BookPdfViewPage(),
          settings: routeSettings,
        );
      case category:
        return MaterialPageRoute(
          builder: (_) => const CategoryPage(),
          settings: routeSettings,
        );
      case categoryWiseBookList:
        return MaterialPageRoute(
          builder: (_) => const CategoryWiseBookListPage(),
          settings: routeSettings,
        );
      case profile:
        return MaterialPageRoute(
          builder: (_) => const ProfilePage(),
          settings: routeSettings,
        );
      case myOrders:
        return MaterialPageRoute(
          builder: (_) => const MyOrdersPage(),
          settings: routeSettings,
        );
      case orderDetails:
        return MaterialPageRoute(
          builder: (_) => const OrderDetailsPage(),
          settings: routeSettings,
        );
      case orderTrack:
        return MaterialPageRoute(
          builder: (_) => const OrderTrackPage(),
          settings: routeSettings,
        );
      case orderTrackForm:
        return MaterialPageRoute(
          builder: (_) => const OrderTrackFormPage(),
          settings: routeSettings,
        );
      case wishlist:
        return MaterialPageRoute(
          builder: (_) => WishlistPage(),
          settings: routeSettings,
        );
      case ratingsAndReviews:
        return MaterialPageRoute(
          builder: (_) => const RatingsAndReviewsPage(),
          settings: routeSettings,
        );
      case cart:
        return MaterialPageRoute(
          builder: (_) => const CartPage(),
          settings: routeSettings,
        );
      case checkoutPaymentSummary:
        return MaterialPageRoute(
          builder: (_) => const CheckoutPaymentSummaryPage(),
          settings: routeSettings,
        );
      case addNewAddress:
        return MaterialPageRoute(
          builder: (_) => const AddNewAddressPage(),
          settings: routeSettings,
        );
      case checkoutPaymentMethod:
        return MaterialPageRoute(
          builder: (_) => const CheckoutPaymentMethodPage(),
          settings: routeSettings,
        );
      case checkoutConfirmOrder:
        return MaterialPageRoute(
          builder: (_) => const CheckoutConfirmOrderPage(),
          settings: routeSettings,
        );
      case notifications:
        return MaterialPageRoute(
          builder: (_) => const NotificationsPage(),
          settings: routeSettings,
        );
      case searchPage:
        return MaterialPageRoute(
          builder: (_) => const SearchPage(),
          settings: routeSettings,
        );
      case notFoundSearchBookRequest:
        return MaterialPageRoute(
          builder: (_) => const NotFoundSearchBookRequestPage(),
          settings: routeSettings,
        );
      case aboutUs:
        return MaterialPageRoute(
          builder: (_) => const AboutUsPage(),
          settings: routeSettings,
        );
      case contactUs:
        return MaterialPageRoute(
          builder: (_) => ContactUsPage(),
          settings: routeSettings,
        );
      case returnPolicy:
        return MaterialPageRoute(
          builder: (_) => const ReturnPolicyPage(),
          settings: routeSettings,
        );
      case authorPage:
        return MaterialPageRoute(
          builder: (_) => const AuthorPage(),
          settings: routeSettings,
        );
      case authorBookListPage:
        return MaterialPageRoute(
          builder: (_) => const AuthorBookListPage(),
          settings: routeSettings,
        );
      case publisherPage:
        return MaterialPageRoute(
          builder: (_) => const PublisherPage(),
          settings: routeSettings,
        );
      case publisherBookListPage:
        return MaterialPageRoute(
          builder: (_) => const PublisherBookListPage(),
          settings: routeSettings,
        );

      /// courier features
      case courierDashboard:
        return MaterialPageRoute(
          builder: (_) => const CourierDashboardPage(),
          settings: routeSettings,
        );
      case courierHomepage:
        return MaterialPageRoute(
          builder: (_) => CourierHomepage(),
          settings: routeSettings,
        );
      case collectionRequest:
        return MaterialPageRoute(
          builder: (_) => const CollectionRequestPage(),
          settings: routeSettings,
        );
      case collectionRequestDetails:
        return MaterialPageRoute(
          builder: (_) => const CollectionRequestDetailsPage(),
          settings: routeSettings,
        );
      case deliveryListPage:
        return MaterialPageRoute(
          builder: (_) => const DeliveryListPage(),
          settings: routeSettings,
        );


        /// default
      default:
        return MaterialPageRoute(
          builder: (_) => const SplashPage(),
          settings: routeSettings,
        );
    }
  }
}
