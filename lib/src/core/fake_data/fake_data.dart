import 'package:book_store/src/core/enums/app_enum.dart';

class FakeBookData {
  final String? name;
  final String? author;
  final num? regularPrice;
  final num? offerPrice;
  final bool? inStock;
  final double? ratings;
  final int? totalRatings;
  final int? totalReviews;
  final String? category;
  final String? type;
  final List<String>? specialOffers;
  final String? summary;
  final String? language;
  final String? languageSign;
  final int? totalPage;
  final String? releaseDate;
  final String? releaseNo;
  final String? publisherName;
  final String? publisherImage;
  final String? image;

  FakeBookData({
    this.name,
    this.author,
    this.regularPrice,
    this.offerPrice,
    this.inStock,
    this.ratings,
    this.totalRatings,
    this.totalReviews,
    this.category,
    this.type,
    this.specialOffers,
    this.summary,
    this.language,
    this.languageSign,
    this.totalPage,
    this.releaseDate,
    this.releaseNo,
    this.publisherName,
    this.publisherImage,
    this.image,
  });
}

class Highlights {
  final String title;
  final String value;

  Highlights({required this.title, required this.value});
}

class Specifications {
  final String title;
  final String value;

  Specifications({required this.title, required this.value});
}

var fakeBookList = [
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 3.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    summary: '''
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।''',
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 4.5,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    summary: '''
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।''',
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    summary: '''
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।
    মননের মিলন-মেলা বইমেলা। যার জন্য গোটা বছর অপেক্ষমান থাকেন পুস্তকপ্রেমীরা। বইয়ের ভূবনে হারিয়ে যাওয়ার প্রবল আকুতিতে অধীর আগ্রহে দিন গুনেন সবাই। এবার সেই অপেক্ষার পালা শেষ। ২৪ মার্চ থেকে এই প্রাণের মেলায় থাকবে আলোচনা, কবি সম্মেলন, ক্যুইজ, সাংস্কৃতিক অনুষ্ঠান আরও কতকিছু।''',
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
  FakeBookData(
    name: 'কোরআন ও হাদিস থেকে সংগৃহীত যিকির ও দোয়া',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Philosophyr_Boyos_Koto-Abdullah_Al_Mamun_Kaikor-6cf9d-271152.png',
  ),
  FakeBookData(
    name: 'মনা ও মেছুভূত',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/Mujibnagor_Sarkar_O_Bortoman_Bangladesh-Akbar_Ali_Khan_-c78ba-273053.jpg',
  ),
  FakeBookData(
    name: 'না বলতে শিখুন',
    author: 'সব্যসাচী মিস্ত্রী',
    regularPrice: 80,
    offerPrice: 60,
    inStock: true,
    ratings: 0.0,
    totalRatings: 4,
    totalReviews: 5,
    category: 'বয়স যখন ৪-৮ঃ কমিক্স ও ছবির গল্প',
    type: 'পেপারব্যাক',
    specialOffers: [
      'আজ নিশ্চিন্ত ফ্রি শিপিং ৬৯৯৳+ অর্ডারে',
      'নগদে পেমেন্টে ২১% ইন্সট্যান্ট ক্যাশব্যাক (সর্বোচ্চ ১২৫৳)',
    ],
    language: 'Bnagla',
    languageSign: 'BN',
    totalPage: 195,
    releaseDate: '2021',
    releaseNo: '1st',
    image:
        'https://ds.rokomari.store/rokomari110/ProductNew20190903/260X372/She_ese_bosuk_pashe-Sadat_Hossain_-0be5e-280288.jpeg',
  ),
];

class FakeCountryInfo {
  final String flagUrl;
  final String name;
  final String code;
  final int maxPhoneNoLength;

  FakeCountryInfo({
    required this.flagUrl,
    required this.name,
    required this.code,
    required this.maxPhoneNoLength,
  });
}

final fakeCountryList = [
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/3371/3371885.png",
    name: 'Bangladesh',
    code: '+880',
    maxPhoneNoLength: 10,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/128/206/206606.png",
    name: 'India',
    code: '+91',
    maxPhoneNoLength: 10,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/206/206741.png",
    name: 'Afganistan',
    code: '+93',
    maxPhoneNoLength: 9,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/3371/3371885.png",
    name: 'Bangladesh',
    code: '+880',
    maxPhoneNoLength: 11,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/128/206/206606.png",
    name: 'India',
    code: '+91',
    maxPhoneNoLength: 10,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/206/206741.png",
    name: 'Afganistan',
    code: '+93',
    maxPhoneNoLength: 9,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/3371/3371885.png",
    name: 'Bangladesh',
    code: '+880',
    maxPhoneNoLength: 11,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/128/206/206606.png",
    name: 'India',
    code: '+91',
    maxPhoneNoLength: 10,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/206/206741.png",
    name: 'Afganistan',
    code: '+93',
    maxPhoneNoLength: 9,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/3371/3371885.png",
    name: 'Bangladesh',
    code: '+880',
    maxPhoneNoLength: 11,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/128/206/206606.png",
    name: 'India',
    code: '+91',
    maxPhoneNoLength: 10,
  ),
  FakeCountryInfo(
    flagUrl: "https://cdn-icons-png.flaticon.com/512/206/206741.png",
    name: 'Afganistan',
    code: '+93',
    maxPhoneNoLength: 9,
  ),
];

class FakeOrderData {
  final String id;
  final String status;
  final List<FakeBookData> books;

  FakeOrderData({
    required this.id,
    required this.status,
    required this.books,
  });
}

final fakeOrderList = [
  FakeOrderData(
    id: '65467465465465',
    status: 'Cancelled',
    books: [
      fakeBookList[0],
      fakeBookList[1],
      fakeBookList[2],
    ],
  ),
  FakeOrderData(
    id: '5478454664',
    status: 'Processing',
    books: [
      fakeBookList[0],
      fakeBookList[1],
      fakeBookList[2],
    ],
  ),
];

class FakeCheckoutData {
  final List<FakeBookData> items;
  final num shippingCharge;
  final num subtotal;
  final num total;
  final num payableTotal;

  FakeCheckoutData({
    required this.items,
    required this.shippingCharge,
    required this.subtotal,
    required this.total,
    required this.payableTotal,
  });
}

class FakeAddressData {
  final String name;
  final String phone;
  final AddressType addressType;
  final String country;
  final String city;
  final String area;
  final String address;

  FakeAddressData({
    required this.name,
    required this.phone,
    required this.addressType,
    required this.country,
    required this.city,
    required this.area,
    required this.address,
  });
}

final fakeAddressList = [];

final fakeCountryNameList = [
  'Bangladesh',
  'India',
  'Pakistan',
  'Nepal',
  'Bhutan',
  'England',
  'United States',
];

final fakeCityNameList = [
  'Dhaka',
  'Chittagong',
  'Cumilla',
  'Barisal',
  'Rangpur',
  'Rajshahi',
];

final fakeAreaNameList = [
  'Uttara',
  'Mirpur',
  'Rajarbag',
  'Gulistan',
  'Sadar Ghat',
];

class FakeNotificationData {
  final String date;
  final String notificationMsg;
  final String url;

  FakeNotificationData({
    required this.date,
    required this.notificationMsg,
    required this.url,
  });
}

final List<FakeNotificationData> fakeNotificationList = [
  FakeNotificationData(
    date: '20/03/2023',
    notificationMsg: 'যেভাবে মেডিকেলে প্রথম হলেন রাফসান জামান। দেখুন-',
    url: 'https://www.youtube.com/watch?v=GG4jK6G4K0E',
  ),
  FakeNotificationData(
    date: '17/03/2023',
    notificationMsg:
        'জনপ্রিয় কথাসাহিত্যিক মোশতাক আহমেদের যে কোন ৩ টি বই কিনলেই পাচ্ছেন ফ্রিশিপিং। প্রিয় লেখকের যে কোন বইয়ে পাবেন ২৫ শতাংশ ছাড়,',
    url: 'https://www.rokomari.com/book/author/5659/mostaque-ahamed',
  ),
  FakeNotificationData(
    date: '20/03/2023',
    notificationMsg: 'ট্র্যাজেডি কী, জানুন কোচ কাঞ্চনের কাছ থেকে',
    url: 'https://www.youtube.com/watch?v=OwBIDAeE8ds',
  ),
];

final fakeReturnPolicyHTMLData = '''
  <p>We want you to be completely satisfied with your purchase. If for any reason you are not happy with your order, we offer a full refund within 30 days of delivery.</p>

  <h2>Conditions for Return</h2>

  <p>To be eligible for a return, your item must be:</p>

  <ul>
    <li>In new and unused condition</li>
    <li>In the original packaging</li>
    <li>Accompanied by a receipt or proof of purchase</li>
    <li>Returned within 30 days of delivery</li>
  </ul>

  <p>Please note that we cannot accept returns for:</p>

  <ul>
    <li>Gift cards</li>
    <li>Downloadable software products</li>
    <li>Health and personal care items</li>
    <li>Intimate or sanitary goods</li>
    <li>Hazardous materials</li>
    <li>Flammable liquids or gases</li>
    <li>Perishable goods</li>
  </ul>

  <h2>How to Initiate a Return</h2>

  <p>To initiate a return, please follow these steps:</p>

  <ol>
    <li>Contact our customer support team to obtain a Return Merchandise Authorization (RMA) number</li>
    <li>Package the item securely, including all original materials and documentation, and include the RMA number on the outside of the package</li>
    <li>Ship the package to the address provided by our customer support team</li>
  </ol>

  <p>Please note that you are responsible for the cost of return shipping unless the item is defective or damaged. We recommend using a trackable shipping service and purchasing shipping insurance, as we cannot guarantee that we will receive your returned item.</p>

  <h2>Refunds</h2>

  <p>Once your return is received and inspected, we will send you an email to notify you that we have received your returned item. We will also notify you of the approval or rejection of your refund.</p>

  <p>If your return is approved, then your refund will be processed, and a credit will automatically be applied to your original method of payment, within a certain amount of days.</p>

  <p>Please note that we cannot refund shipping costs or customs duties and taxes, as these fees are paid directly to third-party carriers and government agencies.</p>
''';

class FakeAuthorData {
  final String image;
  final String name;

  FakeAuthorData({
    required this.image,
    required this.name,
  });
}

final fakeAuthorList = [
  FakeAuthorData(
    image:
        'https://cdn.hswstatic.com/gif/play/0b7f4e9b-f59c-4024-9f06-b3dc12850ab7-1920-1080.jpg',
    name: 'থিবো মেরিস',
  ),
  FakeAuthorData(
    image:
        'https://images.healthshots.com/healthshots/en/uploads/2020/12/08182549/positive-person.jpg',
    name: 'Jobayer Hossain',
  ),
  FakeAuthorData(
    image:
        'https://cdn.shopify.com/s/files/1/0850/2114/files/tips_to_help_heighten_senses_480x480.png?v=1624399167',
    name: 'Rasel Ahmed',
  ),
  FakeAuthorData(
    image: '',
    name: 'Karim Hossain',
  ),
  FakeAuthorData(
    image: '',
    name: 'থিবো মেরিস',
  ),
  FakeAuthorData(
    image: '',
    name: 'থিবো মেরিস',
  ),
  FakeAuthorData(
    image:
        'https://cdn.hswstatic.com/gif/play/0b7f4e9b-f59c-4024-9f06-b3dc12850ab7-1920-1080.jpg',
    name: 'থিবো মেরিস',
  ),
  FakeAuthorData(
    image:
        'https://images.healthshots.com/healthshots/en/uploads/2020/12/08182549/positive-person.jpg',
    name: 'Jobayer Hossain',
  ),
  FakeAuthorData(
    image:
        'https://cdn.shopify.com/s/files/1/0850/2114/files/tips_to_help_heighten_senses_480x480.png?v=1624399167',
    name: 'Rasel Ahmed',
  ),
  FakeAuthorData(
    image: '',
    name: 'Karim Hossain',
  ),
];

class FakePublisherData {
  final String image;
  final String name;

  FakePublisherData({
    required this.image,
    required this.name,
  });
}

final fakePublisherList = [
  FakePublisherData(
    image:
        'https://cdn.hswstatic.com/gif/play/0b7f4e9b-f59c-4024-9f06-b3dc12850ab7-1920-1080.jpg',
    name: 'ভিজুয়াল ট্রেনিং',
  ),
  FakePublisherData(
    image:
        'https://images.healthshots.com/healthshots/en/uploads/2020/12/08182549/positive-person.jpg',
    name: 'অন ডিমান্ড পাবলিশিং, এলএলসি-ক্রিয়েট স্পেস',
  ),
  FakePublisherData(
    image:
        'https://cdn.shopify.com/s/files/1/0850/2114/files/tips_to_help_heighten_senses_480x480.png?v=1624399167',
    name: 'Rasel Ahmed',
  ),
  FakePublisherData(
    image: '',
    name: 'Karim Hossain',
  ),
  FakePublisherData(
    image: '',
    name: 'থিবো মেরিস',
  ),
  FakePublisherData(
    image: '',
    name: 'থিবো মেরিস',
  ),
  FakePublisherData(
    image:
        'https://cdn.hswstatic.com/gif/play/0b7f4e9b-f59c-4024-9f06-b3dc12850ab7-1920-1080.jpg',
    name: 'থিবো মেরিস',
  ),
  FakePublisherData(
    image:
        'https://images.healthshots.com/healthshots/en/uploads/2020/12/08182549/positive-person.jpg',
    name: 'Jobayer Hossain',
  ),
  FakePublisherData(
    image:
        'https://cdn.shopify.com/s/files/1/0850/2114/files/tips_to_help_heighten_senses_480x480.png?v=1624399167',
    name: 'Rasel Ahmed',
  ),
  FakePublisherData(
    image: '',
    name: 'Karim Hossain',
  ),
];

class FakeCourierOrderData {
  final String id;
  final String date;
  final String status;
  final String collectionPoint;
  final int totalQuantity;
  final List<CourierBookData> books;

  FakeCourierOrderData({
    required this.id,
    required this.date,
    required this.status,
    required this.collectionPoint,
    required this.totalQuantity,
    required this.books,
  });
}

class CourierBookData {
  final String sl;
  final String name;
  final int receivedQty;

  CourierBookData({
    required this.sl,
    required this.name,
    required this.receivedQty,
  });
}

final fakeCourierOrderList = [
  FakeCourierOrderData(
    id: 'OR-001',
    date: '29-January, 2023',
    status: 'Pending to Collection',
    collectionPoint: 'Head Office',
    totalQuantity: 30,
    books: [
      CourierBookData(
        sl: '01',
        name: 'উত্তর পাহাড়ের ঝর্না',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '02',
        name: 'কাগজের নৌকা',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '03',
        name: 'চন্দ্রমুখী',
        receivedQty: 12,
      ),
    ],
  ),
  FakeCourierOrderData(
    id: 'OR-002',
    date: '29-January, 2023',
    status: 'Pending to Collection',
    collectionPoint: 'Head Office',
    totalQuantity: 30,
    books: [
      CourierBookData(
        sl: '01',
        name: 'উত্তর পাহাড়ের ঝর্না',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '02',
        name: 'কাগজের নৌকা',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '03',
        name: 'চন্দ্রমুখী',
        receivedQty: 12,
      ),
    ],
  ),
  FakeCourierOrderData(
    id: 'OR-003',
    date: '29-January, 2023',
    status: 'Pending to Collection',
    collectionPoint: 'Head Office',
    totalQuantity: 30,
    books: [
      CourierBookData(
        sl: '01',
        name: 'উত্তর পাহাড়ের ঝর্না',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '02',
        name: 'কাগজের নৌকা',
        receivedQty: 5,
      ),
      CourierBookData(
        sl: '03',
        name: 'চন্দ্রমুখী',
        receivedQty: 12,
      ),
    ],
  ),
];
