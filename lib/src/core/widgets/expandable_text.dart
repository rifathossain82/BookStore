import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  final String text;
  final double maxHeight;

  const ExpandableText({
    Key? key,
    required this.text,
    this.maxHeight = 50.0,
  }) : super(key: key);

  @override
  State<ExpandableText> createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText> {

  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    bool isLongText = widget.text.length > widget.maxHeight;
    return Column(
      children: <Widget>[
        ConstrainedBox(
          constraints: isExpanded
              ? const BoxConstraints()
              : BoxConstraints(maxHeight: widget.maxHeight),
          child: Text(
            widget.text,
            softWrap: true,
            overflow: TextOverflow.fade,
          ),
        ),
        if (isLongText)
          const SizedBox(height: 10),
        if (isLongText)
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  isExpanded = !isExpanded;
                });
              },
              child: Text(
                isExpanded ? 'Read less' : 'Read more',
                style: h4.copyWith(
                  fontWeight: FontWeight.bold,
                  color: mainColor,
                ),
              ),
            ),
          ),
      ],
    );
  }
}
