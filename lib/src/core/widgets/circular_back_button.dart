import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class CircularBackButton extends StatelessWidget {
  const CircularBackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Container(
        height: 40,
        width: 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: kWhite,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              offset: const Offset(0, 3),
              color: kGreyMedium.withOpacity(0.5),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Icon(
          Icons.arrow_back_ios_rounded,
          color: kBlackLight,
          size: 15,
        ),
      ),
    );
  }
}
