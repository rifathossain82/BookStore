import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ProductImageBuilder extends StatelessWidget {
  final String imgUrl;
  final double height;
  final double width;
  final bool enableShadow;
  final double borderRadius;

  const ProductImageBuilder({
    Key? key,
    required this.imgUrl,
    this.height = 200,
    this.width = 100,
    this.enableShadow = true,
    this.borderRadius = 5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: CachedNetworkImage(
        imageUrl: imgUrl,
        imageBuilder: (context, imageProvider) => Container(
          height: height,
          width: width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
            boxShadow: [
              enableShadow
                  ? BoxShadow(
                      offset: const Offset(0, 2),
                      blurRadius: 4,
                      spreadRadius: 0,
                      color: kBlackMedium.withOpacity(0.2),
                    )
                  : const BoxShadow(),
            ],
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        fit: BoxFit.cover,
        placeholder: (context, url) => SizedBox(
          height: height,
          width: width,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(borderRadius),
            child: Image.asset(
              AssetPath.placeholder,
              fit: BoxFit.cover,
            ),
          ),
        ),
        errorWidget: (context, url, error) => Container(
          height: height,
          width: width,
          alignment: Alignment.center,
          child: Icon(
            size: 30,
            Icons.image_not_supported,
            color: kBlueGrey,
          ),
        ),
      ),
    );
  }
}
