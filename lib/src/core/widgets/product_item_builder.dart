import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/app_constants.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/product_image_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductItemBuilder extends StatelessWidget {
  final FakeBookData bookData;

  const ProductItemBuilder({
    Key? key,
    required this.bookData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteGenerator.bookDetails,
        arguments: bookData,
      ),
      child: Container(
        width: 130,
        padding: const EdgeInsets.all(5),
        child: Column(
          children: [
            Expanded(
              flex: 6,
              child: ProductImageBuilder(
                imgUrl: bookData.image!,
                height: 200,
                width: 100,
              ),
            ),
            Expanded(
              flex: 4,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 8),
                  Text(
                    bookData.name!,
                    textAlign: TextAlign.center,
                    style: h4,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${AppConstants.currency} ${bookData.regularPrice}',
                        style: h4.copyWith(
                          color: kGrey,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Text(
                        '${AppConstants.currency} ${bookData.offerPrice}',
                        style: h4,
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
