import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class HTMLViewBuilder extends StatelessWidget {
  final String htmlData;

  const HTMLViewBuilder({Key? key, required this.htmlData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Html(
      data: htmlData,
      shrinkWrap: true,
    );
  }
}
