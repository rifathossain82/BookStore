import 'package:book_store/src/core/routes/routes.dart';
import 'package:book_store/src/core/utils/asset_path.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/courier_features/dashboard/presentation/widgets/courier_bottom_navigation_item_builder.dart';
import 'package:flutter/material.dart';

class CourierBottomNavigation extends StatelessWidget {
  const CourierBottomNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      alignment: Alignment.center,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        color: kWhite,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CourierBottomNavigationItemBuilder(
            onTap: () {
              if (ModalRoute.of(context)!.settings.name !=
                  RouteGenerator.courierDashboard) {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  RouteGenerator.courierDashboard,
                  (route) => false,
                );
              }
            },
            assetIconPath: AssetPath.homeIcon,
            isSelected: true,
          ),
          CourierBottomNavigationItemBuilder(
            onTap: () {},
            assetIconPath: AssetPath.supportIcon,
            isSelected: false,
          ),
          CourierBottomNavigationItemBuilder(
            onTap: () {},
            assetIconPath: AssetPath.deliveryBoyIcon,
            isSelected: false,
          ),
          CourierBottomNavigationItemBuilder(
            onTap: () {},
            assetIconPath: AssetPath.historyIcon,
            isSelected: false,
          ),
        ],
      ),
    );
  }
}
