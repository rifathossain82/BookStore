import 'package:flutter/material.dart';

class IconButtonBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final IconData icon;
  final Color? color;

  const IconButtonBuilder({
    Key? key,
    required this.onTap,
    required this.icon,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(5),
        margin: const EdgeInsets.only(right: 8),
        child: Icon(
          icon,
          color: color,
        ),
      ),
    );
  }
}
