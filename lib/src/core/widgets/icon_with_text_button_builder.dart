import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/material.dart';

class IconWithTextButtonBuilder extends StatelessWidget {
  final VoidCallback onTap;
  final double size;
  final Widget icon;
  final String title;
  final Color bgColor;

  const IconWithTextButtonBuilder({
    Key? key,
    required this.onTap,
    this.size = 40,
    required this.icon,
    required this.title,
    required this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: size,
            width: size,
            decoration: BoxDecoration(
              color: bgColor,
              shape: BoxShape.circle,
            ),
            child: icon
          ),
          const SizedBox(height: 5),
          Text(
            title,
            style: h5,
          )
        ],
      ),
    );
  }
}
