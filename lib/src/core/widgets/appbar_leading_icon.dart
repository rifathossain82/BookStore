import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class AppBarLeadingIcon extends StatelessWidget {
  const AppBarLeadingIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pop(context),
      icon: Icon(
        Icons.arrow_back_ios_rounded,
        color: kBlackLight,
        size: 20,
      ),
    );
  }
}
