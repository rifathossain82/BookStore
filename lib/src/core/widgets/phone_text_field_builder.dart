import 'package:book_store/src/core/errors/messages.dart';
import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:flutter/material.dart';

class PhoneTextFieldBuilder extends StatelessWidget {
  final TextEditingController controller;
  final ValueChanged onChanged;
  final bool isValid;
  final int maxLength;
  final FakeCountryInfo selectedCountry;
  final VoidCallback onTapPrefixIcon;
  final TextInputAction textInputAction;
  final String? labelText;

  const PhoneTextFieldBuilder({
    Key? key,
    required this.controller,
    required this.onChanged,
    required this.isValid,
    required this.maxLength,
    required this.selectedCountry,
    required this.onTapPrefixIcon,
    this.textInputAction = TextInputAction.next,
    this.labelText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: (value) {
        if (value.toString().isEmpty) {
          return Message.emptyPhone;
        } else if (value.toString().length < maxLength) {
          return Message.invalidPhone;
        }
        return null;
      },
      onChanged: onChanged,
      maxLines: 1,
      maxLength: maxLength,
      keyboardType: TextInputType.phone,
      textInputAction: textInputAction,
      decoration: InputDecoration(
        labelText: labelText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4),
          borderSide: BorderSide(
            color: isValid ? mainColor : kRedDeep,
          ),
        ),
        isDense: true,
        contentPadding: const EdgeInsets.all(8),
        prefixIcon: GestureDetector(
          onTap: onTapPrefixIcon,
          child: Container(
            padding: const EdgeInsets.only(left: 2, right: 8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black.withOpacity(0.8),
                ),
                const SizedBox(
                  width: 4,
                ),
                Image.network(
                  selectedCountry.flagUrl,
                  height: 30,
                  width: 35,
                ),
                Text(selectedCountry.code),
              ],
            ),
          ),
        ),
        suffix: isValid
            ? Container(
                alignment: Alignment.center,
                width: 24.0,
                height: 24.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: mainColor,
                ),
                child: const Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 13,
                ),
              )
            : Container(
                alignment: Alignment.center,
                width: 24.0,
                height: 24.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: kRedDeep,
                ),
                child: Icon(
                  Icons.close,
                  color: kWhite,
                  size: 13,
                ),
              ),
      ),
    );
  }
}
