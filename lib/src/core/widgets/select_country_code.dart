import 'package:book_store/src/core/fake_data/fake_data.dart';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:book_store/src/core/widgets/k_divider.dart';
import 'package:flutter/material.dart';

Future selectCountryCode(BuildContext context) {
  final TextEditingController searchController = TextEditingController();

  return showDialog(
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            alignment: Alignment.bottomCenter,
            actionsAlignment: MainAxisAlignment.spaceEvenly,
            title: TextField(
              controller: searchController,
              onChanged: (value){
                /// TODO: your search method
              },
              maxLines: 1,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                isDense: true,
                hintText: 'Search country',
                suffixIcon: Icon(
                  Icons.search,
                  size: 20,
                  color: kGrey,
                ),
              ),
            ),
            content: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: fakeCountryList.length,
                      itemBuilder: (context, index) {
                        return CountryCodeTileBuilder(
                          data: fakeCountryList[index],
                        );
                      },
                      separatorBuilder: (context, index) => KDivider(
                        height: 0,
                        color: kGrey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    },
  );
}

class CountryCodeTileBuilder extends StatelessWidget {
  const CountryCodeTileBuilder({Key? key, required this.data})
      : super(key: key);

  final FakeCountryInfo data;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => Navigator.pop(context, data),
      contentPadding: EdgeInsets.zero,
      leading: Image.network(
        data.flagUrl,
        height: 30,
        width: 35,
      ),
      title: Text(
        data.name.toString(),
        style: h4.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
      trailing: Text(
        data.code,
        style: h4.copyWith(
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
