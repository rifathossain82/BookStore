import 'dart:async';
import 'package:book_store/src/core/utils/color.dart';
import 'package:book_store/src/core/utils/styles.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void kSnackBar({
  required BuildContext context,
  required String message,
  required Color bgColor,
}) {
  ScaffoldMessenger.of(context).hideCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      key: UniqueKey(),
      content: Text(message),
      backgroundColor: bgColor,
      duration: const Duration(seconds: 3),
      margin: const EdgeInsets.all(10),
      behavior: SnackBarBehavior.floating,
    ),
  );
}

/// to print in console when app is only debug mode
void kPrint(dynamic data) {
  if (kDebugMode) {
    print(data);
  }
}

Future<bool?> kConfirmDialog({
  required BuildContext context,
  required String title,
  required String negativeActionText,
  required String positiveActionText,
}) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          icon: Icon(
            Icons.question_mark,
            color: mainColor,
          ),
          title: Text(
            title,
            style: h2.copyWith(
              fontWeight: FontWeight.w500,
            ),
          ),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text(
                negativeActionText,
                style: h4.copyWith(
                  color: kGrey,
                ),
              ),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text(
                positiveActionText,
                style: h4.copyWith(
                  color: mainColor,
                ),
              ),
            ),
          ],
        );
      });
}

Future<bool?> customDialog({
  required BuildContext context,
  required String title,
  required AlignmentGeometry dialogPosition,
  List<Widget>? actions,
  Widget? content,
}) {
  return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          content: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: content,
          ),
          insetPadding: const EdgeInsets.all(15),
          alignment: dialogPosition,
          title: Text(
            title,
            textAlign: TextAlign.center,
            style: h2.copyWith(
              fontWeight: FontWeight.w500,
            ),
          ),
          actionsAlignment: MainAxisAlignment.spaceEvenly,
          actions: actions,
        );
      });
}

Future customBottomSheet({
  required BuildContext context,
  double horizontalMargin = 0,
  required Widget content,
}) {
  return showModalBottomSheet(
    context: context,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10),
        topRight: Radius.circular(10),
      ),
    ),
    constraints: BoxConstraints(
        maxWidth: MediaQuery.of(context).size.width - horizontalMargin),
    builder: (context) {
      return content;
    },
  );
}
