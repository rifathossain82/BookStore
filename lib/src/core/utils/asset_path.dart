class AssetPath{

  /// images
  static const String bookStoreAppLogo = 'assets/images/book_store_app_logo.png';
  static const String bookStoreLogo = 'assets/images/book_store_logo.png';
  static const String profile = 'assets/images/people.png';
  static const String placeholder = 'assets/images/placeholder.png';
  static const String sliderImage1 = 'assets/images/slider1.jpg';
  static const String sliderImage2 = 'assets/images/slider2.png';
  static const String categoryBgImage = 'assets/images/category_bg_image.jpg';
  static const String signInLogo = 'assets/images/sign_in.png';
  static const String bookStoreLocationImage = 'assets/images/rokomari_location_image.png';
  static const String authorBg = 'assets/images/author_bg.jpg';

  /// icons
  static const String medical = 'assets/icons/medical.png';
  static const String engineer = 'assets/icons/engineer.png';
  static const String university = 'assets/icons/university.png';
  static const String nursing = 'assets/icons/nursing.png';
  static const String college = 'assets/icons/collage.png';
  static const String school = 'assets/icons/school.png';
  static const String facebookIcon = 'assets/icons/facebook_icon.png';
  static const String googleIcon = 'assets/icons/google_icon.png';
  static const String cashOnDelivery = 'assets/icons/cash-on-delivery.png';
  static const String moneyBackGuarantee = 'assets/icons/money_back_guarantee.png';
  static const String original = 'assets/icons/original.png';
  static const String purchase = 'assets/icons/purchase.png';
  static const String replacement = 'assets/icons/replacement.png';
  static const String masterCard = 'assets/icons/master_card.png';
  static const String nogodPayment = 'assets/icons/nogod_icon.png';
  static const String aboutUs = 'assets/icons/about_us_icon.png';
  static const String booksIcon = 'assets/icons/books_icon.png';
  static const String contactUsIcon = 'assets/icons/contact_us_icon.png';
  static const String giftCardIcon = 'assets/icons/gift_card_icon.png';
  static const String kidsZoneIcon = 'assets/icons/kid_zone_icon.png';
  static const String orderTrackIcon = 'assets/icons/order_track_icon.png';
  static const String returnPolicyIcon = 'assets/icons/return_policy_icon.png';
  static const String publisherIcon = 'assets/icons/publisher_icon.png';
  static const String deliveryIcon = 'assets/icons/delivery_icon.png';
  static const String globalAddressIcon = 'assets/icons/address.png';

  /// courier icons
  static const String homeIcon = 'assets/icons/home.png';
  static const String supportIcon = 'assets/icons/support.png';
  static const String deliveryBoyIcon = 'assets/icons/delivery_boy.png';
  static const String historyIcon = 'assets/icons/history.png';

  /// lotties
  static const String acceptedLottie = 'assets/lotties/accepted_lottie.json';
}