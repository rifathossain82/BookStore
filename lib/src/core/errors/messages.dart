class Message {

  /// validation message
  static const String emptyName = "Name is empty!";
  static const String emptyEmailOrPhone = "Email/Phone is empty!";
  static const String emptyEmail = "Email Address is empty!";
  static const String invalidEmail = "Invalid Email Address!";
  static const String emptyPhone = "Phone Number is empty!";
  static const String invalidPhone = "Invalid Phone Number!";
  static const String emptyPassword = "Password is empty!";
  static const String invalidPassword = "Password must be at least 8 characters!";
  static const String passwordDoesNotMatch = "Password doesn't match!";
  static const String emptyOldPassword = "Please enter your old password!";
  static const String emptyNewPassword = "Please enter your new password!";
  static const String emptyConfirmPassword = "Please enter your confirm password!";
  static const String emptyOTP = "Please enter OTP!";
  static const String invalidOTP = "OTP must be 6 characters!";
  static const String emptyMessage = "Message is empty!";
  static const String emptyReference = "Reference/Url is empty!";
  static const String emptyBookName = "Book Name is empty!";
  static const String emptySubject = "Subject is empty!";
  static const String emptyDescription = "Description is empty!";
  static const String emptyPin = "Pin is empty!";
  static const String invalidPin = "Invalid Pin!";


  /// error message
  static const String noInternet = "Please check your connection!";
}